// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Melange/TEA',
  tagline: 'Melange and TEA docs',
  url: 'https://mel-tea.gitlab.io/',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'mel-tea', // Usually your GitHub org/user name.
  projectName: 'mel-tea.gitlab.io', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/mel-tea/mel-tea.gitlab.io/-/blob/main/',
          routeBasePath: '/',
        },
        blog: false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Mel/TEA',
        logo: {
          alt: 'Mel/TEA',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'bucklescript-docs/intro/what-why',
            position: 'left',
            label: 'Bucklescript docs',
          },
          {
            type: 'doc',
            docId: 'tablecloth-api/Tablecloth',
            position: 'left',
            label: 'Tablecloth API',
          },
          {
            type: 'doc',
            docId: 'tea-api/Tea',
            position: 'left',
            label: 'TEA API',
          },
          {
            type: 'doc',
            docId: 'melange-tutorial/intro',
            position: 'left',
            label: 'Melange tutorial',
          },
          {
            type: 'doc',
            docId: 'tea-tutorial/intro',
            position: 'left',
            label: 'TEA tutorial',
          },
          {
            href: 'https://gitlab.com/mel-tea/mel-tea.gitlab.io',
            label: 'Repository',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Melange',
            items: [
              {
                label: 'GitHub',
                href: 'https://github.com/melange-re/melange',
              },
              {
                label: 'Discord',
                href: 'https://discordapp.com/channels/235176658175262720/825155604641218580',
              },
            ],
          },
          {
            title: 'TEA',
            items: [
              {
                label: 'bucklescript-tea',
                href: 'https://github.com/OvermindDL1/bucklescript-tea',
              },
              {
                label: 'rescript-tea',
                href: 'https://github.com/pbiggar/rescript-tea',
              },
              {
                label: 'Elm guide',
                href: 'https://guide.elm-lang.org/architecture/',
              },
            ],
          },
          {
            title: 'Tablecloth',
            items: [
              {
                label: 'Website',
                href: 'https://tableclothml.netlify.app/',
              },
              {
                label: 'GitHub',
                href: 'https://github.com/darklang/tablecloth',
              },
              {
                label: 'NPM',
                href: 'https://www.npmjs.com/package/tablecloth',
              },
            ],
          },
        ],
        copyright: `Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
      algolia: {
        // The application ID provided by Algolia
        appId: 'D7M1FQDVF9',
        // Public API key: it is safe to commit it
        apiKey: '3c8de151ab785fcf30901207a6c3cf8c',
        indexName: 'mel-tea',
        // Optional: see doc section below
        contextualSearch: true,
        // Optional: Specify domains where the navigation should occur through window.location instead on history.push. Useful when our Algolia config crawls multiple documentation sites and we want to navigate with window.location.href to them.
        // externalUrlRegex: 'external\\.com|domain\\.com',
        // Optional: Algolia search parameters
        searchParameters: {},
        // Optional: path for search page that enabled by default (`false` to disable it)
        searchPagePath: 'search',
      },
    }),
};

module.exports = config;
