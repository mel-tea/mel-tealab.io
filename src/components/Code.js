import React from 'react';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

export default function Code({ children }) {
  const langMap = { ocaml: "OCaml", reason: "ReasonML" };

  const tabItems = children.map(element => {
    const lang = element.props.children.props.className.substr(9)

    return (
      <TabItem value={lang} label={langMap[lang]}>
        {element}
      </TabItem>
    )
  });

  return (
    <Tabs groupId="syntax">
      {tabItems}
    </Tabs>
  );
}