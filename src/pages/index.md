---
title: Index
---

# Melange and TEA docs

This place is intended to host tutorials and documentation for Melange and TEA.

For now it is only hosting [the old Bucklescript documentation](/bucklescript-docs), and the [API documentation](/tablecloth-api/Tablecloth) of [Tablecloth](https://tableclothml.netlify.app/), for convenience. The rest is very much WIP.

Ideally, every code sample should show all 3 syntaxes (OCaml, ReasonML, Rescript).

For contributing, please use [the GitLab repository](https://gitlab.com/mel-tea/mel-tea.gitlab.io).
