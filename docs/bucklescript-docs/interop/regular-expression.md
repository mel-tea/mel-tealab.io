---
title: Regular Expression
sidebar_position: 15
---

See the Reason documentation on [Regular Expression](https://reasonml.github.io/docs/en/regular-expression)
