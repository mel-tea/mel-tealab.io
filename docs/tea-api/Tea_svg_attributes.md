# Module `Tea_svg_attributes`

### `val accentHeight`

``` ocaml
val accentHeight : string -> 'a Vdom.property
```

### `val accelerate`

``` ocaml
val accelerate : string -> 'a Vdom.property
```

### `val accumulate`

``` ocaml
val accumulate : string -> 'a Vdom.property
```

### `val additive`

``` ocaml
val additive : string -> 'a Vdom.property
```

### `val alphabetic`

``` ocaml
val alphabetic : string -> 'a Vdom.property
```

### `val allowReorder`

``` ocaml
val allowReorder : string -> 'a Vdom.property
```

### `val amplitude`

``` ocaml
val amplitude : string -> 'a Vdom.property
```

### `val arabicForm`

``` ocaml
val arabicForm : string -> 'a Vdom.property
```

### `val ascent`

``` ocaml
val ascent : string -> 'a Vdom.property
```

### `val attributeName`

``` ocaml
val attributeName : string -> 'a Vdom.property
```

### `val attributeType`

``` ocaml
val attributeType : string -> 'a Vdom.property
```

### `val autoReverse`

``` ocaml
val autoReverse : string -> 'a Vdom.property
```

### `val azimuth`

``` ocaml
val azimuth : string -> 'a Vdom.property
```

### `val baseFrequency`

``` ocaml
val baseFrequency : string -> 'a Vdom.property
```

### `val baseProfile`

``` ocaml
val baseProfile : string -> 'a Vdom.property
```

### `val bbox`

``` ocaml
val bbox : string -> 'a Vdom.property
```

### `val begin'`

``` ocaml
val begin' : string -> 'a Vdom.property
```

### `val bias`

``` ocaml
val bias : string -> 'a Vdom.property
```

### `val by`

``` ocaml
val by : string -> 'a Vdom.property
```

### `val calcMode`

``` ocaml
val calcMode : string -> 'a Vdom.property
```

### `val capHeight`

``` ocaml
val capHeight : string -> 'a Vdom.property
```

### `val class'`

``` ocaml
val class' : string -> 'a Vdom.property
```

### `val clipPathUnits`

``` ocaml
val clipPathUnits : string -> 'a Vdom.property
```

### `val contentScriptType`

``` ocaml
val contentScriptType : string -> 'a Vdom.property
```

### `val contentStyleType`

``` ocaml
val contentStyleType : string -> 'a Vdom.property
```

### `val cx`

``` ocaml
val cx : string -> 'a Vdom.property
```

### `val cy`

``` ocaml
val cy : string -> 'a Vdom.property
```

### `val d`

``` ocaml
val d : string -> 'a Vdom.property
```

### `val decelerate`

``` ocaml
val decelerate : string -> 'a Vdom.property
```

### `val descent`

``` ocaml
val descent : string -> 'a Vdom.property
```

### `val diffuseConstant`

``` ocaml
val diffuseConstant : string -> 'a Vdom.property
```

### `val divisor`

``` ocaml
val divisor : string -> 'a Vdom.property
```

### `val dur`

``` ocaml
val dur : string -> 'a Vdom.property
```

### `val dx`

``` ocaml
val dx : string -> 'a Vdom.property
```

### `val dy`

``` ocaml
val dy : string -> 'a Vdom.property
```

### `val edgeMode`

``` ocaml
val edgeMode : string -> 'a Vdom.property
```

### `val elevation`

``` ocaml
val elevation : string -> 'a Vdom.property
```

### `val end'`

``` ocaml
val end' : string -> 'a Vdom.property
```

### `val exponent`

``` ocaml
val exponent : string -> 'a Vdom.property
```

### `val externalResourcesRequired`

``` ocaml
val externalResourcesRequired : string -> 'a Vdom.property
```

### `val filterRes`

``` ocaml
val filterRes : string -> 'a Vdom.property
```

### `val filterUnits`

``` ocaml
val filterUnits : string -> 'a Vdom.property
```

### `val format`

``` ocaml
val format : string -> 'a Vdom.property
```

### `val from`

``` ocaml
val from : string -> 'a Vdom.property
```

### `val fx`

``` ocaml
val fx : string -> 'a Vdom.property
```

### `val fy`

``` ocaml
val fy : string -> 'a Vdom.property
```

### `val g1`

``` ocaml
val g1 : string -> 'a Vdom.property
```

### `val g2`

``` ocaml
val g2 : string -> 'a Vdom.property
```

### `val glyphName`

``` ocaml
val glyphName : string -> 'a Vdom.property
```

### `val glyphRef`

``` ocaml
val glyphRef : string -> 'a Vdom.property
```

### `val gradientTransform`

``` ocaml
val gradientTransform : string -> 'a Vdom.property
```

### `val gradientUnits`

``` ocaml
val gradientUnits : string -> 'a Vdom.property
```

### `val hanging`

``` ocaml
val hanging : string -> 'a Vdom.property
```

### `val height`

``` ocaml
val height : string -> 'a Vdom.property
```

### `val horizAdvX`

``` ocaml
val horizAdvX : string -> 'a Vdom.property
```

### `val horizOriginX`

``` ocaml
val horizOriginX : string -> 'a Vdom.property
```

### `val horizOriginY`

``` ocaml
val horizOriginY : string -> 'a Vdom.property
```

### `val id`

``` ocaml
val id : string -> 'a Vdom.property
```

### `val ideographic`

``` ocaml
val ideographic : string -> 'a Vdom.property
```

### `val in'`

``` ocaml
val in' : string -> 'a Vdom.property
```

### `val in2`

``` ocaml
val in2 : string -> 'a Vdom.property
```

### `val intercept`

``` ocaml
val intercept : string -> 'a Vdom.property
```

### `val k`

``` ocaml
val k : string -> 'a Vdom.property
```

### `val k1`

``` ocaml
val k1 : string -> 'a Vdom.property
```

### `val k2`

``` ocaml
val k2 : string -> 'a Vdom.property
```

### `val k3`

``` ocaml
val k3 : string -> 'a Vdom.property
```

### `val k4`

``` ocaml
val k4 : string -> 'a Vdom.property
```

### `val kernelMatrix`

``` ocaml
val kernelMatrix : string -> 'a Vdom.property
```

### `val kernelUnitLength`

``` ocaml
val kernelUnitLength : string -> 'a Vdom.property
```

### `val keyPoints`

``` ocaml
val keyPoints : string -> 'a Vdom.property
```

### `val keySplines`

``` ocaml
val keySplines : string -> 'a Vdom.property
```

### `val keyTimes`

``` ocaml
val keyTimes : string -> 'a Vdom.property
```

### `val lang`

``` ocaml
val lang : string -> 'a Vdom.property
```

### `val lengthAdjust`

``` ocaml
val lengthAdjust : string -> 'a Vdom.property
```

### `val limitingConeAngle`

``` ocaml
val limitingConeAngle : string -> 'a Vdom.property
```

### `val local`

``` ocaml
val local : string -> 'a Vdom.property
```

### `val markerHeight`

``` ocaml
val markerHeight : string -> 'a Vdom.property
```

### `val markerUnits`

``` ocaml
val markerUnits : string -> 'a Vdom.property
```

### `val markerWidth`

``` ocaml
val markerWidth : string -> 'a Vdom.property
```

### `val maskContentUnits`

``` ocaml
val maskContentUnits : string -> 'a Vdom.property
```

### `val maskUnits`

``` ocaml
val maskUnits : string -> 'a Vdom.property
```

### `val mathematical`

``` ocaml
val mathematical : string -> 'a Vdom.property
```

### `val max`

``` ocaml
val max : string -> 'a Vdom.property
```

### `val media`

``` ocaml
val media : string -> 'a Vdom.property
```

### `val method'`

``` ocaml
val method' : string -> 'a Vdom.property
```

### `val min`

``` ocaml
val min : string -> 'a Vdom.property
```

### `val mode`

``` ocaml
val mode : string -> 'a Vdom.property
```

### `val name`

``` ocaml
val name : string -> 'a Vdom.property
```

### `val numOctaves`

``` ocaml
val numOctaves : string -> 'a Vdom.property
```

### `val offset`

``` ocaml
val offset : string -> 'a Vdom.property
```

### `val operator`

``` ocaml
val operator : string -> 'a Vdom.property
```

### `val order`

``` ocaml
val order : string -> 'a Vdom.property
```

### `val orient`

``` ocaml
val orient : string -> 'a Vdom.property
```

### `val orientation`

``` ocaml
val orientation : string -> 'a Vdom.property
```

### `val origin`

``` ocaml
val origin : string -> 'a Vdom.property
```

### `val overlinePosition`

``` ocaml
val overlinePosition : string -> 'a Vdom.property
```

### `val overlineThickness`

``` ocaml
val overlineThickness : string -> 'a Vdom.property
```

### `val panose1`

``` ocaml
val panose1 : string -> 'a Vdom.property
```

### `val path`

``` ocaml
val path : string -> 'a Vdom.property
```

### `val pathLength`

``` ocaml
val pathLength : string -> 'a Vdom.property
```

### `val patternContentUnits`

``` ocaml
val patternContentUnits : string -> 'a Vdom.property
```

### `val patternTransform`

``` ocaml
val patternTransform : string -> 'a Vdom.property
```

### `val patternUnits`

``` ocaml
val patternUnits : string -> 'a Vdom.property
```

### `val pointOrder`

``` ocaml
val pointOrder : string -> 'a Vdom.property
```

### `val points`

``` ocaml
val points : string -> 'a Vdom.property
```

### `val pointsAtX`

``` ocaml
val pointsAtX : string -> 'a Vdom.property
```

### `val pointsAtY`

``` ocaml
val pointsAtY : string -> 'a Vdom.property
```

### `val pointsAtZ`

``` ocaml
val pointsAtZ : string -> 'a Vdom.property
```

### `val preserveAlpha`

``` ocaml
val preserveAlpha : string -> 'a Vdom.property
```

### `val preserveAspectRatio`

``` ocaml
val preserveAspectRatio : string -> 'a Vdom.property
```

### `val primitiveUnits`

``` ocaml
val primitiveUnits : string -> 'a Vdom.property
```

### `val r`

``` ocaml
val r : string -> 'a Vdom.property
```

### `val radius`

``` ocaml
val radius : string -> 'a Vdom.property
```

### `val refX`

``` ocaml
val refX : string -> 'a Vdom.property
```

### `val refY`

``` ocaml
val refY : string -> 'a Vdom.property
```

### `val renderingIntent`

``` ocaml
val renderingIntent : string -> 'a Vdom.property
```

### `val repeatCount`

``` ocaml
val repeatCount : string -> 'a Vdom.property
```

### `val repeatDur`

``` ocaml
val repeatDur : string -> 'a Vdom.property
```

### `val requiredExtensions`

``` ocaml
val requiredExtensions : string -> 'a Vdom.property
```

### `val requiredFeatures`

``` ocaml
val requiredFeatures : string -> 'a Vdom.property
```

### `val restart`

``` ocaml
val restart : string -> 'a Vdom.property
```

### `val result`

``` ocaml
val result : string -> 'a Vdom.property
```

### `val rotate`

``` ocaml
val rotate : string -> 'a Vdom.property
```

### `val rx`

``` ocaml
val rx : string -> 'a Vdom.property
```

### `val ry`

``` ocaml
val ry : string -> 'a Vdom.property
```

### `val scale`

``` ocaml
val scale : string -> 'a Vdom.property
```

### `val seed`

``` ocaml
val seed : string -> 'a Vdom.property
```

### `val slope`

``` ocaml
val slope : string -> 'a Vdom.property
```

### `val spacing`

``` ocaml
val spacing : string -> 'a Vdom.property
```

### `val specularConstant`

``` ocaml
val specularConstant : string -> 'a Vdom.property
```

### `val specularExponent`

``` ocaml
val specularExponent : string -> 'a Vdom.property
```

### `val speed`

``` ocaml
val speed : string -> 'a Vdom.property
```

### `val spreadMethod`

``` ocaml
val spreadMethod : string -> 'a Vdom.property
```

### `val startOffset`

``` ocaml
val startOffset : string -> 'a Vdom.property
```

### `val stdDeviation`

``` ocaml
val stdDeviation : string -> 'a Vdom.property
```

### `val stemh`

``` ocaml
val stemh : string -> 'a Vdom.property
```

### `val stemv`

``` ocaml
val stemv : string -> 'a Vdom.property
```

### `val stitchTiles`

``` ocaml
val stitchTiles : string -> 'a Vdom.property
```

### `val strikethroughPosition`

``` ocaml
val strikethroughPosition : string -> 'a Vdom.property
```

### `val strikethroughThickness`

``` ocaml
val strikethroughThickness : string -> 'a Vdom.property
```

### `val string`

``` ocaml
val string : string -> 'a Vdom.property
```

### `val style`

``` ocaml
val style : string -> 'a Vdom.property
```

### `val surfaceScale`

``` ocaml
val surfaceScale : string -> 'a Vdom.property
```

### `val systemLanguage`

``` ocaml
val systemLanguage : string -> 'a Vdom.property
```

### `val tableValues`

``` ocaml
val tableValues : string -> 'a Vdom.property
```

### `val target`

``` ocaml
val target : string -> 'a Vdom.property
```

### `val targetX`

``` ocaml
val targetX : string -> 'a Vdom.property
```

### `val targetY`

``` ocaml
val targetY : string -> 'a Vdom.property
```

### `val textLength`

``` ocaml
val textLength : string -> 'a Vdom.property
```

### `val title`

``` ocaml
val title : string -> 'a Vdom.property
```

### `val to'`

``` ocaml
val to' : string -> 'a Vdom.property
```

### `val transform`

``` ocaml
val transform : string -> 'a Vdom.property
```

### `val type'`

``` ocaml
val type' : string -> 'a Vdom.property
```

### `val u1`

``` ocaml
val u1 : string -> 'a Vdom.property
```

### `val u2`

``` ocaml
val u2 : string -> 'a Vdom.property
```

### `val underlinePosition`

``` ocaml
val underlinePosition : string -> 'a Vdom.property
```

### `val underlineThickness`

``` ocaml
val underlineThickness : string -> 'a Vdom.property
```

### `val unicode`

``` ocaml
val unicode : string -> 'a Vdom.property
```

### `val unicodeRange`

``` ocaml
val unicodeRange : string -> 'a Vdom.property
```

### `val unitsPerEm`

``` ocaml
val unitsPerEm : string -> 'a Vdom.property
```

### `val vAlphabetic`

``` ocaml
val vAlphabetic : string -> 'a Vdom.property
```

### `val vHanging`

``` ocaml
val vHanging : string -> 'a Vdom.property
```

### `val vIdeographic`

``` ocaml
val vIdeographic : string -> 'a Vdom.property
```

### `val vMathematical`

``` ocaml
val vMathematical : string -> 'a Vdom.property
```

### `val values`

``` ocaml
val values : string -> 'a Vdom.property
```

### `val version`

``` ocaml
val version : string -> 'a Vdom.property
```

### `val vertAdvY`

``` ocaml
val vertAdvY : string -> 'a Vdom.property
```

### `val vertOriginX`

``` ocaml
val vertOriginX : string -> 'a Vdom.property
```

### `val vertOriginY`

``` ocaml
val vertOriginY : string -> 'a Vdom.property
```

### `val viewBox`

``` ocaml
val viewBox : string -> 'a Vdom.property
```

### `val viewTarget`

``` ocaml
val viewTarget : string -> 'a Vdom.property
```

### `val width`

``` ocaml
val width : string -> 'a Vdom.property
```

### `val widths`

``` ocaml
val widths : string -> 'a Vdom.property
```

### `val x`

``` ocaml
val x : string -> 'a Vdom.property
```

### `val xHeight`

``` ocaml
val xHeight : string -> 'a Vdom.property
```

### `val x1`

``` ocaml
val x1 : string -> 'a Vdom.property
```

### `val x2`

``` ocaml
val x2 : string -> 'a Vdom.property
```

### `val xChannelSelector`

``` ocaml
val xChannelSelector : string -> 'a Vdom.property
```

### `val xlinkActuate`

``` ocaml
val xlinkActuate : string -> 'a Vdom.property
```

### `val xlinkArcrole`

``` ocaml
val xlinkArcrole : string -> 'a Vdom.property
```

### `val xlinkHref`

``` ocaml
val xlinkHref : string -> 'a Vdom.property
```

### `val xlinkRole`

``` ocaml
val xlinkRole : string -> 'a Vdom.property
```

### `val xlinkShow`

``` ocaml
val xlinkShow : string -> 'a Vdom.property
```

### `val xlinkTitle`

``` ocaml
val xlinkTitle : string -> 'a Vdom.property
```

### `val xlinkType`

``` ocaml
val xlinkType : string -> 'a Vdom.property
```

### `val xmlBase`

``` ocaml
val xmlBase : string -> 'a Vdom.property
```

### `val xmlLang`

``` ocaml
val xmlLang : string -> 'a Vdom.property
```

### `val xmlSpace`

``` ocaml
val xmlSpace : string -> 'a Vdom.property
```

### `val y`

``` ocaml
val y : string -> 'a Vdom.property
```

### `val y1`

``` ocaml
val y1 : string -> 'a Vdom.property
```

### `val y2`

``` ocaml
val y2 : string -> 'a Vdom.property
```

### `val yChannelSelector`

``` ocaml
val yChannelSelector : string -> 'a Vdom.property
```

### `val z`

``` ocaml
val z : string -> 'a Vdom.property
```

### `val zoomAndPan`

``` ocaml
val zoomAndPan : string -> 'a Vdom.property
```

### `val alignmentBaseline`

``` ocaml
val alignmentBaseline : string -> 'a Vdom.property
```

### `val baselineShift`

``` ocaml
val baselineShift : string -> 'a Vdom.property
```

### `val clipPath`

``` ocaml
val clipPath : string -> 'a Vdom.property
```

### `val clipRule`

``` ocaml
val clipRule : string -> 'a Vdom.property
```

### `val clip`

``` ocaml
val clip : string -> 'a Vdom.property
```

### `val colorInterpolationFilters`

``` ocaml
val colorInterpolationFilters : string -> 'a Vdom.property
```

### `val colorInterpolation`

``` ocaml
val colorInterpolation : string -> 'a Vdom.property
```

### `val colorProfile`

``` ocaml
val colorProfile : string -> 'a Vdom.property
```

### `val colorRendering`

``` ocaml
val colorRendering : string -> 'a Vdom.property
```

### `val color`

``` ocaml
val color : string -> 'a Vdom.property
```

### `val cursor`

``` ocaml
val cursor : string -> 'a Vdom.property
```

### `val direction`

``` ocaml
val direction : string -> 'a Vdom.property
```

### `val display`

``` ocaml
val display : string -> 'a Vdom.property
```

### `val dominantBaseline`

``` ocaml
val dominantBaseline : string -> 'a Vdom.property
```

### `val enableBackground`

``` ocaml
val enableBackground : string -> 'a Vdom.property
```

### `val fillOpacity`

``` ocaml
val fillOpacity : string -> 'a Vdom.property
```

### `val fillRule`

``` ocaml
val fillRule : string -> 'a Vdom.property
```

### `val fill`

``` ocaml
val fill : string -> 'a Vdom.property
```

### `val filter`

``` ocaml
val filter : string -> 'a Vdom.property
```

### `val floodColor`

``` ocaml
val floodColor : string -> 'a Vdom.property
```

### `val floodOpacity`

``` ocaml
val floodOpacity : string -> 'a Vdom.property
```

### `val fontFamily`

``` ocaml
val fontFamily : string -> 'a Vdom.property
```

### `val fontSizeAdjust`

``` ocaml
val fontSizeAdjust : string -> 'a Vdom.property
```

### `val fontSize`

``` ocaml
val fontSize : string -> 'a Vdom.property
```

### `val fontStretch`

``` ocaml
val fontStretch : string -> 'a Vdom.property
```

### `val fontStyle`

``` ocaml
val fontStyle : string -> 'a Vdom.property
```

### `val fontVariant`

``` ocaml
val fontVariant : string -> 'a Vdom.property
```

### `val fontWeight`

``` ocaml
val fontWeight : string -> 'a Vdom.property
```

### `val glyphOrientationHorizontal`

``` ocaml
val glyphOrientationHorizontal : string -> 'a Vdom.property
```

### `val glyphOrientationVertical`

``` ocaml
val glyphOrientationVertical : string -> 'a Vdom.property
```

### `val imageRendering`

``` ocaml
val imageRendering : string -> 'a Vdom.property
```

### `val kerning`

``` ocaml
val kerning : string -> 'a Vdom.property
```

### `val letterSpacing`

``` ocaml
val letterSpacing : string -> 'a Vdom.property
```

### `val lightingColor`

``` ocaml
val lightingColor : string -> 'a Vdom.property
```

### `val markerEnd`

``` ocaml
val markerEnd : string -> 'a Vdom.property
```

### `val markerMid`

``` ocaml
val markerMid : string -> 'a Vdom.property
```

### `val markerStart`

``` ocaml
val markerStart : string -> 'a Vdom.property
```

### `val mask`

``` ocaml
val mask : string -> 'a Vdom.property
```

### `val opacity`

``` ocaml
val opacity : string -> 'a Vdom.property
```

### `val overflow`

``` ocaml
val overflow : string -> 'a Vdom.property
```

### `val pointerEvents`

``` ocaml
val pointerEvents : string -> 'a Vdom.property
```

### `val shapeRendering`

``` ocaml
val shapeRendering : string -> 'a Vdom.property
```

### `val stopColor`

``` ocaml
val stopColor : string -> 'a Vdom.property
```

### `val stopOpacity`

``` ocaml
val stopOpacity : string -> 'a Vdom.property
```

### `val strokeDasharray`

``` ocaml
val strokeDasharray : string -> 'a Vdom.property
```

### `val strokeDashoffset`

``` ocaml
val strokeDashoffset : string -> 'a Vdom.property
```

### `val strokeLinecap`

``` ocaml
val strokeLinecap : string -> 'a Vdom.property
```

### `val strokeLinejoin`

``` ocaml
val strokeLinejoin : string -> 'a Vdom.property
```

### `val strokeMiterlimit`

``` ocaml
val strokeMiterlimit : string -> 'a Vdom.property
```

### `val strokeOpacity`

``` ocaml
val strokeOpacity : string -> 'a Vdom.property
```

### `val strokeWidth`

``` ocaml
val strokeWidth : string -> 'a Vdom.property
```

### `val stroke`

``` ocaml
val stroke : string -> 'a Vdom.property
```

### `val textAnchor`

``` ocaml
val textAnchor : string -> 'a Vdom.property
```

### `val textDecoration`

``` ocaml
val textDecoration : string -> 'a Vdom.property
```

### `val textRendering`

``` ocaml
val textRendering : string -> 'a Vdom.property
```

### `val unicodeBidi`

``` ocaml
val unicodeBidi : string -> 'a Vdom.property
```

### `val visibility`

``` ocaml
val visibility : string -> 'a Vdom.property
```

### `val wordSpacing`

``` ocaml
val wordSpacing : string -> 'a Vdom.property
```

### `val writingMode`

``` ocaml
val writingMode : string -> 'a Vdom.property
```
