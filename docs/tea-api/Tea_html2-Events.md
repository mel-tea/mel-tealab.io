# Module `Tea_html2.Events`

-   [Primitives](#primitives)
-   [Mouse helpers](#mouse-helpers)
-   [Form helpers](#form-helpers)
-   [Focus helpers](#focus-helpers)

## [](#primitives)Primitives

### `val onCB`

``` ocaml
val onCB :    string ->   string ->   ( Web.Node.event -> 'a option ) ->   'a Vdom.property
```

### `val onMsg`

``` ocaml
val onMsg : string -> 'a -> 'b Vdom.property
```

### `val on`

``` ocaml
val on :    key:string ->   string ->   ( 'a, 'b ) Tea_json.Decoder.t ->   'b Vdom.property
```

### `val onWithOptions`

``` ocaml
val onWithOptions :    key:string ->   string ->   Tea_html.options ->   ( 'a, 'b ) Tea_json.Decoder.t ->   'b Vdom.property
```

### `val defaultOptions`

``` ocaml
val defaultOptions : Tea_html.options
```

### `val targetValue`

``` ocaml
val targetValue : ( Web.Json.t, string ) Tea_json.Decoder.t
```

### `val targetChecked`

``` ocaml
val targetChecked : ( Web.Json.t, bool ) Tea_json.Decoder.t
```

### `val keyCode`

``` ocaml
val keyCode : ( Web.Json.t, int ) Tea_json.Decoder.t
```

### `val preventDefaultOn`

``` ocaml
val preventDefaultOn :    ?key:string ->   string ->   ( 'a, 'b ) Tea_json.Decoder.t ->   'b Vdom.property
```

## [](#mouse-helpers)Mouse helpers

### `val onClick`

``` ocaml
val onClick : 'a -> 'b Vdom.property
```

### `val onDoubleClick`

``` ocaml
val onDoubleClick : 'a -> 'b Vdom.property
```

### `val onMouseDown`

``` ocaml
val onMouseDown : 'a -> 'b Vdom.property
```

### `val onMouseUp`

``` ocaml
val onMouseUp : 'a -> 'b Vdom.property
```

### `val onMouseEnter`

``` ocaml
val onMouseEnter : 'a -> 'b Vdom.property
```

### `val onMouseLeave`

``` ocaml
val onMouseLeave : 'a -> 'b Vdom.property
```

### `val onMouseOver`

``` ocaml
val onMouseOver : 'a -> 'b Vdom.property
```

### `val onMouseOut`

``` ocaml
val onMouseOut : 'a -> 'b Vdom.property
```

## [](#form-helpers)Form helpers

### `val onInputOpt`

``` ocaml
val onInputOpt : ?key:string -> ( string -> 'a option ) -> 'a Vdom.property
```

### `val onInput`

``` ocaml
val onInput : ?key:string -> ( string -> 'a ) -> 'b Vdom.property
```

### `val onCheckOpt`

``` ocaml
val onCheckOpt : ?key:string -> ( bool -> 'a option ) -> 'a Vdom.property
```

### `val onCheck`

``` ocaml
val onCheck : ?key:string -> ( bool -> 'a ) -> 'b Vdom.property
```

### `val onChangeOpt`

``` ocaml
val onChangeOpt : ?key:string -> ( string -> 'a option ) -> 'a Vdom.property
```

### `val onChange`

``` ocaml
val onChange : ?key:string -> ( string -> 'a ) -> 'b Vdom.property
```

### `val onSubmit`

``` ocaml
val onSubmit : 'a -> 'b Vdom.property
```

## [](#focus-helpers)Focus helpers

### `val onBlur`

``` ocaml
val onBlur : 'a -> 'b Vdom.property
```

### `val onFocus`

``` ocaml
val onFocus : 'a -> 'b Vdom.property
```
