# Module `Web_window`

### `module History`

``` ocaml
module History = Web_window_history
```

### `module LocalStorage`

``` ocaml
module LocalStorage = Web_window_localstorage
```

### `type timeoutHandlerID`

``` ocaml
type timeoutHandlerID = int
```

### `type t`

``` ocaml
type t =   < history : History.t Js.Undefined.t     ; location : Web_location.t     ; clearTimeout : ( timeoutHandlerID -> unit ) Js_OO.Meth.arity1     ; requestAnimationFrame : ( ( float -> unit ) -> int ) Js_OO.Meth.arity1     ; cancelAnimationFrame : ( int -> unit ) Js_OO.Meth.arity1     ; setInterval :        ( ( unit -> unit ) -> float -> timeoutHandlerID ) Js_OO.Meth.arity2     ; setTimeout :        ( ( unit -> unit ) -> float -> timeoutHandlerID ) Js_OO.Meth.arity2     ; addEventListener :        ( string ->         Web_node.t Web_event.cb ->         Web_event.options ->         unit )         Js_OO.Meth.arity3     ; removeEventListener :        ( string ->         Web_node.t Web_event.cb ->         Web_event.options ->         unit )         Js_OO.Meth.arity3     ; localStorage : LocalStorage.t Js.Undefined.t >     Js.t
```

### `val window`

``` ocaml
val window : t
```

### `val history`

``` ocaml
val history : unit -> History.t Js.Undefined.t
```

### `val localStorage`

``` ocaml
val localStorage : unit -> LocalStorage.t Js.Undefined.t
```

### `val location`

``` ocaml
val location : unit -> Web_location.t
```

### `val requestAnimationFrame`

``` ocaml
val requestAnimationFrame : ( float -> unit ) -> int
```

### `val cancelAnimationFrame`

``` ocaml
val cancelAnimationFrame : int -> unit
```

### `val clearTimeout`

``` ocaml
val clearTimeout : timeoutHandlerID -> unit
```

### `val setInterval`

``` ocaml
val setInterval : ( unit -> unit ) -> float -> timeoutHandlerID
```

### `val setTimeout`

``` ocaml
val setTimeout : ( unit -> unit ) -> float -> timeoutHandlerID
```

### `val addEventListener`

``` ocaml
val addEventListener :    string ->   Web_node.t Web_event.cb ->   Web_event.options ->   unit
```

### `val removeEventListener`

``` ocaml
val removeEventListener :    string ->   Web_node.t Web_event.cb ->   Web_event.options ->   unit
```

### `val requestAnimationFrame_polyfill`

``` ocaml
val requestAnimationFrame_polyfill : unit -> unit
```
