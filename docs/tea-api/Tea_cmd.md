# Module `Tea_cmd`

### `type applicationCallbacks`

``` ocaml
type 'msg applicationCallbacks = 'msg Vdom.applicationCallbacks
```

### `type t`

``` ocaml
type 'msg t = 
```

### `val none`

``` ocaml
val none : 'a t
```

### `val batch`

``` ocaml
val batch : 'a t list -> 'a t
```

### `val call`

``` ocaml
val call : ( 'a applicationCallbacks Stdlib.ref -> unit ) -> 'a t
```

### `val fnMsg`

``` ocaml
val fnMsg : ( unit -> 'a ) -> 'b t
```

### `val msg`

``` ocaml
val msg : 'a -> 'b t
```

### `val run`

``` ocaml
val run : 'msg. 'msg applicationCallbacks Stdlib.ref -> 'msg t -> unit
```

### `val map`

``` ocaml
val map : 'a 'b. ( 'a -> 'b ) -> 'a t -> 'b t
```
