# Module `Tea_ex`

### `val render_event`

``` ocaml
val render_event : ?key:string -> 'a -> 'b Tea_sub.t
```

### `module LocalStorage`

``` ocaml
module LocalStorage : sig ... end
```
