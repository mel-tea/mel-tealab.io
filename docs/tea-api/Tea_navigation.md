# Module `Tea_navigation`

### `type navigationProgram`

``` ocaml
type ('flags, 'model, 'msg) navigationProgram = {
```

### `val getLocation`

``` ocaml
val getLocation : unit -> Web.Location.location
```

### `val notifier`

``` ocaml
val notifier : ( Web.Location.location -> unit ) option Stdlib.ref
```

### `val notifyUrlChange`

``` ocaml
val notifyUrlChange : unit -> unit
```

### `val subscribe`

``` ocaml
val subscribe : ( Web.Location.location -> 'a ) -> 'b Tea_sub.t
```

### `val replaceState`

``` ocaml
val replaceState : string -> unit
```

### `val pushState`

``` ocaml
val pushState : string -> unit
```

### `val modifyUrl`

``` ocaml
val modifyUrl : string -> 'a Tea_cmd.t
```

### `val newUrl`

``` ocaml
val newUrl : string -> 'a Tea_cmd.t
```

### `val go`

``` ocaml
val go : int -> 'a Tea_cmd.t
```

### `val back`

``` ocaml
val back : int -> 'a Tea_cmd.t
```

### `val forward`

``` ocaml
val forward : int -> 'a Tea_cmd.t
```

### `val navigationProgram`

``` ocaml
val navigationProgram :    ( Web.Location.location -> 'a ) ->   ( 'b, 'c, 'd ) navigationProgram ->   Web.Node.t Js.null_undefined ->   'e ->   'f Tea_app.programInterface
```
