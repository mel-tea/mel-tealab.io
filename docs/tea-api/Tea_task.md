# Module `Tea_task`

### `type never`

``` ocaml
type never
```

### `type t`

``` ocaml
type ('succeed, 'fail) t = 
```

### `val nothing`

``` ocaml
val nothing : unit -> unit
```

### `val performOpt`

``` ocaml
val performOpt :    ( 'value -> 'msg option ) ->   ( 'value0, never ) t ->   'msg0 Tea_cmd.t
```

### `val perform`

``` ocaml
val perform : ( 'value -> 'msg ) -> ( 'value0, never ) t -> 'msg0 Tea_cmd.t
```

### `val attemptOpt`

``` ocaml
val attemptOpt :    ( ( 'succeed, 'fail ) Tea_result.t -> 'msg option ) ->   ( 'succeed0, 'fail0 ) t ->   'msg0 Tea_cmd.t
```

### `val attempt`

``` ocaml
val attempt :    ( ( 'succeed, 'fail ) Tea_result.t -> 'msg ) ->   ( 'succeed0, 'fail0 ) t ->   'msg0 Tea_cmd.t
```

### `val ignore`

``` ocaml
val ignore : ( 'a, 'b ) t -> 'c Tea_cmd.t
```

### `val succeed`

``` ocaml
val succeed : 'v -> ( 'v0, 'e ) t
```

### `val fail`

``` ocaml
val fail : 'v -> ( 'e, 'v0 ) t
```

### `val nativeBinding`

``` ocaml
val nativeBinding :    ( ( ( 'succeed, 'fail ) Tea_result.t -> unit ) -> unit ) ->   ( 'succeed0, 'fail0 ) t
```

### `val andThen`

``` ocaml
val andThen : ( 'a -> ( 'b, 'c ) t ) -> ( 'd, 'e ) t -> ( 'f, 'g ) t
```

### `val onError`

``` ocaml
val onError : ( 'a -> ( 'b, 'c ) t ) -> ( 'd, 'e ) t -> ( 'f, 'g ) t
```

### `val fromResult`

``` ocaml
val fromResult :    ( 'success, 'failure ) Tea_result.t ->   ( 'success, 'failure ) t
```

### `val mapError`

``` ocaml
val mapError : ( 'a -> 'b ) -> ( 'c, 'd ) t -> ( 'c, 'e ) t
```

### `val toOption`

``` ocaml
val toOption : ( 'a, 'b ) t -> ( 'c option, 'd ) t
```

### `val map`

``` ocaml
val map : ( 'a -> 'b ) -> ( 'c, 'd ) t -> ( 'e, 'd ) t
```

### `val map2`

``` ocaml
val map2 : ( 'a -> 'b -> 'c ) -> ( 'd, 'e ) t -> ( 'f, 'g ) t -> ( 'h, 'e ) t
```

### `val map3`

``` ocaml
val map3 :    ( 'a -> 'b -> 'c -> 'd ) ->   ( 'e, 'f ) t ->   ( 'g, 'h ) t ->   ( 'i, 'j ) t ->   ( 'k, 'f ) t
```

### `val map4`

``` ocaml
val map4 :    ( 'a -> 'b -> 'c -> 'd -> 'e ) ->   ( 'f, 'g ) t ->   ( 'h, 'i ) t ->   ( 'j, 'k ) t ->   ( 'l, 'm ) t ->   ( 'n, 'g ) t
```

### `val map5`

``` ocaml
val map5 :    ( 'a -> 'b -> 'c -> 'd -> 'e -> 'f ) ->   ( 'g, 'h ) t ->   ( 'i, 'j ) t ->   ( 'k, 'l ) t ->   ( 'm, 'n ) t ->   ( 'o, 'p ) t ->   ( 'q, 'h ) t
```

### `val map6`

``` ocaml
val map6 :    ( 'a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g ) ->   ( 'h, 'i ) t ->   ( 'j, 'k ) t ->   ( 'l, 'm ) t ->   ( 'n, 'o ) t ->   ( 'p, 'q ) t ->   ( 'r, 's ) t ->   ( 't, 'i ) t
```

### `val sequence`

``` ocaml
val sequence : ( 'a, 'b ) t list -> ( 'c list, 'b ) t
```

### `val testing_deop`

``` ocaml
val testing_deop : bool Stdlib.ref
```

### `val testing`

``` ocaml
val testing : unit -> unit
```
