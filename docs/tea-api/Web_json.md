# Module `Web_json`

`include module type of struct include Js.Json end`

### `type t`

``` ocaml
type t = Js_json.t
```

### `type kind`

``` ocaml
type !'a kind = 'a Js_json.kind = 
```

### `type tagged_t`

``` ocaml
type tagged_t = Js_json.tagged_t = 
```

### `val classify`

``` ocaml
val classify : t -> tagged_t
```

### `val test`

``` ocaml
val test : 'a -> 'b kind -> bool
```

### `val decodeString`

``` ocaml
val decodeString : t -> Js_string.t option
```

### `val decodeNumber`

``` ocaml
val decodeNumber : t -> float option
```

### `val decodeObject`

``` ocaml
val decodeObject : t -> t Js_dict.t option
```

### `val decodeArray`

``` ocaml
val decodeArray : t -> t array option
```

### `val decodeBoolean`

``` ocaml
val decodeBoolean : t -> bool option
```

### `val decodeNull`

``` ocaml
val decodeNull : t -> 'a Js_null.t option
```

### `val string`

``` ocaml
val string : string -> t
```

### `val number`

``` ocaml
val number : float -> t
```

### `val boolean`

``` ocaml
val boolean : bool -> t
```

### `val object_`

``` ocaml
val object_ : t Js_dict.t -> t
```

### `val array`

``` ocaml
val array : t array -> t
```

### `val stringArray`

``` ocaml
val stringArray : string array -> t
```

### `val numberArray`

``` ocaml
val numberArray : float array -> t
```

### `val booleanArray`

``` ocaml
val booleanArray : bool array -> t
```

### `val objectArray`

``` ocaml
val objectArray : t Js_dict.t array -> t
```

### `val parseExn`

``` ocaml
val parseExn : string -> t
```

### `val stringifyWithSpace`

``` ocaml
val stringifyWithSpace : t -> int -> string
```

### `val stringifyAny`

``` ocaml
val stringifyAny : 'a -> string option
```

### `val deserializeUnsafe`

``` ocaml
val deserializeUnsafe : string -> 'a
```

### `val serializeExn`

``` ocaml
val serializeExn : 'a -> string
```

### `type nothingYet`

``` ocaml
type nothingYet
```

### `val stringify`

``` ocaml
val stringify : 't -> nothingYet Js.null -> int -> string
```

### `val string_of_json`

``` ocaml
val string_of_json : ?indent:int -> 'a Js.Undefined.t -> string
```

### `val of_type`

``` ocaml
val of_type : 'a kind -> 'a0 -> t
```

### `val null`

``` ocaml
val null : Js_types.null_val
```
