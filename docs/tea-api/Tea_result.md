# Module `Tea_result`

### `type t`

``` ocaml
type ('a, 'b) t = ( 'a, 'b ) Stdlib.result = 
```

### `val result_to_option`

``` ocaml
val result_to_option : ( 'a, 'b ) t -> 'c option
```

### `val option_of_result`

``` ocaml
val option_of_result : ( 'a, 'b ) t -> 'c option
```

### `val ok`

``` ocaml
val ok : ( 'a, 'b ) t -> 'c option
```

### `val error`

``` ocaml
val error : ( 'a, 'b ) t -> 'c option
```

### `val last_of`

``` ocaml
val last_of : ( 'a, 'b ) t list -> ( 'a, 'b ) t
```

### `val accumulate`

``` ocaml
val accumulate : ( 'a, 'b ) t list -> ( 'c list, 'd ) t
```

### `val first`

``` ocaml
val first : ( 'a, 'b ) t -> ( 'c, 'd ) t -> ( 'a, 'b ) t
```

### `val error_of_any`

``` ocaml
val error_of_any : ( 'a, 'b ) t list -> 'c option
```

### `val error_of_first`

``` ocaml
val error_of_first : ( 'a, 'b ) t -> ( 'c, 'd ) t -> 'e option
```
