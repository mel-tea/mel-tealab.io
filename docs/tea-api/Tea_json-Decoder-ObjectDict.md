# Module `Decoder.ObjectDict`

### `type key`

``` ocaml
type key = Stdlib.String.t
```

### `type t`

``` ocaml
type !'a t = 'a Map.Make(Stdlib.String).t
```

### `val empty`

``` ocaml
val empty : 'a t
```

### `val is_empty`

``` ocaml
val is_empty : 'a t -> bool
```

### `val mem`

``` ocaml
val mem : key -> 'a t -> bool
```

### `val add`

``` ocaml
val add : key -> 'a -> 'a t -> 'a t
```

### `val update`

``` ocaml
val update : key -> ( 'a option -> 'a option ) -> 'a t -> 'a t
```

### `val singleton`

``` ocaml
val singleton : key -> 'a -> 'a t
```

### `val remove`

``` ocaml
val remove : key -> 'a t -> 'a t
```

### `val merge`

``` ocaml
val merge :    ( key -> 'a option -> 'b option -> 'c option ) ->   'a t ->   'b t ->   'c t
```

### `val union`

``` ocaml
val union : ( key -> 'a -> 'a -> 'a option ) -> 'a t -> 'a t -> 'a t
```

### `val compare`

``` ocaml
val compare : ( 'a -> 'a -> int ) -> 'a t -> 'a t -> int
```

### `val equal`

``` ocaml
val equal : ( 'a -> 'a -> bool ) -> 'a t -> 'a t -> bool
```

### `val iter`

``` ocaml
val iter : ( key -> 'a -> unit ) -> 'a t -> unit
```

### `val fold`

``` ocaml
val fold : ( key -> 'a -> 'b -> 'b ) -> 'a t -> 'b -> 'b
```

### `val for_all`

``` ocaml
val for_all : ( key -> 'a -> bool ) -> 'a t -> bool
```

### `val exists`

``` ocaml
val exists : ( key -> 'a -> bool ) -> 'a t -> bool
```

### `val filter`

``` ocaml
val filter : ( key -> 'a -> bool ) -> 'a t -> 'a t
```

### `val filter_map`

``` ocaml
val filter_map : ( key -> 'a -> 'b option ) -> 'a t -> 'b t
```

### `val partition`

``` ocaml
val partition : ( key -> 'a -> bool ) -> 'a t -> 'a t * 'a t
```

### `val cardinal`

``` ocaml
val cardinal : 'a t -> int
```

### `val bindings`

``` ocaml
val bindings : 'a t -> (key * 'a) list
```

### `val min_binding`

``` ocaml
val min_binding : 'a t -> key * 'a
```

### `val min_binding_opt`

``` ocaml
val min_binding_opt : 'a t -> (key * 'a) option
```

### `val max_binding`

``` ocaml
val max_binding : 'a t -> key * 'a
```

### `val max_binding_opt`

``` ocaml
val max_binding_opt : 'a t -> (key * 'a) option
```

### `val choose`

``` ocaml
val choose : 'a t -> key * 'a
```

### `val choose_opt`

``` ocaml
val choose_opt : 'a t -> (key * 'a) option
```

### `val split`

``` ocaml
val split : key -> 'a t -> 'a t * 'a option * 'a t
```

### `val find`

``` ocaml
val find : key -> 'a t -> 'a
```

### `val find_opt`

``` ocaml
val find_opt : key -> 'a t -> 'a option
```

### `val find_first`

``` ocaml
val find_first : ( key -> bool ) -> 'a t -> key * 'a
```

### `val find_first_opt`

``` ocaml
val find_first_opt : ( key -> bool ) -> 'a t -> (key * 'a) option
```

### `val find_last`

``` ocaml
val find_last : ( key -> bool ) -> 'a t -> key * 'a
```

### `val find_last_opt`

``` ocaml
val find_last_opt : ( key -> bool ) -> 'a t -> (key * 'a) option
```

### `val map`

``` ocaml
val map : ( 'a -> 'b ) -> 'a t -> 'b t
```

### `val mapi`

``` ocaml
val mapi : ( key -> 'a -> 'b ) -> 'a t -> 'b t
```

### `val to_seq`

``` ocaml
val to_seq : 'a t -> (key * 'a) Seq.t
```

### `val to_rev_seq`

``` ocaml
val to_rev_seq : 'a t -> (key * 'a) Seq.t
```

### `val to_seq_from`

``` ocaml
val to_seq_from : key -> 'a t -> (key * 'a) Seq.t
```

### `val add_seq`

``` ocaml
val add_seq : (key * 'a) Seq.t -> 'a t -> 'a t
```

### `val of_seq`

``` ocaml
val of_seq : (key * 'a) Seq.t -> 'a t
```
