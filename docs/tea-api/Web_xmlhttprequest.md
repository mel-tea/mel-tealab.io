# Module `Web_xmlhttprequest`

### `type unresolved`

``` ocaml
type unresolved
```

### `type xmlHttpRequestUpload`

``` ocaml
type xmlHttpRequestUpload
```

### `type event_readystatechange`

``` ocaml
type event_readystatechange = Web_json.t
```

### `type event_abort`

``` ocaml
type event_abort = Web_json.t
```

### `type event_error`

``` ocaml
type event_error = Web_json.t
```

### `type event_load`

``` ocaml
type event_load = Web_json.t
```

### `type event_loadstart`

``` ocaml
type event_loadstart = Web_json.t
```

### `type event_progress`

``` ocaml
type event_progress = Web_json.t
```

### `type event_timeout`

``` ocaml
type event_timeout = Web_json.t
```

### `type event_loadend`

``` ocaml
type event_loadend = Web_json.t
```

### `class type _xmlhttprequest`

``` ocaml
class type  _xmlhttprequest = object ... end
```

### `type t`

``` ocaml
type t = _xmlhttprequest Js.t
```

### `val create`

``` ocaml
val create : unit -> t
```

### `type errors`

``` ocaml
type errors = 
```

### `type body`

``` ocaml
type body = 
```

### `val abort`

``` ocaml
val abort : t -> unit
```

### `val getAllResponseHeaders`

``` ocaml
val getAllResponseHeaders : t -> ( string, errors ) Tea_result.t
```

### `val getAllResponseHeadersAsList`

``` ocaml
val getAllResponseHeadersAsList :    t ->   ( (string * string) list, errors ) Tea_result.t
```

### `val getAllResponseHeadersAsDict`

``` ocaml
val getAllResponseHeadersAsDict :    t ->   ( string Stdlib.Map.Make(Stdlib.String).t, errors ) Tea_result.t
```

### `val getResponseHeader`

``` ocaml
val getResponseHeader :    'a ->   < getResponse : ( 'b -> 'c Js.Null.t ) Js_OO.Meth.arity1.. > Js.t ->   'c option
```

### `val open_`

``` ocaml
val open_ :    string ->   string ->   ?async:bool ->   ?user:string ->   ?password:string ->   < _open :      ( string -> string -> bool -> string -> string -> 'a ) Js_OO.Meth.arity5.. >     Js.t ->   'b
```

### `val overrideMimeType`

``` ocaml
val overrideMimeType : string -> t -> unit
```

### `val send`

``` ocaml
val send : body -> t -> unit
```

### `val setRequestHeader`

``` ocaml
val setRequestHeader : string -> string -> t -> unit
```

### `type state`

``` ocaml
type state = 
```

### `type responseType`

``` ocaml
type responseType = 
```

### `type responseBody`

``` ocaml
type responseBody = 
```

### `val set_onreadystatechange`

``` ocaml
val set_onreadystatechange : ( event_readystatechange -> unit ) -> t -> unit
```

### `val get_onreadystatechange`

``` ocaml
val get_onreadystatechange : t -> event_readystatechange -> unit
```

### `val readyState`

``` ocaml
val readyState : t -> state
```

### `val set_responseType`

``` ocaml
val set_responseType : responseType -> t -> unit
```

### `val get_responseType`

``` ocaml
val get_responseType : t -> responseType
```

### `val get_response`

``` ocaml
val get_response : t -> responseBody
```

### `val get_responseText`

``` ocaml
val get_responseText : t -> string
```

### `val get_responseURL`

``` ocaml
val get_responseURL : t -> string
```

### `val get_responseXML`

``` ocaml
val get_responseXML : t -> Web_document.t option
```

### `val get_status`

``` ocaml
val get_status : t -> int
```

### `val get_statusText`

``` ocaml
val get_statusText : t -> string
```

### `val set_timeout`

``` ocaml
val set_timeout : float -> t -> unit
```

### `val get_timeout`

``` ocaml
val get_timeout : t -> float
```

### `val set_withCredentials`

``` ocaml
val set_withCredentials : bool -> t -> unit
```

### `val get_withCredentials`

``` ocaml
val get_withCredentials : t -> bool
```

### `val set_onabort`

``` ocaml
val set_onabort : ( event_abort -> unit ) -> t -> unit
```

### `val get_onabort`

``` ocaml
val get_onabort : t -> event_abort -> unit
```

### `val set_onerror`

``` ocaml
val set_onerror : ( event_error -> unit ) -> t -> unit
```

### `val get_onerror`

``` ocaml
val get_onerror : t -> event_error -> unit
```

### `val set_onload`

``` ocaml
val set_onload : ( event_load -> unit ) -> t -> unit
```

### `val get_onload`

``` ocaml
val get_onload : t -> event_load -> unit
```

### `val set_onloadstart`

``` ocaml
val set_onloadstart : ( event_loadstart -> unit ) -> t -> unit
```

### `val get_onloadstart`

``` ocaml
val get_onloadstart : t -> event_loadstart -> unit
```

### `val set_onprogress`

``` ocaml
val set_onprogress : ( event_loadstart -> unit ) -> t -> unit
```

### `val get_onprogress`

``` ocaml
val get_onprogress : t -> event_loadstart -> unit
```

### `val set_ontimeout`

``` ocaml
val set_ontimeout : ( event_timeout -> unit ) -> t -> unit
```

### `val get_ontimeout`

``` ocaml
val get_ontimeout : t -> event_timeout -> unit
```

### `val set_onloadend`

``` ocaml
val set_onloadend : ( event_loadend -> unit ) -> t -> unit
```

### `val get_onloadend`

``` ocaml
val get_onloadend : t -> event_loadend -> unit
```
