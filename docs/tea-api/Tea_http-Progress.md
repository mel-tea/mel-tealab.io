# Module `Tea_http.Progress`

### `type t`

``` ocaml
type t = {
```

### `val emptyProgress`

``` ocaml
val emptyProgress : t
```

### `val track`

``` ocaml
val track : ( t -> 'a ) -> ( 'b, 'c ) request -> ( 'd, 'e ) request
```
