# Module `Web_date`

### `type t`

``` ocaml
type t = <  > Js.t
```

### `type date_obj`

``` ocaml
type date_obj = < now : float Js_OO.Meth.arity0 > Js.t
```

### `val create_date`

``` ocaml
val create_date : unit -> t
```

### `val date_obj`

``` ocaml
val date_obj : date_obj
```

### `val now`

``` ocaml
val now : unit -> float
```
