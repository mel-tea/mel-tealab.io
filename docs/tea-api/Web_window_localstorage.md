# Module `Web_window_localstorage`

### `type t`

``` ocaml
type t =   < length : int     ; clear : unit Js_OO.Meth.arity0     ; key : ( int -> string ) Js_OO.Meth.arity1     ; getItem : ( string -> string ) Js_OO.Meth.arity1     ; removeItem : ( string -> unit ) Js_OO.Meth.arity1     ; setItem : ( string -> string -> unit ) Js_OO.Meth.arity2 >     Js.t
```

### `val length`

``` ocaml
val length :    < localStorage : < length : 'a.. > Js.t Js.Undefined.t.. > Js.t ->   'b option
```

### `val clear`

``` ocaml
val clear :    < localStorage : < clear : 'a Js_OO.Meth.arity0.. > Js.t Js.Undefined.t.. >     Js.t ->   'b option
```

### `val key`

``` ocaml
val key :    < localStorage :      < key : ( 'a -> 'b ) Js_OO.Meth.arity1.. > Js.t Js.Undefined.t.. >     Js.t ->   'c ->   'd option
```

### `val getItem`

``` ocaml
val getItem :    < localStorage :      < getItem : ( 'a -> 'b ) Js_OO.Meth.arity1.. > Js.t Js.Undefined.t.. >     Js.t ->   'c ->   'd option
```

### `val removeItem`

``` ocaml
val removeItem :    < localStorage :      < removeItem : ( 'a -> 'b ) Js_OO.Meth.arity1.. > Js.t Js.Undefined.t.. >     Js.t ->   'c ->   'd option
```

### `val setItem`

``` ocaml
val setItem :    < localStorage :      < setItem : ( 'a -> 'b -> 'c ) Js_OO.Meth.arity2.. > Js.t Js.Undefined.t.. >     Js.t ->   'd ->   'e ->   'f option
```
