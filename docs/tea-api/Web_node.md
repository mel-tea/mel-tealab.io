# Module `Web_node`

### `type style`

``` ocaml
type style =   < setProperty : Web_json.t Js.undefined     ; setProperty__ :        ( string -> string Js.null -> string Js.null -> unit ) Js_OO.Meth.arity3 >     Js.t
```

### `type t`

``` ocaml
type t =   < style : style     ; value#= : ( string Js.undefined -> unit ) Js_OO.Meth.arity1     ; value : string Js.undefined     ; checked#= : ( bool Js.undefined -> unit ) Js_OO.Meth.arity1     ; checked : bool Js.undefined     ; childNodes : t Js.Array.t     ; firstChild : t Js.Null.t     ; appendChild : ( t -> t ) Js_OO.Meth.arity1     ; removeChild : ( t -> t ) Js_OO.Meth.arity1     ; insertBefore : ( t -> t -> t ) Js_OO.Meth.arity2     ; remove : unit Js_OO.Meth.arity0     ; setAttributeNS : ( string -> string -> string -> unit ) Js_OO.Meth.arity3     ; setAttribute : ( string -> string -> unit ) Js_OO.Meth.arity2     ; removeAttributeNS : ( string -> string -> unit ) Js_OO.Meth.arity2     ; removeAttribute : ( string -> unit ) Js_OO.Meth.arity1     ; addEventListener :        ( string ->         t Web_event.cb ->         Web_event.options ->         unit )         Js_OO.Meth.arity3     ; removeEventListener :        ( string ->         t Web_event.cb ->         Web_event.options ->         unit )         Js_OO.Meth.arity3     ; focus : unit Js_OO.Meth.arity0     ; nodeValue#= : ( string -> unit ) Js_OO.Meth.arity1     ; nodeValue : string Js.null >     Js.t
```

### `val document_node`

``` ocaml
val document_node : t
```

### `type event`

``` ocaml
type event = t Web_event.t
```

### `type event_cb`

``` ocaml
type event_cb = t Web_event.cb
```

### `val getProp_asEventListener`

``` ocaml
val getProp_asEventListener : t -> 'key -> t Web_event.cb Js.undefined
```

### `val setProp_asEventListener`

``` ocaml
val setProp_asEventListener : t -> 'key -> t Web_event.cb Js.undefined -> unit
```

### `val getProp`

``` ocaml
val getProp : t -> 'key -> 'value
```

### `val setProp`

``` ocaml
val setProp : t -> 'key -> 'value -> unit
```

### `val style`

``` ocaml
val style : < style : 'a.. > Js.t -> 'b
```

### `val getStyle`

``` ocaml
val getStyle : < style : style.. > Js.t -> string -> string Js.null
```

### `val setStyle`

``` ocaml
val setStyle : < style : style.. > Js.t -> string -> string Js.null -> unit
```

### `val setStyleProperty`

``` ocaml
val setStyleProperty :    < style : style.. > Js.t Js.t ->   ?priority:bool ->   string ->   string Js.null ->   unit
```

### `val childNodes`

``` ocaml
val childNodes : < childNodes : 'a.. > Js.t -> 'b
```

### `val firstChild`

``` ocaml
val firstChild : < firstChild : 'a.. > Js.t -> 'b
```

### `val appendChild`

``` ocaml
val appendChild :    < appendChild : ( 'a -> 'b ) Js_OO.Meth.arity1.. > Js.t ->   'c ->   'd
```

### `val removeChild`

``` ocaml
val removeChild :    < removeChild : ( 'a -> 'b ) Js_OO.Meth.arity1.. > Js.t ->   'c ->   'd
```

### `val insertBefore`

``` ocaml
val insertBefore :    < insertBefore : ( 'a -> 'b -> 'c ) Js_OO.Meth.arity2.. > Js.t ->   'd ->   'e ->   'f
```

### `val remove`

``` ocaml
val remove : < remove : ( 'a -> 'b ) Js_OO.Meth.arity1.. > Js.t -> 'c -> 'd
```

### `val setAttributeNS`

``` ocaml
val setAttributeNS :    < setAttributeNS : ( 'a -> 'b -> 'c -> 'd ) Js_OO.Meth.arity3.. > Js.t ->   'e ->   'f ->   'g ->   'h
```

### `val setAttribute`

``` ocaml
val setAttribute :    < setAttribute : ( 'a -> 'b -> 'c ) Js_OO.Meth.arity2.. > Js.t ->   'd ->   'e ->   'f
```

### `val setAttributeNsOptional`

``` ocaml
val setAttributeNsOptional :    < setAttribute : ( 'a -> 'b -> 'c ) Js_OO.Meth.arity2     ; setAttributeNS : ( string -> 'd -> 'e -> 'f ) Js_OO.Meth.arity3.. >     Js.t ->   string ->   'g ->   'h ->   'i
```

### `val removeAttributeNS`

``` ocaml
val removeAttributeNS :    < removeAttributeNS : ( 'a -> 'b -> 'c ) Js_OO.Meth.arity2.. > Js.t ->   'd ->   'e ->   'f
```

### `val removeAttribute`

``` ocaml
val removeAttribute :    < removeAttribute : ( 'a -> 'b ) Js_OO.Meth.arity1.. > Js.t ->   'c ->   'd
```

### `val removeAttributeNsOptional`

``` ocaml
val removeAttributeNsOptional :    < removeAttribute : ( 'a -> 'b ) Js_OO.Meth.arity1     ; removeAttributeNS : ( string -> 'c -> 'd ) Js_OO.Meth.arity2.. >     Js.t ->   string ->   'e ->   'f
```

### `val addEventListener`

``` ocaml
val addEventListener :    < addEventListener : ( 'a -> 'b -> 'c -> 'd ) Js_OO.Meth.arity3.. > Js.t ->   'e ->   'f ->   'g ->   'h
```

### `val removeEventListener`

``` ocaml
val removeEventListener :    < removeEventListener : ( 'a -> 'b -> 'c -> 'd ) Js_OO.Meth.arity3.. > Js.t ->   'e ->   'f ->   'g ->   'h
```

### `val focus`

``` ocaml
val focus : < focus : 'a Js_OO.Meth.arity0.. > Js.t -> 'b
```

### `val set_nodeValue`

``` ocaml
val set_nodeValue :    < nodeValue#= : ( 'a -> unit ) Js_OO.Meth.arity1.. > Js.t ->   'b ->   unit
```

### `val get_nodeValue`

``` ocaml
val get_nodeValue : < nodeValue : 'a.. > Js.t -> 'b
```

### `val remove_polyfill`

``` ocaml
val remove_polyfill : unit -> unit
```
