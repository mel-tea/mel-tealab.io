# Module `Web_location`

### `type t`

``` ocaml
type t =   < href#= : ( string -> unit ) Js_OO.Meth.arity1     ; href : string     ; protocol#= : ( string -> unit ) Js_OO.Meth.arity1     ; protocol : string     ; host#= : ( string -> unit ) Js_OO.Meth.arity1     ; host : string     ; hostname#= : ( string -> unit ) Js_OO.Meth.arity1     ; hostname : string     ; port#= : ( string -> unit ) Js_OO.Meth.arity1     ; port : string     ; pathname#= : ( string -> unit ) Js_OO.Meth.arity1     ; pathname : string     ; search#= : ( string -> unit ) Js_OO.Meth.arity1     ; search : string     ; hash#= : ( string -> unit ) Js_OO.Meth.arity1     ; hash : string     ; username#= : ( string -> unit ) Js_OO.Meth.arity1     ; username : string     ; password#= : ( string -> unit ) Js_OO.Meth.arity1     ; password : string     ; origin : string >     Js.t
```

### `val getHref`

``` ocaml
val getHref : < href : 'a.. > Js.t -> 'b
```

### `val setHref`

``` ocaml
val setHref :    < href#= : ( 'a -> unit ) Js_OO.Meth.arity1.. > Js.t ->   'b ->   unit
```

### `val getProtocol`

``` ocaml
val getProtocol : < protocol : 'a.. > Js.t -> 'b
```

### `val setProtocol`

``` ocaml
val setProtocol :    < protocol#= : ( 'a -> unit ) Js_OO.Meth.arity1.. > Js.t ->   'b ->   unit
```

### `val getHost`

``` ocaml
val getHost : < host : 'a.. > Js.t -> 'b
```

### `val setHost`

``` ocaml
val setHost :    < host#= : ( 'a -> unit ) Js_OO.Meth.arity1.. > Js.t ->   'b ->   unit
```

### `val getHostname`

``` ocaml
val getHostname : < hostname : 'a.. > Js.t -> 'b
```

### `val setHostname`

``` ocaml
val setHostname :    < hostname#= : ( 'a -> unit ) Js_OO.Meth.arity1.. > Js.t ->   'b ->   unit
```

### `val getPort`

``` ocaml
val getPort : < port : 'a.. > Js.t -> 'b
```

### `val setPort`

``` ocaml
val setPort :    < port#= : ( 'a -> unit ) Js_OO.Meth.arity1.. > Js.t ->   'b ->   unit
```

### `val getPathname`

``` ocaml
val getPathname : < pathname : 'a.. > Js.t -> 'b
```

### `val setPathname`

``` ocaml
val setPathname :    < pathname#= : ( 'a -> unit ) Js_OO.Meth.arity1.. > Js.t ->   'b ->   unit
```

### `val getSearch`

``` ocaml
val getSearch : < search : 'a.. > Js.t -> 'b
```

### `val setSearch`

``` ocaml
val setSearch :    < search#= : ( 'a -> unit ) Js_OO.Meth.arity1.. > Js.t ->   'b ->   unit
```

### `val getHash`

``` ocaml
val getHash : < hash : 'a.. > Js.t -> 'b
```

### `val setHash`

``` ocaml
val setHash :    < hash#= : ( 'a -> unit ) Js_OO.Meth.arity1.. > Js.t ->   'b ->   unit
```

### `val getUsername`

``` ocaml
val getUsername : < username : 'a.. > Js.t -> 'b
```

### `val setUsername`

``` ocaml
val setUsername :    < username#= : ( 'a -> unit ) Js_OO.Meth.arity1.. > Js.t ->   'b ->   unit
```

### `val getPassword`

``` ocaml
val getPassword : < password : 'a.. > Js.t -> 'b
```

### `val setPassword`

``` ocaml
val setPassword :    < password#= : ( 'a -> unit ) Js_OO.Meth.arity1.. > Js.t ->   'b ->   unit
```

### `val getOrigin`

``` ocaml
val getOrigin : < origin : 'a.. > Js.t -> 'b
```

### `type location`

``` ocaml
type location = {
```

### `val asRecord`

``` ocaml
val asRecord :    < hash : string     ; host : string     ; hostname : string     ; href : string     ; origin : string     ; password : string     ; pathname : string     ; port : string     ; protocol : string     ; search : string     ; username : string.. >     Js.t ->   location
```
