# Module `Web_event`

### `type t`

``` ocaml
type 'node t =   < target : 'node Js.undefined     ; keyCode : int     ; preventDefault : unit Js_OO.Meth.arity0     ; stopPropagation : unit Js_OO.Meth.arity0 >     Js.t
```

### `type cb`

``` ocaml
type 'node cb = ( 'node t -> unit ) Js.Fn.arity1
```

### `type options`

``` ocaml
type options = bool
```

### `type popstateEvent`

``` ocaml
type popstateEvent = <  > Js.t
```

### `type popstateCb`

``` ocaml
type popstateCb = ( popstateEvent -> unit ) Js.Fn.arity1
```
