# Module `Tea_html2`

This file is organized roughly in order of popularity. The tags which
you'd expect to use frequently will be closer to the top.

-   [Primitives](#primitives)
-   [Tags](#tags)
    -   [Headers](#headers)
    -   [Grouping Content](#grouping-content)
    -   [Text](#text)
    -   [Lists](#lists)
    -   [Embedded Content](#embedded-content)
    -   [Form and inputs](#form-and-inputs)
    -   [Sections](#sections)
    -   [Figures](#figures)
    -   [Tables](#tables)
    -   [Less common inputs](#less-common-inputs)
    -   [Audio and Video](#audio-and-video)
    -   [Embedded objects](#embedded-objects)
    -   [Text edits](#text-edits)
    -   [Semantic text](#semantic-text)
    -   [Less common text tags](#less-common-text-tags)
    -   [Interactive elements](#interactive-elements)
    -   [Header elements](#header-elements)

### `module Cmds`

``` ocaml
module Cmds = Tea_html_cmds
```

### `val map`

``` ocaml
val map : ( 'a -> 'b ) -> 'a Vdom.t -> 'b Vdom.t
```

## [](#primitives)Primitives

### `val text`

``` ocaml
val text : string -> 'a Vdom.t
```

### `val node`

``` ocaml
val node :    ?namespace:string ->   string ->   ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val noNode`

``` ocaml
val noNode : 'a Vdom.t
```

### `val lazy1`

``` ocaml
val lazy1 : string -> ( unit -> 'a Vdom.t ) -> 'a Vdom.t
```

## [](#tags)Tags

### [](#headers)Headers

### `val h1`

``` ocaml
val h1 :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val h2`

``` ocaml
val h2 :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val h3`

``` ocaml
val h3 :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val h4`

``` ocaml
val h4 :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val h5`

``` ocaml
val h5 :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val h6`

``` ocaml
val h6 :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#grouping-content)Grouping Content

### `val div`

``` ocaml
val div :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val p`

``` ocaml
val p :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val hr`

``` ocaml
val hr :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val pre`

``` ocaml
val pre :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val blockquote`

``` ocaml
val blockquote :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#text)Text

### `val span`

``` ocaml
val span :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val a`

``` ocaml
val a :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val code`

``` ocaml
val code :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val em`

``` ocaml
val em :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val strong`

``` ocaml
val strong :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val i`

``` ocaml
val i :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val b`

``` ocaml
val b :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val u`

``` ocaml
val u :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val sub`

``` ocaml
val sub :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val sup`

``` ocaml
val sup :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val br`

``` ocaml
val br : 'a Vdom.properties -> 'a Vdom.t
```

### `val br'`

``` ocaml
val br' :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#lists)Lists

### `val ol`

``` ocaml
val ol :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val ul`

``` ocaml
val ul :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val li`

``` ocaml
val li :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val dl`

``` ocaml
val dl :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val dt`

``` ocaml
val dt :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val dd`

``` ocaml
val dd :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#embedded-content)Embedded Content

### `val img`

``` ocaml
val img :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val iframe`

``` ocaml
val iframe :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val canvas`

``` ocaml
val canvas :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val math`

``` ocaml
val math :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#form-and-inputs)Form and inputs

### `val form`

``` ocaml
val form :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val input'`

``` ocaml
val input' :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val textarea`

``` ocaml
val textarea :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val button`

``` ocaml
val button :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val select`

``` ocaml
val select :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val option'`

``` ocaml
val option' :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val optgroup`

``` ocaml
val optgroup :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val label`

``` ocaml
val label :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val fieldset`

``` ocaml
val fieldset :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val legend`

``` ocaml
val legend :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#sections)Sections

### `val section`

``` ocaml
val section :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val nav`

``` ocaml
val nav :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val article`

``` ocaml
val article :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val aside`

``` ocaml
val aside :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val header`

``` ocaml
val header :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val footer`

``` ocaml
val footer :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val address`

``` ocaml
val address :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val main`

``` ocaml
val main :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val body`

``` ocaml
val body :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#figures)Figures

### `val figure`

``` ocaml
val figure :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val figcaption`

``` ocaml
val figcaption :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#tables)Tables

### `val table`

``` ocaml
val table :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val caption`

``` ocaml
val caption :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val colgroup`

``` ocaml
val colgroup :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val col`

``` ocaml
val col :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val tbody`

``` ocaml
val tbody :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val thead`

``` ocaml
val thead :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val tfoot`

``` ocaml
val tfoot :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val tr`

``` ocaml
val tr :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val th`

``` ocaml
val th :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val td`

``` ocaml
val td :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#less-common-inputs)Less common inputs

### `val datalist`

``` ocaml
val datalist :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val keygen`

``` ocaml
val keygen :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val output`

``` ocaml
val output :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val progress`

``` ocaml
val progress :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val meter`

``` ocaml
val meter :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#audio-and-video)Audio and Video

### `val audio`

``` ocaml
val audio :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val video`

``` ocaml
val video :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val source`

``` ocaml
val source :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val track`

``` ocaml
val track :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#embedded-objects)Embedded objects

### `val embed`

``` ocaml
val embed :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val object'`

``` ocaml
val object' :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val param`

``` ocaml
val param :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#text-edits)Text edits

### `val ins`

``` ocaml
val ins :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val del`

``` ocaml
val del :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#semantic-text)Semantic text

### `val small`

``` ocaml
val small :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val cite`

``` ocaml
val cite :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val dfn`

``` ocaml
val dfn :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val abbr`

``` ocaml
val abbr :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val time`

``` ocaml
val time :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val var'`

``` ocaml
val var' :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val samp`

``` ocaml
val samp :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val kbd`

``` ocaml
val kbd :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val s`

``` ocaml
val s :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val q`

``` ocaml
val q :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#less-common-text-tags)Less common text tags

### `val mark`

``` ocaml
val mark :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val ruby`

``` ocaml
val ruby :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val rt`

``` ocaml
val rt :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val rp`

``` ocaml
val rp :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val bdi`

``` ocaml
val bdi :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val bdo`

``` ocaml
val bdo :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val wbr`

``` ocaml
val wbr :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#interactive-elements)Interactive elements

### `val details`

``` ocaml
val details :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val summary`

``` ocaml
val summary :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val menuitem`

``` ocaml
val menuitem :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val menu`

``` ocaml
val menu :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### [](#header-elements)Header elements

### `val meta`

``` ocaml
val meta : ?key:string -> ?unique:string -> 'a Vdom.properties -> 'a Vdom.t
```

### `val style`

``` ocaml
val style :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   string ->   'a Vdom.t
```

### `val title`

``` ocaml
val title :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   string ->   'a Vdom.t
```

### `val link`

``` ocaml
val link : ?key:string -> ?unique:string -> 'a Vdom.properties -> 'a Vdom.t
```

### `module Attributes`

``` ocaml
module Attributes : sig ... end
```

Helper functions for HTML attributes. They are organized roughly by
category.

### `module Events`

``` ocaml
module Events : sig ... end
```
