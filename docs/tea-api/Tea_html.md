# Module `Tea_html`

### `module Cmds`

``` ocaml
module Cmds = Tea_html_cmds
```

### `val map`

``` ocaml
val map : ( 'a -> 'b ) -> 'a Vdom.t -> 'b Vdom.t
```

### `val noNode`

``` ocaml
val noNode : 'a Vdom.t
```

### `val text`

``` ocaml
val text : string -> 'a Vdom.t
```

### `val lazy1`

``` ocaml
val lazy1 : string -> ( unit -> 'a Vdom.t ) -> 'a Vdom.t
```

### `val node`

``` ocaml
val node :    ?namespace:string ->   string ->   ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val br`

``` ocaml
val br : 'a Vdom.properties -> 'a Vdom.t
```

### `val br'`

``` ocaml
val br' :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val div`

``` ocaml
val div :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val span`

``` ocaml
val span :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val p`

``` ocaml
val p :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val pre`

``` ocaml
val pre :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val a`

``` ocaml
val a :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val section`

``` ocaml
val section :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val header`

``` ocaml
val header :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val footer`

``` ocaml
val footer :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val h1`

``` ocaml
val h1 :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val h2`

``` ocaml
val h2 :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val h3`

``` ocaml
val h3 :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val h4`

``` ocaml
val h4 :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val h5`

``` ocaml
val h5 :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val h6`

``` ocaml
val h6 :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val i`

``` ocaml
val i :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val strong`

``` ocaml
val strong :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val button`

``` ocaml
val button :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val input'`

``` ocaml
val input' :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val textarea`

``` ocaml
val textarea :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val label`

``` ocaml
val label :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val ul`

``` ocaml
val ul :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val ol`

``` ocaml
val ol :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val li`

``` ocaml
val li :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val table`

``` ocaml
val table :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val thead`

``` ocaml
val thead :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val tfoot`

``` ocaml
val tfoot :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val tbody`

``` ocaml
val tbody :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val th`

``` ocaml
val th :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val tr`

``` ocaml
val tr :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val td`

``` ocaml
val td :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val progress`

``` ocaml
val progress :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val img`

``` ocaml
val img :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val select`

``` ocaml
val select :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val option'`

``` ocaml
val option' :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val form`

``` ocaml
val form :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val nav`

``` ocaml
val nav :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val main`

``` ocaml
val main :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val aside`

``` ocaml
val aside :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val article`

``` ocaml
val article :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val details`

``` ocaml
val details :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val figcaption`

``` ocaml
val figcaption :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val figure`

``` ocaml
val figure :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val mark`

``` ocaml
val mark :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val summary`

``` ocaml
val summary :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val time`

``` ocaml
val time :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val hr`

``` ocaml
val hr :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val blockquote`

``` ocaml
val blockquote :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val code`

``` ocaml
val code :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val em`

``` ocaml
val em :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val b`

``` ocaml
val b :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val u`

``` ocaml
val u :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val sub`

``` ocaml
val sub :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val sup`

``` ocaml
val sup :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val dl`

``` ocaml
val dl :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val dt`

``` ocaml
val dt :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val dd`

``` ocaml
val dd :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val iframe`

``` ocaml
val iframe :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val canvas`

``` ocaml
val canvas :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val address`

``` ocaml
val address :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val caption`

``` ocaml
val caption :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val colgroup`

``` ocaml
val colgroup :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val col`

``` ocaml
val col :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val fieldset`

``` ocaml
val fieldset :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val legend`

``` ocaml
val legend :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val datalist`

``` ocaml
val datalist :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val optgroup`

``` ocaml
val optgroup :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val output`

``` ocaml
val output :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val meter`

``` ocaml
val meter :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val audio`

``` ocaml
val audio :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val video`

``` ocaml
val video :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val source`

``` ocaml
val source :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val track`

``` ocaml
val track :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val embed`

``` ocaml
val embed :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val object'`

``` ocaml
val object' :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val param`

``` ocaml
val param :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val ins`

``` ocaml
val ins :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val del`

``` ocaml
val del :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val small`

``` ocaml
val small :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val cite`

``` ocaml
val cite :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val dfn`

``` ocaml
val dfn :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val abbr`

``` ocaml
val abbr :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val var'`

``` ocaml
val var' :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val samp`

``` ocaml
val samp :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val kbd`

``` ocaml
val kbd :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val s`

``` ocaml
val s :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val q`

``` ocaml
val q :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val rt`

``` ocaml
val rt :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val bdi`

``` ocaml
val bdi :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val bdo`

``` ocaml
val bdo :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val wbr`

``` ocaml
val wbr :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val menuitem`

``` ocaml
val menuitem :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val menu`

``` ocaml
val menu :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val noProp`

``` ocaml
val noProp : 'a Vdom.property
```

### `val id`

``` ocaml
val id : string -> 'a Vdom.property
```

### `val href`

``` ocaml
val href : string -> 'a Vdom.property
```

### `val src`

``` ocaml
val src : string -> 'a Vdom.property
```

### `val title`

``` ocaml
val title : string -> 'a Vdom.property
```

### `val class'`

``` ocaml
val class' : string -> 'a Vdom.property
```

### `val classList`

``` ocaml
val classList : (string * bool) list -> 'a Vdom.property
```

### `val type'`

``` ocaml
val type' : string -> 'a Vdom.property
```

### `val style`

``` ocaml
val style : string -> string -> 'a Vdom.property
```

### `val styles`

``` ocaml
val styles : (string * string) list -> 'a Vdom.property
```

### `val placeholder`

``` ocaml
val placeholder : string -> 'a Vdom.property
```

### `val autofocus`

``` ocaml
val autofocus : bool -> 'a Vdom.property
```

### `val value`

``` ocaml
val value : string -> 'a Vdom.property
```

### `val name`

``` ocaml
val name : string -> 'a Vdom.property
```

### `val checked`

``` ocaml
val checked : bool -> 'a Vdom.property
```

### `val for'`

``` ocaml
val for' : string -> 'a Vdom.property
```

### `val hidden`

``` ocaml
val hidden : bool -> 'a Vdom.property
```

### `val target`

``` ocaml
val target : string -> 'a Vdom.property
```

### `val action`

``` ocaml
val action : string -> 'a Vdom.property
```

### `val method'`

``` ocaml
val method' : string -> 'a Vdom.property
```

### `val onCB`

``` ocaml
val onCB :    string ->   string ->   ( Web.Node.event -> 'a option ) ->   'a Vdom.property
```

### `val onMsg`

``` ocaml
val onMsg : string -> 'a -> 'b Vdom.property
```

### `val onInputOpt`

``` ocaml
val onInputOpt : ?key:string -> ( string -> 'a option ) -> 'a Vdom.property
```

### `val onInput`

``` ocaml
val onInput : ?key:string -> ( string -> 'a ) -> 'b Vdom.property
```

### `val onChangeOpt`

``` ocaml
val onChangeOpt : ?key:string -> ( string -> 'a option ) -> 'a Vdom.property
```

### `val onChange`

``` ocaml
val onChange : ?key:string -> ( string -> 'a ) -> 'b Vdom.property
```

### `val onClick`

``` ocaml
val onClick : 'a -> 'b Vdom.property
```

### `val onDoubleClick`

``` ocaml
val onDoubleClick : 'a -> 'b Vdom.property
```

### `val onBlur`

``` ocaml
val onBlur : 'a -> 'b Vdom.property
```

### `val onFocus`

``` ocaml
val onFocus : 'a -> 'b Vdom.property
```

### `val onCheckOpt`

``` ocaml
val onCheckOpt : ?key:string -> ( bool -> 'a option ) -> 'a Vdom.property
```

### `val onCheck`

``` ocaml
val onCheck : ?key:string -> ( bool -> 'a ) -> 'b Vdom.property
```

### `val onMouseDown`

``` ocaml
val onMouseDown : 'a -> 'b Vdom.property
```

### `val onMouseUp`

``` ocaml
val onMouseUp : 'a -> 'b Vdom.property
```

### `val onMouseEnter`

``` ocaml
val onMouseEnter : 'a -> 'b Vdom.property
```

### `val onMouseLeave`

``` ocaml
val onMouseLeave : 'a -> 'b Vdom.property
```

### `val onMouseOver`

``` ocaml
val onMouseOver : 'a -> 'b Vdom.property
```

### `val onMouseOut`

``` ocaml
val onMouseOut : 'a -> 'b Vdom.property
```

### `type options`

``` ocaml
type options = {
```

### `val defaultOptions`

``` ocaml
val defaultOptions : options
```

### `val onWithOptions`

``` ocaml
val onWithOptions :    key:string ->   string ->   options ->   ( 'a, 'b ) Tea_json.Decoder.t ->   'c Vdom.property
```

### `val on`

``` ocaml
val on :    key:string ->   string ->   ( 'a, 'b ) Tea_json.Decoder.t ->   'b Vdom.property
```

### `val targetValue`

``` ocaml
val targetValue : ( Web.Json.t, string ) Tea_json.Decoder.t
```

### `val targetChecked`

``` ocaml
val targetChecked : ( Web.Json.t, bool ) Tea_json.Decoder.t
```

### `val keyCode`

``` ocaml
val keyCode : ( Web.Json.t, int ) Tea_json.Decoder.t
```

### `module Attributes`

``` ocaml
module Attributes : sig ... end
```
