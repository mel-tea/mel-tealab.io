# Module `Tea_http`

### `type response_status`

``` ocaml
type response_status = {
```

### `type requestBody`

``` ocaml
type requestBody = Web.XMLHttpRequest.body
```

### `type bodyType`

``` ocaml
type bodyType = Web.XMLHttpRequest.responseType
```

### `type responseBody`

``` ocaml
type responseBody = Web.XMLHttpRequest.responseBody
```

### `type response`

``` ocaml
type response = {
```

### `type error`

``` ocaml
type 'parsedata error = 
```

### `val string_of_error`

``` ocaml
val string_of_error : 'a error -> string
```

### `type header`

``` ocaml
type header = 
```

### `type expect`

``` ocaml
type 'res expect = 
```

### `type requestEvents`

``` ocaml
type 'msg requestEvents = {
```

### `val emptyRequestEvents`

``` ocaml
val emptyRequestEvents : 'a requestEvents
```

### `type rawRequest`

``` ocaml
type 'res rawRequest = {
```

### `type request`

``` ocaml
type ('msg, 'res) request = 
```

### `val expectStringResponse`

``` ocaml
val expectStringResponse :    ( string -> ( 'a, string ) Tea_result.t ) ->   'a expect
```

### `val expectString`

``` ocaml
val expectString : string expect
```

### `val request`

``` ocaml
val request : 'a rawRequest -> ( 'b, 'a ) request
```

### `val getString`

``` ocaml
val getString : string -> ( 'a, string ) request
```

### `val toTask`

``` ocaml
val toTask : ( 'a, 'b ) request -> ( 'c, string error ) Tea_task.t
```

### `val send`

``` ocaml
val send :    ( ( 'a, string error ) Tea_result.t -> 'b ) ->   ( 'c, 'd ) request ->   'e Tea_cmd.t
```

### `val encodeURIComponent`

``` ocaml
val encodeURIComponent : string -> string
```

### `val encodeUri`

``` ocaml
val encodeUri : string -> string
```

### `val decodeURIComponent`

``` ocaml
val decodeURIComponent : string -> string
```

### `val decodeUri`

``` ocaml
val decodeUri : string -> string option
```

### `module Progress`

``` ocaml
module Progress : sig ... end
```
