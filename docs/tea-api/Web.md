# Module `Web`

### `module Event`

``` ocaml
module Event = Web_event
```

### `module Node`

``` ocaml
module Node = Web_node
```

### `module Document`

``` ocaml
module Document = Web_document
```

### `module Date`

``` ocaml
module Date = Web_date
```

### `module Window`

``` ocaml
module Window = Web_window
```

### `module Location`

``` ocaml
module Location = Web_location
```

### `module Json`

``` ocaml
module Json = Web_json
```

### `module XMLHttpRequest`

``` ocaml
module XMLHttpRequest = Web_xmlhttprequest
```

### `module FormData`

``` ocaml
module FormData = Web_formdata
```

### `val polyfills`

``` ocaml
val polyfills : unit -> unit
```
