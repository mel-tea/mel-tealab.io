# Module `Tea_app`

### `type program`

``` ocaml
type ('flags, 'model, 'msg) program = {
```

### `type standardProgram`

``` ocaml
type ('flags, 'model, 'msg) standardProgram = {
```

### `type beginnerProgram`

``` ocaml
type ('model, 'msg) beginnerProgram = {
```

### `type pumpInterface`

``` ocaml
type ('model, 'msg) pumpInterface = {
```

### `type programInterface`

``` ocaml
type 'msg programInterface =   < pushMsg : 'msg -> unit     ; shutdown : unit -> unit     ; getHtmlString : unit -> string >     Js.t
```

### `val makeProgramInterface`

``` ocaml
val makeProgramInterface :    pushMsg:( 'msg -> unit ) ->   shutdown:( unit -> unit ) ->   getHtmlString:( unit -> string ) ->   'msg programInterface
```

### `val programStateWrapper`

``` ocaml
val programStateWrapper :    'a ->   ( 'msg Vdom.applicationCallbacks Stdlib.ref -> ( 'b, 'msg0 ) pumpInterface ) ->   ( 'c -> 'msg1 Tea_cmd.t ) ->   'msg2 programInterface
```

### `val programLoop`

``` ocaml
val programLoop :    ( 'a -> 'b -> 'c * 'd Tea_cmd.t ) ->   ( 'e -> 'f Vdom.t ) ->   ( 'g -> 'h Tea_sub.t ) ->   'i ->   'j Tea_cmd.t ->   Web.Node.t option ->   'k Tea_cmd.applicationCallbacks Stdlib.ref ->   ( 'l, 'm ) pumpInterface
```

### `val program`

``` ocaml
val program :    ( 'flags, 'model, 'msg ) program ->   Web.Node.t Js.null_undefined ->   'flags ->   'msg programInterface
```

### `val standardProgram`

``` ocaml
val standardProgram :    ( 'flags, 'model, 'msg ) standardProgram ->   Web.Node.t Js.null_undefined ->   'flags ->   'msg programInterface
```

### `val beginnerProgram`

``` ocaml
val beginnerProgram :    ( 'model, 'msg ) beginnerProgram ->   Web.Node.t Js.null_undefined ->   unit ->   'msg programInterface
```

### `val map`

``` ocaml
val map : ( 'a -> 'b ) -> 'a Vdom.t -> 'b Vdom.t
```
