# Module `Tea_sub`

### `type t`

``` ocaml
type 'msg t = 
```

### `type applicationCallbacks`

``` ocaml
type 'msg applicationCallbacks = 'msg Vdom.applicationCallbacks
```

### `val none`

``` ocaml
val none : 'a t
```

### `val batch`

``` ocaml
val batch : 'a t list -> 'a t
```

### `val registration`

``` ocaml
val registration :    string ->   ( 'a Vdom.applicationCallbacks -> unit -> unit ) ->   'a t
```

### `val map`

``` ocaml
val map : ( 'a -> 'b ) -> 'c t -> 'd t
```

### `val mapFunc`

``` ocaml
val mapFunc :    ( 'a Vdom.applicationCallbacks Stdlib.ref ->     'b Vdom.applicationCallbacks Stdlib.ref ) ->   'b t ->   'a t
```

### `val run`

``` ocaml
val run :    'msgOld 'msgNew. 'msgOld Vdom.applicationCallbacks Stdlib.ref ->   'msgNew Vdom.applicationCallbacks Stdlib.ref ->   'msgOld t ->   'msgNew t ->   'msgNew t
```
