# Module `Vdom`

### `type systemMessage`

``` ocaml
type 'msg systemMessage = 
```

### `type applicationCallbacks`

``` ocaml
type 'msg applicationCallbacks = {
```

### `type eventHandler`

``` ocaml
type 'msg eventHandler = 
```

### `type eventCache`

``` ocaml
type 'msg eventCache = {
```

### `type property`

``` ocaml
type 'msg property = 
```

### `type properties`

``` ocaml
type 'msg properties = 'msg property list
```

### `type t`

``` ocaml
type 'msg t = 
```

### `val noNode`

``` ocaml
val noNode : 'msg t
```

### `val comment`

``` ocaml
val comment : string -> 'msg t
```

### `val text`

``` ocaml
val text : string -> 'msg t
```

### `val fullnode`

``` ocaml
val fullnode :    string ->   string ->   string ->   string ->   'msg properties ->   'msg0 t list ->   'msg1 t
```

### `val node`

``` ocaml
val node :    ?namespace:string ->   string ->   ?key:string ->   ?unique:string ->   'msg properties ->   'msg0 t list ->   'msg1 t
```

### `val lazyGen`

``` ocaml
val lazyGen : string -> ( unit -> 'msg t ) -> 'msg0 t
```

### `val noProp`

``` ocaml
val noProp : 'msg property
```

### `val prop`

``` ocaml
val prop : string -> string -> 'msg property
```

### `val onCB`

``` ocaml
val onCB :    string ->   string ->   ( Web.Node.event -> 'msg option ) ->   'msg0 property
```

### `val onMsg`

``` ocaml
val onMsg : string -> 'msg -> 'msg0 property
```

### `val attribute`

``` ocaml
val attribute : string -> string -> string -> 'msg property
```

### `val data`

``` ocaml
val data : string -> string -> 'msg property
```

### `val style`

``` ocaml
val style : string -> string -> 'msg property
```

### `val styles`

``` ocaml
val styles : (string * string) list -> 'msg property
```

### `val renderToHtmlString`

``` ocaml
val renderToHtmlString : 'msg t -> string
```

### `val emptyEventHandler`

``` ocaml
val emptyEventHandler : Web.Node.event_cb
```

### `val emptyEventCB`

``` ocaml
val emptyEventCB : 'a -> Web.Node.event_cb option
```

### `val eventHandler`

``` ocaml
val eventHandler :    'msg applicationCallbacks Stdlib.ref ->   ( Web.Node.event -> 'msg0 option ) Stdlib.ref ->   Web.Node.event_cb
```

### `val eventHandler_GetCB`

``` ocaml
val eventHandler_GetCB : 'msg eventHandler -> Web.Node.event -> 'msg option
```

### `val compareEventHandlerTypes`

``` ocaml
val compareEventHandlerTypes : 'msg eventHandler -> 'msg0 eventHandler -> bool
```

### `val eventHandler_Register`

``` ocaml
val eventHandler_Register :    'msg applicationCallbacks Stdlib.ref ->   Web.Node.t ->   string ->   'msg0 eventHandler ->   'msg1 eventCache option
```

### `val eventHandler_Unregister`

``` ocaml
val eventHandler_Unregister :    Web.Node.t ->   string ->   'msg eventCache option ->   'msg eventCache option
```

### `val eventHandler_Mutate`

``` ocaml
val eventHandler_Mutate :    'msg applicationCallbacks Stdlib.ref ->   Web.Node.t ->   string ->   string ->   'msg0 eventHandler ->   'msg1 eventHandler ->   'msg2 eventCache option Stdlib.ref ->   'msg3 eventCache option Stdlib.ref ->   unit
```

### `val patchVNodesOnElems_PropertiesApply_Add`

``` ocaml
val patchVNodesOnElems_PropertiesApply_Add :    'msg applicationCallbacks Stdlib.ref ->   Web.Node.t ->   int ->   'msg0 property ->   unit
```

### `val patchVNodesOnElems_PropertiesApply_Remove`

``` ocaml
val patchVNodesOnElems_PropertiesApply_Remove :    'msg applicationCallbacks Stdlib.ref ->   Web.Node.t ->   int ->   'msg0 property ->   unit
```

### `val patchVNodesOnElems_PropertiesApply_RemoveAdd`

``` ocaml
val patchVNodesOnElems_PropertiesApply_RemoveAdd :    'msg applicationCallbacks Stdlib.ref ->   Web.Node.t ->   int ->   'msg0 property ->   'msg1 property ->   unit
```

### `val patchVNodesOnElems_PropertiesApply_Mutate`

``` ocaml
val patchVNodesOnElems_PropertiesApply_Mutate :    'msg applicationCallbacks Stdlib.ref ->   Web.Node.t ->   int ->   'msg0 property ->   'msg1 property ->   unit
```

### `val patchVNodesOnElems_PropertiesApply`

``` ocaml
val patchVNodesOnElems_PropertiesApply :    'msg applicationCallbacks Stdlib.ref ->   Web.Node.t ->   int ->   'msg0 property list ->   'msg1 property list ->   bool
```

### `val patchVNodesOnElems_Properties`

``` ocaml
val patchVNodesOnElems_Properties :    'msg applicationCallbacks Stdlib.ref ->   Web.Node.t ->   'msg0 property list ->   'msg1 property list ->   bool
```

### `val genEmptyProps`

``` ocaml
val genEmptyProps : int -> 'msg property list
```

### `val mapEmptyProps`

``` ocaml
val mapEmptyProps : 'msg property list -> 'msg0 property list
```

### `val patchVNodesOnElems_ReplaceNode`

``` ocaml
val patchVNodesOnElems_ReplaceNode :    'msg applicationCallbacks Stdlib.ref ->   Web.Node.t ->   Web.Node.t array ->   int ->   'msg0 t ->   unit
```

### `val patchVNodesOnElems_CreateElement`

``` ocaml
val patchVNodesOnElems_CreateElement :    'msg applicationCallbacks Stdlib.ref ->   'msg0 t ->   Web.Node.t
```

### `val patchVNodesOnElems_MutateNode`

``` ocaml
val patchVNodesOnElems_MutateNode :    'msg applicationCallbacks Stdlib.ref ->   Web.Node.t ->   Web.Node.t array ->   int ->   'msg0 t ->   'msg1 t ->   unit
```

### `val patchVNodesOnElems`

``` ocaml
val patchVNodesOnElems :    'msg applicationCallbacks Stdlib.ref ->   Web_node.t ->   Web_node.t Js.Array.t ->   int ->   'msg0 t list ->   'msg1 t list ->   unit
```

### `val patchVNodesIntoElement`

``` ocaml
val patchVNodesIntoElement :    'msg applicationCallbacks Stdlib.ref ->   Web.Node.t ->   'msg0 t list ->   'msg1 t list ->   'msg2 t list
```

### `val patchVNodeIntoElement`

``` ocaml
val patchVNodeIntoElement :    'msg applicationCallbacks Stdlib.ref ->   Web.Node.t ->   'msg0 t ->   'msg1 t ->   'msg2 t list
```

### `val wrapCallbacks_On`

``` ocaml
val wrapCallbacks_On :    'a 'b. ( 'a -> 'b ) ->   'a systemMessage ->   'b systemMessage
```

### `val wrapCallbacks`

``` ocaml
val wrapCallbacks :    'a 'b. ( 'a -> 'b ) ->   'b applicationCallbacks Stdlib.ref ->   'a applicationCallbacks Stdlib.ref
```

### `val map`

``` ocaml
val map : ( 'a -> 'b ) -> 'a t -> 'b t
```
