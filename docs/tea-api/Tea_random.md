# Module `Tea_random`

### `val minInt`

``` ocaml
val minInt : int
```

### `val maxInt`

``` ocaml
val maxInt : int
```

### `val minFloat`

``` ocaml
val minFloat : float
```

### `val maxFloat`

``` ocaml
val maxFloat : float
```

### `type t`

``` ocaml
type 'typ t = 
```

### `val bool`

``` ocaml
val bool : bool t
```

### `val int`

``` ocaml
val int : int -> int -> int t
```

### `val float`

``` ocaml
val float : float -> float -> float t
```

### `val list`

``` ocaml
val list : int -> 'a t -> 'b list t
```

### `val map`

``` ocaml
val map : ( 'a -> 'b ) -> 'c t -> 'd t
```

### `val map2`

``` ocaml
val map2 : ( 'a -> 'b -> 'c ) -> 'd t -> 'e t -> 'f t
```

### `val map3`

``` ocaml
val map3 : ( 'a -> 'b -> 'c -> 'd ) -> 'e t -> 'f t -> 'g t -> 'h t
```

### `val map4`

``` ocaml
val map4 :    ( 'a -> 'b -> 'c -> 'd -> 'e ) ->   'f t ->   'g t ->   'h t ->   'i t ->   'j t
```

### `val map5`

``` ocaml
val map5 :    ( 'a -> 'b -> 'c -> 'd -> 'e -> 'f ) ->   'g t ->   'h t ->   'i t ->   'j t ->   'k t ->   'l t
```

### `val andThen`

``` ocaml
val andThen : ( 'a -> 'b t ) -> 'c t -> 'd t
```

### `val pair`

``` ocaml
val pair : 'a t -> 'b t -> ('c * 'd) t
```

### `val generate`

``` ocaml
val generate : ( 'a -> 'b ) -> 'c t -> 'd Tea_cmd.t
```

### `type seed`

``` ocaml
type seed = 
```

### `val step`

``` ocaml
val step : 'a t -> seed -> 'b * seed
```

### `val initialSeed`

``` ocaml
val initialSeed : int -> seed
```
