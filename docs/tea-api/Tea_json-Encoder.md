# Module `Tea_json.Encoder`

### `type t`

``` ocaml
type t = Web.Json.t
```

### `val encode`

``` ocaml
val encode : int -> 'a -> string
```

### `val string`

``` ocaml
val string : string -> Web.Json.t
```

### `val int`

``` ocaml
val int : int -> Web.Json.t
```

### `val float`

``` ocaml
val float : float -> Web.Json.t
```

### `val bool`

``` ocaml
val bool : bool -> Web.Json.t
```

### `val null`

``` ocaml
val null : Web.Json.t
```

### `val object_`

``` ocaml
val object_ : (Js.Dict.key * Web.Json.t) list -> Web.Json.t
```

### `val array`

``` ocaml
val array : Web.Json.t array -> Web.Json.t
```

### `val list`

``` ocaml
val list : Web.Json.t list -> Web.Json.t
```
