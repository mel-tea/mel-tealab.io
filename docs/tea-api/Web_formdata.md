# Module `Web_formdata`

### `class type _formdata`

``` ocaml
class type  _formdata = object ... end
```

### `type t`

``` ocaml
type t = _formdata Js.t
```

### `val create`

``` ocaml
val create : unit -> t
```

### `val append`

``` ocaml
val append :    'a ->   'b ->   < append : ( 'c -> 'd -> 'e ) Js_OO.Meth.arity2.. > Js.t ->   'f
```
