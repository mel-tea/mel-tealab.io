# Module `Web_window_history`

### `type t`

``` ocaml
type t =   < length : int     ; back : unit Js_OO.Meth.arity0     ; forward : unit Js_OO.Meth.arity0     ; go : ( int -> unit ) Js_OO.Meth.arity1     ; pushState : ( Js.Json.t -> string -> string -> unit ) Js_OO.Meth.arity3     ; replaceState :        ( Js.Json.t -> string -> string -> unit ) Js_OO.Meth.arity3     ; state : Js.Json.t >     Js.t
```

### `val length`

``` ocaml
val length : < history : < length : int.. > Js.t Js.Undefined.t.. > Js.t -> int
```

### `val back`

``` ocaml
val back : < history : < back : unit.. > Js.t Js.Undefined.t.. > Js.t -> unit
```

### `val forward`

``` ocaml
val forward :    < history : < forward : unit.. > Js.t Js.Undefined.t.. > Js.t ->   unit
```

### `val go`

``` ocaml
val go :    < history : < go : ( 'a -> unit ) Js_OO.Meth.arity1.. > Js.t Js.Undefined.t.. >     Js.t ->   'b ->   unit
```

### `val pushState`

``` ocaml
val pushState :    < history :      < pushState : ( 'a -> 'b -> 'c -> unit ) Js_OO.Meth.arity3.. > Js.t       Js.Undefined.t.. >     Js.t ->   'd ->   'e ->   'f ->   unit
```

### `val replaceState`

``` ocaml
val replaceState :    < history :      < replaceState : ( 'a -> 'b -> 'c -> unit ) Js_OO.Meth.arity3.. > Js.t       Js.Undefined.t.. >     Js.t ->   'd ->   'e ->   'f ->   unit
```

### `val state`

``` ocaml
val state :    < history : < state : 'a Js.Undefined.t.. > Js.t Js.Undefined.t.. > Js.t ->   'a Js.Undefined.t
```
