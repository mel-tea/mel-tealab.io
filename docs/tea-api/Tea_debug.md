# Module `Tea_debug`

### `type debug_msg`

``` ocaml
type 'msg debug_msg = 
```

### `val client_msg`

``` ocaml
val client_msg : 'a -> 'b debug_msg
```

### `type state`

``` ocaml
type state = 
```

### `type debug_model`

``` ocaml
type 'model debug_model = {
```

### `val debug`

``` ocaml
val debug :    ( 'msg -> string ) ->   ( 'model -> 'msg0 -> 'model * 'msg0 Tea_cmd.t ) ->   ( 'model0 -> 'msg1 Vdom.t ) ->   ( 'model1 -> 'msg2 Tea_sub.t ) ->   ( 'model2 -> 'msg3 Tea_cmd.t ) ->   ( ('model3 * 'msg4 Tea_cmd.t) ->   'model3 debug_model * 'msg4 debug_msg Tea_cmd.t )   * ( 'model3 debug_model ->   'msg4 debug_msg ->   'model3 debug_model * 'msg4 debug_msg Tea_cmd.t )   * ( 'model3 debug_model ->   'msg4 debug_msg Vdom.t )   * ( 'model3 debug_model ->   'msg4 debug_msg Tea_sub.t )   * ( 'model3 debug_model ->   'msg4 debug_msg Tea_cmd.t )
```

### `val debug_program`

``` ocaml
val debug_program :    ( 'msg -> string ) ->   ( 'flags, 'model, 'msg ) Tea_app.program ->   ( 'flags, 'model debug_model, 'msg debug_msg ) Tea_app.program
```

### `val debug_navigation_program`

``` ocaml
val debug_navigation_program :    ( 'msg -> string ) ->   ( 'flags, 'model, 'msg ) Tea_navigation.navigationProgram ->   ( 'flags, 'model debug_model, 'msg debug_msg )     Tea_navigation.navigationProgram
```

### `val beginnerProgram`

``` ocaml
val beginnerProgram :    ( 'model, 'msg ) Tea_app.beginnerProgram ->   ( 'msg -> string ) ->   Web.Node.t Js.null_undefined ->   unit ->   'msg debug_msg Tea_app.programInterface
```

### `val standardProgram`

``` ocaml
val standardProgram :    ( 'flags, 'model, 'msg ) Tea_app.standardProgram ->   ( 'msg -> string ) ->   Web.Node.t Js.null_undefined ->   'flags ->   'msg debug_msg Tea_app.programInterface
```

### `val program`

``` ocaml
val program :    ( 'flags, 'model, 'msg ) Tea_app.program ->   ( 'msg -> string ) ->   Web.Node.t Js.null_undefined ->   'flags ->   'msg debug_msg Tea_app.programInterface
```

### `val navigationProgram`

``` ocaml
val navigationProgram :    ( Web.Location.location -> 'msg ) ->   ( 'flags, 'model, 'msg ) Tea_navigation.navigationProgram ->   ( 'msg -> string ) ->   Web.Node.t Js.null_undefined ->   'flags ->   'msg debug_msg Tea_app.programInterface
```
