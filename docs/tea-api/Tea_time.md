# Module `Tea_time`

### `type t`

``` ocaml
type t = float
```

### `val every`

``` ocaml
val every : key:string -> float -> ( float -> 'a ) -> 'b Tea_sub.t
```

### `val delay`

``` ocaml
val delay : float -> 'a -> 'b Tea_cmd.t
```

### `val millisecond`

``` ocaml
val millisecond : float
```

### `val second`

``` ocaml
val second : float
```

### `val minute`

``` ocaml
val minute : float
```

### `val hour`

``` ocaml
val hour : float
```

### `val inMilliseconds`

``` ocaml
val inMilliseconds : 'a -> 'b
```

### `val inSeconds`

``` ocaml
val inSeconds : float -> float
```

### `val inMinutes`

``` ocaml
val inMinutes : float -> float
```

### `val inHours`

``` ocaml
val inHours : float -> float
```
