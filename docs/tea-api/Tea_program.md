# Module `Tea_program`

### `type processMsg`

``` ocaml
type 'msg processMsg = 
```

### `val spawn`

``` ocaml
val spawn :    'a ->   ( 'b -> 'c -> 'd option ) ->   ( 'e -> unit ) ->   'f processMsg ->   unit
```

### `module type Process`

``` ocaml
module type Process = sig ... end
```

### `val testing1`

``` ocaml
val testing1 : int
```
