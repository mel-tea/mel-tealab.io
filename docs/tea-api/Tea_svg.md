# Module `Tea_svg`

### `module Cmds`

``` ocaml
module Cmds = Tea_html_cmds
```

### `module Attributes`

``` ocaml
module Attributes = Tea_svg_attributes
```

### `module Events`

``` ocaml
module Events = Tea_svg_events
```

### `val svgNamespace`

``` ocaml
val svgNamespace : string
```

### `val noNode`

``` ocaml
val noNode : 'a Vdom.t
```

### `val text`

``` ocaml
val text : string -> 'a Vdom.t
```

### `val lazy1`

``` ocaml
val lazy1 : string -> ( unit -> 'a Vdom.t ) -> 'a Vdom.t
```

### `val node`

``` ocaml
val node :    string ->   ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val svg`

``` ocaml
val svg :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val foreignObject`

``` ocaml
val foreignObject :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val animate`

``` ocaml
val animate :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val animateColor`

``` ocaml
val animateColor :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val animateMotion`

``` ocaml
val animateMotion :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val animateTransform`

``` ocaml
val animateTransform :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val mpath`

``` ocaml
val mpath :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val set`

``` ocaml
val set :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val a`

``` ocaml
val a :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val defs`

``` ocaml
val defs :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val g`

``` ocaml
val g :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val marker`

``` ocaml
val marker :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val mask`

``` ocaml
val mask :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val missingGlyph`

``` ocaml
val missingGlyph :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val pattern`

``` ocaml
val pattern :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val switch`

``` ocaml
val switch :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val symbol`

``` ocaml
val symbol :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val desc`

``` ocaml
val desc :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val metadata`

``` ocaml
val metadata :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val title`

``` ocaml
val title :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feBlend`

``` ocaml
val feBlend :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feColorMatrix`

``` ocaml
val feColorMatrix :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feComponentTransfer`

``` ocaml
val feComponentTransfer :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feComposite`

``` ocaml
val feComposite :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feConvolveMatrix`

``` ocaml
val feConvolveMatrix :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feDiffuseLighting`

``` ocaml
val feDiffuseLighting :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feDisplacementMap`

``` ocaml
val feDisplacementMap :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feFlood`

``` ocaml
val feFlood :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feFuncA`

``` ocaml
val feFuncA :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feFuncB`

``` ocaml
val feFuncB :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feFuncG`

``` ocaml
val feFuncG :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feFuncR`

``` ocaml
val feFuncR :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feGaussianBlur`

``` ocaml
val feGaussianBlur :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feImage`

``` ocaml
val feImage :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feMerge`

``` ocaml
val feMerge :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feMergeNode`

``` ocaml
val feMergeNode :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feMorphology`

``` ocaml
val feMorphology :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feOffset`

``` ocaml
val feOffset :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feSpecularLighting`

``` ocaml
val feSpecularLighting :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feTile`

``` ocaml
val feTile :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feTurbulence`

``` ocaml
val feTurbulence :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val font`

``` ocaml
val font :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val fontFace`

``` ocaml
val fontFace :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val fontFaceFormat`

``` ocaml
val fontFaceFormat :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val fontFaceName`

``` ocaml
val fontFaceName :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val fontFaceSrc`

``` ocaml
val fontFaceSrc :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val fontFaceUri`

``` ocaml
val fontFaceUri :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val hkern`

``` ocaml
val hkern :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val vkern`

``` ocaml
val vkern :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val linearGradient`

``` ocaml
val linearGradient :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val radialGradient`

``` ocaml
val radialGradient :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val stop`

``` ocaml
val stop :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val circle`

``` ocaml
val circle :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val ellipse`

``` ocaml
val ellipse :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val svgimage`

``` ocaml
val svgimage :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val line`

``` ocaml
val line :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val path`

``` ocaml
val path :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val polygon`

``` ocaml
val polygon :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val polyline`

``` ocaml
val polyline :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val rect`

``` ocaml
val rect :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val use`

``` ocaml
val use :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feDistantLight`

``` ocaml
val feDistantLight :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val fePointLight`

``` ocaml
val fePointLight :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val feSpotLight`

``` ocaml
val feSpotLight :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val altGlyph`

``` ocaml
val altGlyph :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val altGlyphDef`

``` ocaml
val altGlyphDef :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val altGlyphItem`

``` ocaml
val altGlyphItem :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val glyph`

``` ocaml
val glyph :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val glyphRef`

``` ocaml
val glyphRef :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val textPath`

``` ocaml
val textPath :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val text'`

``` ocaml
val text' :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val tref`

``` ocaml
val tref :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val tspan`

``` ocaml
val tspan :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val clipPath`

``` ocaml
val clipPath :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val svgcolorProfile`

``` ocaml
val svgcolorProfile :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val cursor`

``` ocaml
val cursor :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val filter`

``` ocaml
val filter :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val script`

``` ocaml
val script :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val style`

``` ocaml
val style :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```

### `val view`

``` ocaml
val view :    ?key:string ->   ?unique:string ->   'a Vdom.properties ->   'a Vdom.t list ->   'a Vdom.t
```
