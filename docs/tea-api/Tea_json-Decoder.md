# Module `Tea_json.Decoder`

### `type error`

``` ocaml
type error = Stdlib.String.t
```

### `module ObjectDict`

``` ocaml
module ObjectDict : sig ... end
```

### `type t`

``` ocaml
type ('input, 'result) t = 
```

### `exception ParseFail`

``` ocaml
exception ParseFail of string
```

### `val string`

``` ocaml
val string : ( Web.Json.t, string ) t
```

### `val int`

``` ocaml
val int : ( Web.Json.t, int ) t
```

### `val float`

``` ocaml
val float : ( Web.Json.t, float ) t
```

### `val bool`

``` ocaml
val bool : ( Web.Json.t, bool ) t
```

### `val null`

``` ocaml
val null : 'a -> ( Web.Json.t, 'b ) t
```

### `val list`

``` ocaml
val list : ( Web.Json.t, 'a ) t -> ( Web.Json.t, 'b list ) t
```

### `val array`

``` ocaml
val array : ( Web.Json.t, 'a ) t -> ( Web.Json.t, 'b array ) t
```

### `val keyValuePairs`

``` ocaml
val keyValuePairs :    ( Web.Json.t, 'a ) t ->   ( Web.Json.t, (Js.Dict.key * 'b) list ) t
```

### `val dict`

``` ocaml
val dict : ( Web.Json.t, 'a ) t -> ( Web.Json.t, 'b ObjectDict.t ) t
```

### `val field`

``` ocaml
val field : Js.Dict.key -> ( Web.Json.t, 'a ) t -> ( Web.Json.t, 'b ) t
```

### `val at`

``` ocaml
val at : Js.Dict.key list -> ( Web.Json.t, 'a ) t -> ( Web.Json.t, 'a ) t
```

### `val index`

``` ocaml
val index : int -> ( Web.Json.t, 'a ) t -> ( Web.Json.t, 'b ) t
```

### `val maybe`

``` ocaml
val maybe : ( 'a, 'b ) t -> ( 'c, 'd option ) t
```

### `val oneOf`

``` ocaml
val oneOf : ( 'a, 'b ) t list -> ( 'c, 'd ) t
```

### `val map`

``` ocaml
val map : ( 'a -> 'b ) -> ( 'c, 'd ) t -> ( 'e, 'f ) t
```

### `val map2`

``` ocaml
val map2 : ( 'a -> 'b -> 'c ) -> ( 'd, 'e ) t -> ( 'f, 'g ) t -> ( 'h, 'i ) t
```

### `val map3`

``` ocaml
val map3 :    ( 'a -> 'b -> 'c -> 'd ) ->   ( 'e, 'f ) t ->   ( 'g, 'h ) t ->   ( 'i, 'j ) t ->   ( 'k, 'l ) t
```

### `val map4`

``` ocaml
val map4 :    ( 'a -> 'b -> 'c -> 'd -> 'e ) ->   ( 'f, 'g ) t ->   ( 'h, 'i ) t ->   ( 'j, 'k ) t ->   ( 'l, 'm ) t ->   ( 'n, 'o ) t
```

### `val map5`

``` ocaml
val map5 :    ( 'a -> 'b -> 'c -> 'd -> 'e -> 'f ) ->   ( 'g, 'h ) t ->   ( 'i, 'j ) t ->   ( 'k, 'l ) t ->   ( 'm, 'n ) t ->   ( 'o, 'p ) t ->   ( 'q, 'r ) t
```

### `val map6`

``` ocaml
val map6 :    ( 'a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g ) ->   ( 'h, 'i ) t ->   ( 'j, 'k ) t ->   ( 'l, 'm ) t ->   ( 'n, 'o ) t ->   ( 'p, 'q ) t ->   ( 'r, 's ) t ->   ( 't, 'u ) t
```

### `val map7`

``` ocaml
val map7 :    ( 'a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h ) ->   ( 'i, 'j ) t ->   ( 'k, 'l ) t ->   ( 'm, 'n ) t ->   ( 'o, 'p ) t ->   ( 'q, 'r ) t ->   ( 's, 't ) t ->   ( 'u, 'v ) t ->   ( 'w, 'x ) t
```

### `val map8`

``` ocaml
val map8 :    ( 'a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h -> 'i ) ->   ( 'j, 'k ) t ->   ( 'l, 'm ) t ->   ( 'n, 'o ) t ->   ( 'p, 'q ) t ->   ( 'r, 's ) t ->   ( 't, 'u ) t ->   ( 'v, 'w ) t ->   ( 'x, 'y ) t ->   ( 'z, 'a1 ) t
```

### `val succeed`

``` ocaml
val succeed : 'a -> ( 'b, 'c ) t
```

### `val fail`

``` ocaml
val fail : error -> ( 'a, 'b ) t
```

### `val value`

``` ocaml
val value : ( 'a, 'b ) t
```

### `val andThen`

``` ocaml
val andThen : ( 'a -> ( 'b, 'c ) t ) -> ( 'd, 'e ) t -> ( 'f, 'g ) t
```

### `val lazy_`

``` ocaml
val lazy_ : ( unit -> ( 'a, 'b ) t ) -> ( 'a, 'b ) t
```

### `val nullable`

``` ocaml
val nullable : ( Web.Json.t, 'a ) t -> ( Web.Json.t, 'b option ) t
```

### `val decodeValue`

``` ocaml
val decodeValue : ( 'a, 'b ) t -> 'c -> ( 'b, error ) Tea_result.t
```

### `val decodeEvent`

``` ocaml
val decodeEvent : ( 'a, 'b ) t -> Web_node.event -> ( 'b, error ) Tea_result.t
```

### `val decodeString`

``` ocaml
val decodeString : ( Web.Json.t, 'a ) t -> string -> ( 'a, error ) Tea_result.t
```
