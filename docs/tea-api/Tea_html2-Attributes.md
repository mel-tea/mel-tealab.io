# Module `Tea_html2.Attributes`

Helper functions for HTML attributes. They are organized roughly by
category.

-   [Primitives](#primitives)
-   [Super common attributes](#super-common-attributes)
-   [Inputs](#inputs)
-   [Input helpers](#input-helpers)
-   [Input ranges](#input-ranges)
-   [Textarea](#textarea)
-   [Links and areas](#links-and-areas)
-   [Maps](#maps)
-   [Embedded content](#embedded-content)
-   [Audio and Video](#audio-and-video)
-   [IFrames](#iframes)
-   [Ordered lists](#ordered-lists)
-   [Tables](#tables)
-   [Header stuff](#header-stuff)
-   [Less common global attributes](#less-common-global-attributes)
-   [Key generation](#key-generation)
-   [Miscellaneous](#miscellaneous)

## [](#primitives)Primitives

### `val noProp`

``` ocaml
val noProp : 'a Vdom.property
```

### `val style`

``` ocaml
val style : string -> string -> 'a Vdom.property
```

### `val styles`

``` ocaml
val styles : (string * string) list -> 'a Vdom.property
```

## [](#super-common-attributes)Super common attributes

### `val class'`

``` ocaml
val class' : string -> 'a Vdom.property
```

### `val classList`

``` ocaml
val classList : (string * bool) list -> 'a Vdom.property
```

### `val id`

``` ocaml
val id : string -> 'a Vdom.property
```

### `val title`

``` ocaml
val title : string -> 'a Vdom.property
```

### `val hidden`

``` ocaml
val hidden : bool -> 'a Vdom.property
```

## [](#inputs)Inputs

### `val type'`

``` ocaml
val type' : string -> 'a Vdom.property
```

### `val value`

``` ocaml
val value : string -> 'a Vdom.property
```

### `val defaultValue`

``` ocaml
val defaultValue : string -> 'a Vdom.property
```

### `val checked`

``` ocaml
val checked : bool -> 'a Vdom.property
```

### `val placeholder`

``` ocaml
val placeholder : string -> 'a Vdom.property
```

### `val selected`

``` ocaml
val selected : bool -> 'a Vdom.property
```

## [](#input-helpers)Input helpers

### `val accept`

``` ocaml
val accept : string -> 'a Vdom.property
```

### `val acceptCharset`

``` ocaml
val acceptCharset : string -> 'a Vdom.property
```

### `val action`

``` ocaml
val action : string -> 'a Vdom.property
```

### `val autocomplete`

``` ocaml
val autocomplete : bool -> 'a Vdom.property
```

### `val autofocus`

``` ocaml
val autofocus : bool -> 'a Vdom.property
```

### `val disabled`

``` ocaml
val disabled : bool -> 'a Vdom.property
```

### `val enctype`

``` ocaml
val enctype : string -> 'a Vdom.property
```

### `val formaction`

``` ocaml
val formaction : string -> 'a Vdom.property
```

### `val list`

``` ocaml
val list : string -> 'a Vdom.property
```

### `val minlength`

``` ocaml
val minlength : int -> 'a Vdom.property
```

### `val maxlength`

``` ocaml
val maxlength : int -> 'a Vdom.property
```

### `val method'`

``` ocaml
val method' : string -> 'a Vdom.property
```

### `val multiple`

``` ocaml
val multiple : bool -> 'a Vdom.property
```

### `val name`

``` ocaml
val name : string -> 'a Vdom.property
```

### `val novalidate`

``` ocaml
val novalidate : bool -> 'a Vdom.property
```

### `val pattern`

``` ocaml
val pattern : string -> 'a Vdom.property
```

### `val readonly`

``` ocaml
val readonly : bool -> 'a Vdom.property
```

### `val required`

``` ocaml
val required : bool -> 'a Vdom.property
```

### `val size`

``` ocaml
val size : int -> 'a Vdom.property
```

### `val for'`

``` ocaml
val for' : string -> 'a Vdom.property
```

### `val form`

``` ocaml
val form : string -> 'a Vdom.property
```

## [](#input-ranges)Input ranges

### `val max`

``` ocaml
val max : string -> 'a Vdom.property
```

### `val min`

``` ocaml
val min : string -> 'a Vdom.property
```

### `val step`

``` ocaml
val step : string -> 'a Vdom.property
```

## [](#textarea)Textarea

### `val cols`

``` ocaml
val cols : int -> 'a Vdom.property
```

### `val rows`

``` ocaml
val rows : int -> 'a Vdom.property
```

### `val wrap`

``` ocaml
val wrap : string -> 'a Vdom.property
```

## [](#links-and-areas)Links and areas

### `val href`

``` ocaml
val href : string -> 'a Vdom.property
```

### `val target`

``` ocaml
val target : string -> 'a Vdom.property
```

### `val download`

``` ocaml
val download : bool -> 'a Vdom.property
```

### `val downloadAs`

``` ocaml
val downloadAs : string -> 'a Vdom.property
```

### `val hreflang`

``` ocaml
val hreflang : string -> 'a Vdom.property
```

### `val media`

``` ocaml
val media : string -> 'a Vdom.property
```

### `val ping`

``` ocaml
val ping : string -> 'a Vdom.property
```

### `val rel`

``` ocaml
val rel : string -> 'a Vdom.property
```

## [](#maps)Maps

### `val ismap`

``` ocaml
val ismap : bool -> 'a Vdom.property
```

### `val usemap`

``` ocaml
val usemap : string -> 'a Vdom.property
```

### `val shape`

``` ocaml
val shape : string -> 'a Vdom.property
```

### `val coords`

``` ocaml
val coords : string -> 'a Vdom.property
```

## [](#embedded-content)Embedded content

### `val src`

``` ocaml
val src : string -> 'a Vdom.property
```

### `val height`

``` ocaml
val height : int -> 'a Vdom.property
```

### `val width`

``` ocaml
val width : int -> 'a Vdom.property
```

### `val alt`

``` ocaml
val alt : string -> 'a Vdom.property
```

## [](#audio-and-video)Audio and Video

### `val autoplay`

``` ocaml
val autoplay : bool -> 'a Vdom.property
```

### `val controls`

``` ocaml
val controls : bool -> 'a Vdom.property
```

### `val loop`

``` ocaml
val loop : bool -> 'a Vdom.property
```

### `val preload`

``` ocaml
val preload : string -> 'a Vdom.property
```

### `val poster`

``` ocaml
val poster : string -> 'a Vdom.property
```

### `val default`

``` ocaml
val default : bool -> 'a Vdom.property
```

### `val kind`

``` ocaml
val kind : string -> 'a Vdom.property
```

### `val srclang`

``` ocaml
val srclang : string -> 'a Vdom.property
```

## [](#iframes)IFrames

### `val sandbox`

``` ocaml
val sandbox : string -> 'a Vdom.property
```

### `val seamless`

``` ocaml
val seamless : bool -> 'a Vdom.property
```

### `val srcdoc`

``` ocaml
val srcdoc : string -> 'a Vdom.property
```

## [](#ordered-lists)Ordered lists

### `val reversed`

``` ocaml
val reversed : bool -> 'a Vdom.property
```

### `val start`

``` ocaml
val start : int -> 'a Vdom.property
```

## [](#tables)Tables

### `val colspan`

``` ocaml
val colspan : int -> 'a Vdom.property
```

### `val rowspan`

``` ocaml
val rowspan : int -> 'a Vdom.property
```

### `val headers`

``` ocaml
val headers : string -> 'a Vdom.property
```

### `val scope`

``` ocaml
val scope : string -> 'a Vdom.property
```

### `val align`

``` ocaml
val align : string -> 'a Vdom.property
```

## [](#header-stuff)Header stuff

### `val async`

``` ocaml
val async : bool -> 'a Vdom.property
```

### `val charset`

``` ocaml
val charset : string -> 'a Vdom.property
```

### `val content`

``` ocaml
val content : string -> 'a Vdom.property
```

### `val defer`

``` ocaml
val defer : bool -> 'a Vdom.property
```

### `val httpEquiv`

``` ocaml
val httpEquiv : string -> 'a Vdom.property
```

### `val language`

``` ocaml
val language : string -> 'a Vdom.property
```

### `val scoped`

``` ocaml
val scoped : string -> 'a Vdom.property
```

## [](#less-common-global-attributes)Less common global attributes

### `val accesskey`

``` ocaml
val accesskey : char -> 'a Vdom.property
```

### `val contenteditable`

``` ocaml
val contenteditable : bool -> 'a Vdom.property
```

### `val contextmenu`

``` ocaml
val contextmenu : string -> 'a Vdom.property
```

### `val dir`

``` ocaml
val dir : string -> 'a Vdom.property
```

### `val draggable`

``` ocaml
val draggable : string -> 'a Vdom.property
```

### `val dropzone`

``` ocaml
val dropzone : string -> 'a Vdom.property
```

### `val itemprop`

``` ocaml
val itemprop : string -> 'a Vdom.property
```

### `val lang`

``` ocaml
val lang : string -> 'a Vdom.property
```

### `val spellcheck`

``` ocaml
val spellcheck : bool -> 'a Vdom.property
```

### `val tabindex`

``` ocaml
val tabindex : int -> 'a Vdom.property
```

## [](#key-generation)Key generation

### `val challenge`

``` ocaml
val challenge : string -> 'a Vdom.property
```

### `val keytype`

``` ocaml
val keytype : string -> 'a Vdom.property
```

## [](#miscellaneous)Miscellaneous

### `val cite`

``` ocaml
val cite : string -> 'a Vdom.property
```

### `val datetime`

``` ocaml
val datetime : string -> 'a Vdom.property
```

### `val pubdate`

``` ocaml
val pubdate : string -> 'a Vdom.property
```

### `val manifest`

``` ocaml
val manifest : string -> 'a Vdom.property
```
