# Class type `Web_xmlhttprequest._xmlhttprequest`

### `method abort`

``` ocaml
method abort : unit Js_OO.Meth.arity0
```

### `method getAllResponseHeaders`

``` ocaml
method getAllResponseHeaders : string Js.null Js_OO.Meth.arity0
```

### `method getResponseHeader`

``` ocaml
method getResponseHeader : ( string -> string Js.null ) Js_OO.Meth.arity1
```

### `method _open`

``` ocaml
method _open : ( string ->                  string ->                  bool ->                  string ->                  string ->                  unit )                  Js_OO.Meth.arity5
```

### `method overrideMimeType`

``` ocaml
method overrideMimeType : ( string -> unit ) Js_OO.Meth.arity1
```

### `method send`

``` ocaml
method send : unit Js_OO.Meth.arity0
```

### `method send__string`

``` ocaml
method send__string : ( string Js.null -> unit ) Js_OO.Meth.arity1
```

### `method send__formdata`

``` ocaml
method send__formdata : ( Web_formdata.t -> unit ) Js_OO.Meth.arity1
```

### `method send__document`

``` ocaml
method send__document : ( Web_document.t -> unit ) Js_OO.Meth.arity1
```

### `method setRequestHeader`

``` ocaml
method setRequestHeader : ( string -> string -> unit ) Js_OO.Meth.arity2
```

### `method onreadystatechange#=`

``` ocaml
method onreadystatechange#= : ( ( event_readystatechange -> unit ) ->                                 unit )                                 Js_OO.Meth.arity1
```

### `method onreadystatechange`

``` ocaml
method onreadystatechange : event_readystatechange -> unit
```

### `method readyState`

``` ocaml
method readyState : int
```

### `method responseType#=`

``` ocaml
method responseType#= : ( string -> unit ) Js_OO.Meth.arity1
```

### `method responseType`

``` ocaml
method responseType : string
```

### `method response`

``` ocaml
method response : unresolved Js.null
```

### `method responseText`

``` ocaml
method responseText : string
```

### `method responseURL`

``` ocaml
method responseURL : string
```

### `method responseXML`

``` ocaml
method responseXML : Web_document.t Js.null
```

### `method status`

``` ocaml
method status : int
```

### `method statusText`

``` ocaml
method statusText : string
```

### `method timeout#=`

``` ocaml
method timeout#= : ( float -> unit ) Js_OO.Meth.arity1
```

### `method timeout`

``` ocaml
method timeout : float
```

### `method upload`

``` ocaml
method upload : xmlHttpRequestUpload
```

### `method withCredentials#=`

``` ocaml
method withCredentials#= : ( bool -> unit ) Js_OO.Meth.arity1
```

### `method withCredentials`

``` ocaml
method withCredentials : bool
```

### `method onabort#=`

``` ocaml
method onabort#= : ( ( event_abort -> unit ) -> unit ) Js_OO.Meth.arity1
```

### `method onabort`

``` ocaml
method onabort : event_abort -> unit
```

### `method onerror#=`

``` ocaml
method onerror#= : ( ( event_error -> unit ) -> unit ) Js_OO.Meth.arity1
```

### `method onerror`

``` ocaml
method onerror : event_error -> unit
```

### `method onload#=`

``` ocaml
method onload#= : ( ( event_load -> unit ) -> unit ) Js_OO.Meth.arity1
```

### `method onload`

``` ocaml
method onload : event_load -> unit
```

### `method onloadstart#=`

``` ocaml
method onloadstart#= : ( ( event_loadstart -> unit ) ->                          unit )                          Js_OO.Meth.arity1
```

### `method onloadstart`

``` ocaml
method onloadstart : event_loadstart -> unit
```

### `method onprogress#=`

``` ocaml
method onprogress#= : ( ( event_loadstart -> unit ) -> unit ) Js_OO.Meth.arity1
```

### `method onprogress`

``` ocaml
method onprogress : event_loadstart -> unit
```

### `method ontimeout#=`

``` ocaml
method ontimeout#= : ( ( event_timeout -> unit ) -> unit ) Js_OO.Meth.arity1
```

### `method ontimeout`

``` ocaml
method ontimeout : event_timeout -> unit
```

### `method onloadend#=`

``` ocaml
method onloadend#= : ( ( event_loadend -> unit ) -> unit ) Js_OO.Meth.arity1
```

### `method onloadend`

``` ocaml
method onloadend : event_loadend -> unit
```
