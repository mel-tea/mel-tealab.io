# Class type `Web_formdata._formdata`

### `method append`

``` ocaml
method append : ( string -> string -> unit ) Js_OO.Meth.arity2
```
