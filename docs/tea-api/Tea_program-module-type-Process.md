# Module type `Tea_program.Process`

### `type msg`

``` ocaml
type msg
```

### `val handleMsg`

``` ocaml
val handleMsg : msg -> unit
```
