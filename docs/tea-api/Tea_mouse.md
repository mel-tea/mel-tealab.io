# Module `Tea_mouse`

### `type position`

``` ocaml
type position = {
```

### `val position`

``` ocaml
val position : ( Web.Json.t, position ) Tea_json.Decoder.t
```

### `val registerGlobal`

``` ocaml
val registerGlobal : string -> string -> ( position -> 'a ) -> 'b Tea_sub.t
```

### `val clicks`

``` ocaml
val clicks : ?key:string -> ( position -> 'a ) -> 'a Tea_sub.t
```

### `val moves`

``` ocaml
val moves : ?key:string -> ( position -> 'a ) -> 'a Tea_sub.t
```

### `val downs`

``` ocaml
val downs : ?key:string -> ( position -> 'a ) -> 'a Tea_sub.t
```

### `val ups`

``` ocaml
val ups : ?key:string -> ( position -> 'a ) -> 'a Tea_sub.t
```
