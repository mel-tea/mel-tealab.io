# Module `Tea_animationframe`

### `type t`

``` ocaml
type t = {
```

### `val every`

``` ocaml
val every : ?key:string -> ( t -> 'a ) -> 'b Tea_sub.t
```

### `val times`

``` ocaml
val times : ?key:string -> ( key:string -> Tea_time.t -> 'a ) -> 'b Tea_sub.t
```

### `val diffs`

``` ocaml
val diffs : ?key:string -> ( key:string -> Tea_time.t -> 'a ) -> 'b Tea_sub.t
```
