# Module `Tea_promise`

### `val cmd`

``` ocaml
val cmd : 'a Js.Promise.t -> ( 'b -> 'c option ) -> 'd Tea_cmd.t
```

### `val result`

``` ocaml
val result :    'a Js.Promise.t ->   ( ( 'b, string ) Tea_result.t -> 'c ) ->   'd Tea_cmd.t
```
