# Module `Tea_html.Attributes`

### `val max`

``` ocaml
val max : string -> 'a Vdom.property
```

### `val min`

``` ocaml
val min : string -> 'a Vdom.property
```

### `val step`

``` ocaml
val step : string -> 'a Vdom.property
```

### `val disabled`

``` ocaml
val disabled : bool -> 'a Vdom.property
```

### `val selected`

``` ocaml
val selected : bool -> 'a Vdom.property
```

### `val acceptCharset`

``` ocaml
val acceptCharset : string -> 'a Vdom.property
```

### `val rel`

``` ocaml
val rel : string -> 'a Vdom.property
```
