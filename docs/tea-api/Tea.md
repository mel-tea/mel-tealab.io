# Module `Tea`

### `module Result`

``` ocaml
module Result = Tea_result
```

### `module Cmd`

``` ocaml
module Cmd = Tea_cmd
```

### `module Sub`

``` ocaml
module Sub = Tea_sub
```

### `module App`

``` ocaml
module App = Tea_app
```

### `module Debug`

``` ocaml
module Debug = Tea_debug
```

### `module Html`

``` ocaml
module Html = Tea_html
```

### `module Html2`

``` ocaml
module Html2 = Tea_html2
```

### `module Svg`

``` ocaml
module Svg = Tea_svg
```

### `module Task`

``` ocaml
module Task = Tea_task
```

### `module Program`

``` ocaml
module Program = Tea_program
```

### `module Time`

``` ocaml
module Time = Tea_time
```

### `module Json`

``` ocaml
module Json = Tea_json
```

### `module Navigation`

``` ocaml
module Navigation = Tea_navigation
```

### `module Random`

``` ocaml
module Random = Tea_random
```

### `module AnimationFrame`

``` ocaml
module AnimationFrame = Tea_animationframe
```

### `module Mouse`

``` ocaml
module Mouse = Tea_mouse
```

### `module Http`

``` ocaml
module Http = Tea_http
```

### `module Ex`

``` ocaml
module Ex = Tea_ex
```
