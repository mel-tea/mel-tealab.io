# Module `Tea_json`

### `module Decoder`

``` ocaml
module Decoder : sig ... end
```

### `module Encoder`

``` ocaml
module Encoder : sig ... end
```

### `type t`

``` ocaml
type t = Web.Json.t
```
