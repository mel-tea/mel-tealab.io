# Module `Web_document`

### `type t`

``` ocaml
type t =   < body : Web_node.t     ; createElement : ( string -> Web_node.t ) Js_OO.Meth.arity1     ; createElementNS : ( string -> string -> Web_node.t ) Js_OO.Meth.arity2     ; createComment : ( string -> Web_node.t ) Js_OO.Meth.arity1     ; createTextNode : ( string -> Web_node.t ) Js_OO.Meth.arity1     ; getElementById :        ( string -> Web_node.t Js.null_undefined ) Js_OO.Meth.arity1     ; location : Web_location.t >     Js.t
```

### `val document`

``` ocaml
val document : t
```

### `val body`

``` ocaml
val body : unit -> Web_node.t
```

### `val createElement`

``` ocaml
val createElement : string -> Web_node.t
```

### `val createElementNS`

``` ocaml
val createElementNS : string -> string -> Web_node.t
```

### `val createComment`

``` ocaml
val createComment : string -> Web_node.t
```

### `val createTextNode`

``` ocaml
val createTextNode : string -> Web_node.t
```

### `val getElementById`

``` ocaml
val getElementById : string -> Web_node.t Js.null_undefined
```

### `val createElementNsOptional`

``` ocaml
val createElementNsOptional : string -> string -> Web_node.t
```

### `val location`

``` ocaml
val location : unit -> Web_location.t
```
