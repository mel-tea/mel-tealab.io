# Module `Tea_ex.LocalStorage`

### `val length`

``` ocaml
val length : ( int, string ) Tea_task.t
```

### `val clear`

``` ocaml
val clear : ( unit, string ) Tea_task.t
```

### `val clearCmd`

``` ocaml
val clearCmd : unit -> 'a Tea_cmd.t
```

### `val key`

``` ocaml
val key : int -> ( string, string ) Tea_task.t
```

### `val getItem`

``` ocaml
val getItem : string -> ( string, string ) Tea_task.t
```

### `val removeItem`

``` ocaml
val removeItem : string -> ( unit, string ) Tea_task.t
```

### `val removeItemCmd`

``` ocaml
val removeItemCmd : string -> 'a Tea_cmd.t
```

### `val setItem`

``` ocaml
val setItem : string -> string -> ( unit, string ) Tea_task.t
```

### `val setItemCmd`

``` ocaml
val setItemCmd : string -> string -> 'a Tea_cmd.t
```
