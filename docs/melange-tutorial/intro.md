---
title: Introduction
sidebar_position: 1
slug: /melange-tutorial
---

## Templates

- [melange-basic-template](https://github.com/melange-re/melange-basic-template) (Webpack, React)
- [spiced-tea](https://github.com/jayeshbhoot/spiced-tea) (Browserify, bucklescript-tea)
- [melange-tea-vite](https://gitlab.com/pdelacroix/melange-tea-vite) (Vite, bucklescript-tea)
