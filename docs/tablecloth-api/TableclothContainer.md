# Module `TableclothContainer`

This module contains module signatures which are used in functions which
accept first class modules.

### `module type Sum`

``` ocaml
module type Sum = sig ... end
```

Modules which conform to this signature can be used with functions like
`Array`.sum or `List`.sum
