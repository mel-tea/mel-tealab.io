# Module type `TableclothContainer.Sum`

Modules which conform to this signature can be used with functions like
`Array`.sum or `List`.sum

### `type t`

``` ocaml
type t
```

### `val zero`

``` ocaml
val zero : t
```

### `val add`

``` ocaml
val add : t -> t -> t
```
