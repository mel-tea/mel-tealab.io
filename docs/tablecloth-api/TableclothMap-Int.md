# Module `TableclothMap.Int`

Construct a Map with [`Int`](#)s for keys.

### `type t`

``` ocaml
type nonrec 'value t = 'value Of(TableclothInt).t
```

### `val empty`

``` ocaml
val empty : 'value t
```

A map with nothing in it.

### `val singleton`

``` ocaml
val singleton : key:int -> value:'value -> 'value t
```

Create a map from a key and value

Examples

``` ocaml
Map.Int.singleton ~key:1 ~value:"Ant" |> Map.toList = [(1, "Ant")]
```

### `val fromArray`

``` ocaml
val fromArray : (int * 'value) array -> 'value t
```

Create a map from an `Array` of key-value tuples

### `val from_array`

``` ocaml
val from_array : (int * 'value) array -> 'value t
```

### `val fromList`

``` ocaml
val fromList : (int * 'value) list -> 'value t
```

Create a map of a `List` of key-value tuples

### `val from_list`

``` ocaml
val from_list : (int * 'value) list -> 'value t
```
