# Module `TableclothString`

-   [Create](#create)
-   [Basic operations](#basic-operations)
-   [Query](#query)
-   [Modify](#modify)
-   [Deconstruct](#deconstruct)
-   [Iterate](#iterate)
-   [Convert](#convert)
-   [Compare](#compare)

Functions for working with `"strings"`

### `type t`

``` ocaml
type t = string
```

## [](#create)Create

Strings literals are created with the `"double quotes"` syntax.

### `val fromChar`

``` ocaml
val fromChar : char -> string
```

Converts the given character to an equivalent string of length one.

### `val from_char`

``` ocaml
val from_char : char -> string
```

### `val fromArray`

``` ocaml
val fromArray : char array -> string
```

Create a string from an `Array` of characters.

Note that these must be individual characters in single quotes, not
strings of length one.

Examples

``` ocaml
String.fromArray [||] = ""
```

``` ocaml
String.fromArray [|'a'; 'b'; 'c'|] = "abc"
```

### `val from_array`

``` ocaml
val from_array : char array -> string
```

### `val fromList`

``` ocaml
val fromList : char list -> string
```

Create a string from a `List` of characters.

Note that these must be individual characters in single quotes, not
strings of length one.

Examples

``` ocaml
String.fromList [] = ""
```

``` ocaml
String.fromList ['a'; 'b'; 'c'] = "abc"
```

### `val from_list`

``` ocaml
val from_list : char list -> string
```

### `val repeat`

``` ocaml
val repeat : string -> count:int -> string
```

Create a string by repeating a string `count` time.

Exceptions

If `count` is negative, `String.repeat` throws a `RangeError` exception.

Examples

``` ocaml
String.repeat ~count:3 "ok" = "okokok"
```

``` ocaml
String.repeat ~count:3 "" = ""
```

``` ocaml
String.repeat ~count:0 "ok" = ""
```

### `val initialize`

``` ocaml
val initialize : int -> f:( int -> char ) -> string
```

Create a string by providing a length and a function to choose
characters.

Returns an empty string if the length is negative.

Examples

``` ocaml
String.initialize 8 ~f:(Fun.constant '9') = "999999999"
```

## [](#basic-operations)Basic operations

### `val get`

``` ocaml
val get : string -> int -> char
```

Get the character at the specified index

### `val getAt`

``` ocaml
val getAt : string -> index:int -> char option
```

Get the character at `~index`

### `val get_at`

``` ocaml
val get_at : string -> index:int -> char option
```

### `val (.?[])`

``` ocaml
val (.?[]) : string -> int -> char option
```

The [index
operator](https://caml.inria.fr/pub/docs/manual-ocaml/indexops.html)
version of [`getAt`](#val-getAt)

**Note** Currently this is only supported by the OCaml syntax.

Examples

``` ocaml
("Doggie".String.?[3]) = Some 'g'
```

``` ocaml
String.("Doggie".?[9]) = None
```

### `val reverse`

``` ocaml
val reverse : string -> string
```

Reverse a string

**Note** This function does not work with Unicode characters.

Examples

``` ocaml
String.reverse "stressed" = "desserts"
```

### `val slice`

``` ocaml
val slice : ?to_:int -> string -> from:int -> string
```

Extract a substring from the specified indicies.

See `Array`.slice.

## [](#query)Query

### `val isEmpty`

``` ocaml
val isEmpty : string -> bool
```

Check if a string is empty

### `val is_empty`

``` ocaml
val is_empty : string -> bool
```

### `val length`

``` ocaml
val length : string -> int
```

Returns the length of the given string.

**Warning** if the string contains non-ASCII characters then `length`
will not equal the number of characters

Examples

``` ocaml
String.length "abc" = 3
```

### `val startsWith`

``` ocaml
val startsWith : string -> prefix:string -> bool
```

See if the second string starts with `prefix`

Examples

``` ocaml
String.startsWith ~prefix:"the" "theory" = true
```

``` ocaml
String.startsWith ~prefix:"ory" "theory" = false
```

### `val starts_with`

``` ocaml
val starts_with : string -> prefix:string -> bool
```

### `val endsWith`

``` ocaml
val endsWith : string -> suffix:string -> bool
```

See if the second string ends with `suffix`.

Examples

``` ocaml
String.endsWith ~suffix:"the" "theory" = false
```

``` ocaml
String.endsWith ~suffix:"ory" "theory" = true
```

### `val ends_with`

``` ocaml
val ends_with : string -> suffix:string -> bool
```

### `val includes`

``` ocaml
val includes : string -> substring:string -> bool
```

Check if one string appears within another

Examples

``` ocaml
String.includes "team" ~substring:"tea" = true
```

``` ocaml
String.includes "team" ~substring:"i" = false
```

``` ocaml
String.includes "ABC" ~substring:"" = true
```

### `val isCapitalized`

``` ocaml
val isCapitalized : string -> bool
```

Test if the first letter of a string is upper case.

**Note** This function works only with ASCII characters, not Unicode.

Examples

``` ocaml
String.isCapitalized "Anastasia" = true
```

``` ocaml
String.isCapitalized "" = false
```

### `val is_capitalized`

``` ocaml
val is_capitalized : string -> bool
```

## [](#modify)Modify

### `val dropLeft`

``` ocaml
val dropLeft : string -> count:int -> string
```

Drop `count` characters from the left side of a string.

Examples

``` ocaml
String.dropLeft ~count:3 "abcdefg" = "defg"
```

``` ocaml
String.dropLeft ~count:0 "abcdefg" = "abcdefg"
```

``` ocaml
String.dropLeft ~count:7 "abcdefg" = ""
```

``` ocaml
String.dropLeft ~count:(-2) "abcdefg" = "fg"
```

``` ocaml
String.dropLeft ~count:8 "abcdefg" = ""
```

### `val drop_left`

``` ocaml
val drop_left : string -> count:int -> string
```

### `val dropRight`

``` ocaml
val dropRight : string -> count:int -> string
```

Drop `count` characters from the right side of a string.

Examples

``` ocaml
String.dropRight ~count:3 "abcdefg" = "abcd"
```

``` ocaml
String.dropRight ~count:0 "abcdefg" = "abcdefg"
```

``` ocaml
String.dropRight ~count:7 "abcdefg" = ""
```

``` ocaml
String.dropRight ~count:(-2) "abcdefg" = "abcdefg"
```

``` ocaml
String.dropRight ~count:8 "abcdefg" = ""
```

### `val drop_right`

``` ocaml
val drop_right : string -> count:int -> string
```

### `val indexOf`

``` ocaml
val indexOf : string -> string -> int option
```

Returns the index of the first occurrence of `string` or None if string
has no occurences of `string`

Examples

``` ocaml
String.indexOf "Hello World World" "World" = Some 6 
```

``` ocaml
String.indexOf "Hello World World" "Bye" = None 
```

### `val index_of`

``` ocaml
val index_of : string -> string -> int option
```

### `val indexOfRight`

``` ocaml
val indexOfRight : string -> string -> int option
```

Returns the index of the last occurrence of `string` or None if string
has no occurences of `string`

Examples

``` ocaml
String.indexOfRight "Hello World World" "World" = Some 12 
```

``` ocaml
String.indexOfRight "Hello World World" "Bye" = None 
```

### `val index_of_right`

``` ocaml
val index_of_right : string -> string -> int option
```

### `val insertAt`

``` ocaml
val insertAt : string -> index:int -> value:t -> string
```

Insert a string at `index`.

The character previously at index will now follow the inserted string.

Examples

``` ocaml
String.insertAt ~value:"**" ~index:2 "abcde" = "ab**cde"
```

``` ocaml
String.insertAt ~value:"**" ~index:0 "abcde" = "**abcde"
```

``` ocaml
String.insertAt ~value:"**" ~index:5 "abcde" = "abcde**"
```

``` ocaml
String.insertAt ~value:"**" ~index:(-2) "abcde" = "abc**de"
```

``` ocaml
String.insertAt ~value:"**" ~index:(-9) "abcde" = "**abcde"
```

``` ocaml
String.insertAt ~value:"**" ~index:9 "abcde" = "abcde**"
```

### `val insert_at`

``` ocaml
val insert_at : string -> index:int -> value:t -> string
```

### `val toLowercase`

``` ocaml
val toLowercase : string -> string
```

Converts all upper case letters to lower case.

**Note** This function works only with ASCII characters, not Unicode.

Examples

``` ocaml
String.toLowercase "AaBbCc123" = "aabbcc123"
```

### `val to_lowercase`

``` ocaml
val to_lowercase : string -> string
```

### `val toUppercase`

``` ocaml
val toUppercase : string -> string
```

Converts all lower case letters to upper case.

**Note** This function works only with ASCII characters, not Unicode.

Examples

``` ocaml
String.toUppercase "AaBbCc123" = "AABBCC123"
```

### `val to_uppercase`

``` ocaml
val to_uppercase : string -> string
```

### `val uncapitalize`

``` ocaml
val uncapitalize : string -> string
```

Converts the first letter to lower case if it is upper case.

**Note** This function works only with ASCII characters, not Unicode.

Examples

``` ocaml
String.uncapitalize "Anastasia" = "anastasia"
```

### `val capitalize`

``` ocaml
val capitalize : string -> string
```

Converts the first letter of `s` to lowercase if it is upper case.

**Note** This function works only with ASCII characters, not Unicode.

Examples

``` ocaml
String.uncapitalize "den" = "Den"
```

### `val trim`

``` ocaml
val trim : string -> string
```

Removes leading and trailing whitespace from a string

Examples

``` ocaml
String.trim "  abc  " = "abc"
```

``` ocaml
String.trim "  abc def  " = "abc def"
```

``` ocaml
String.trim "\r\n\t abc \n\n" = "abc"
```

### `val trimLeft`

``` ocaml
val trimLeft : string -> string
```

Like [`trim`](#val-trim) but only drops characters from the beginning of
the string.

### `val trim_left`

``` ocaml
val trim_left : string -> string
```

### `val trim_right`

``` ocaml
val trim_right : string -> string
```

Like [`trim`](#val-trim) but only drops characters from the end of the
string.

### `val padLeft`

``` ocaml
val padLeft : string -> int -> with_:string -> string
```

Pad a string up to a minimum length

If the string is shorted than the proivded length, adds `with` to the
left of the string until the minimum length is met

Examples

``` ocaml
String.padLeft "5" 3 ~with_:"0" = "005"
```

### `val pad_left`

``` ocaml
val pad_left : string -> int -> with_:string -> string
```

### `val padRight`

``` ocaml
val padRight : string -> int -> with_:string -> string
```

Pad a string up to a minimum length

If the string is shorted than the proivded length, adds `with` to the
left of the string until the minimum length is met

Examples

``` ocaml
String.padRight "Ahh" 7 ~with_:"h" = "Ahhhhhh"
```

### `val pad_right`

``` ocaml
val pad_right : string -> int -> with_:string -> string
```

## [](#deconstruct)Deconstruct

### `val uncons`

``` ocaml
val uncons : string -> (char * string) option
```

Returns, as an `Option`, a tuple containing the first `Char` and the
remaining String.

If given an empty string, returns `None`.

Examples

``` ocaml
String.uncons "abcde" = Some ('a', "bcde")
```

``` ocaml
String.uncons "a" = Some ('a', "")
```

``` ocaml
String.uncons "" = None
```

### `val split`

``` ocaml
val split : string -> on:string -> string list
```

Divide a string into a list of strings, splitting whenever `on` is
encountered.

Examples

``` ocaml
String.split ~on:"/" "a/b/c" = ["a"; "b"; "c"]
String.split ~on:"--" "a--b--c" = ["a"; "b"; "c"]
String.split ~on:"/" "abc" = ["abc"]
String.split ~on:"/" "" = [""]
String.split ~on:"" "abc" = ["a"; "b"; "c"]
```

## [](#iterate)Iterate

### `val forEach`

``` ocaml
val forEach : string -> f:( char -> unit ) -> unit
```

Run `f` on each character in a string.

### `val for_each`

``` ocaml
val for_each : string -> f:( char -> unit ) -> unit
```

### `val fold`

``` ocaml
val fold : string -> initial:'a -> f:( 'a -> char -> 'a ) -> 'a
```

Like `Array`.fold but the elements are `Char`s

## [](#convert)Convert

### `val toArray`

``` ocaml
val toArray : string -> char array
```

Returns an `Array` of the individual characters in the given string.

Examples

``` ocaml
String.toArray "" = [||]
```

``` ocaml
String.toArray "abc" = [|'a'; 'b'; 'c'|]
```

### `val to_array`

``` ocaml
val to_array : string -> char array
```

### `val toList`

``` ocaml
val toList : string -> char list
```

Returns a `List` of the individual characters in the given string.

Examples

``` ocaml
String.toList "" = []
```

``` ocaml
String.toList "abc" = ['a'; 'b'; 'c']
```

### `val to_list`

``` ocaml
val to_list : string -> char list
```

## [](#compare)Compare

### `val equal`

``` ocaml
val equal : string -> string -> bool
```

Test two string for equality

### `val compare`

``` ocaml
val compare : string -> string -> int
```

Compare two strings. Strings use 'dictionary' ordering. 1 Also known as
[lexicographical
ordering](https://en.wikipedia.org/wiki/Lexicographical_order).

Examples

``` ocaml
String.compare "Z" "A" = 1
```

``` ocaml
String.compare "Be" "Bee" = -1
```

``` ocaml
String.compare "Pear" "pear" = 1
```

``` ocaml
String.compare "Peach" "Peach" = 0
```

### `type identity`

``` ocaml
type identity
```

The unique identity for `Comparator`

### `val comparator`

``` ocaml
val comparator : ( t, identity ) TableclothComparator.t
```
