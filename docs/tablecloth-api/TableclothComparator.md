# Module `TableclothComparator`

Comparator provide a way for custom data structures to be used with
`Map`s and `Set`s

Say we have a module `Book` which we want to be able to create a `Set`
of

``` ocaml
module Book = struct
  type t = {
    isbn: string;
    title: string;
  }

  let compare bookA bookB =
    String.compare bookA.isbn bookb.isbn
end
```

First we need to make our module conform to the
[`S`](TableclothComparator-module-type-S) signature.

This can be done by using the [`Make`](TableclothComparator-Make)
functor.

``` ocaml
module Book = struct
  type t = {
    isbn: string;
    title: string;
  }

  let compare bookA bookB =
    String.compare bookA.isbn bookb.isbn
  
  include Comparator.Make(struct 
    type nonrec t = t

    let compare = compare
  end)
end
```

Now we can create a Set of books

``` ocaml
Set.fromList (module Book) [
  { isbn="9788460767923"; title="Moby Dick or The Whale" }
]
```

### `module type T`

``` ocaml
module type T = sig ... end
```

T represents the input for the [`Make`](TableclothComparator-Make)
functor

### `type t`

``` ocaml
type ('a, 'identity) t
```

### `type comparator`

``` ocaml
type ('a, 'identity) comparator = ( 'a, 'identity ) t
```

This just is an alias for [`t`](#type-t)

### `module type S`

``` ocaml
module type S = sig ... end
```

The output type of [`Make`](TableclothComparator-Make).

### `type s`

``` ocaml
type ('a, 'identity) s =   (module S   with type identity = 'identity    and type t = 'a)
```

A type alias that is useful typing functions which accept first class
modules like `Map`.empty or `Set`.fromArray

### `module Make`

``` ocaml
module Make (M : T) : S with type t := M.t
```

Create a new comparator by providing a module which satisifies
[`T`](TableclothComparator-module-type-T).
