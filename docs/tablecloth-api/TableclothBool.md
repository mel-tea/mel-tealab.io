# Module `TableclothBool`

-   [Create](#create)
-   [Basic operations](#basic-operations)
-   [Convert](#convert)
-   [Compare](#compare)

Functions for working with boolean values.

Booleans in OCaml / Reason are represented by the `true` and `false`
literals.

Whilst a bool isnt a variant, you will get warnings if you haven't
exhaustively pattern match on them:

``` ocaml
let bool = false
let string =
  match bool with
  | false -> "false"
(*
  Warning 8: this pattern-matching is not exhaustive.
  Here is an example of a case that is not matched:
  true
*)
```

### `type t`

``` ocaml
type t = bool
```

## [](#create)Create

### `val fromInt`

``` ocaml
val fromInt : int -> bool option
```

Convert an `Int` into a `Bool`.

Examples

``` ocaml
Bool.fromInt 0 = Some false
```

``` ocaml
Bool.fromInt 1 = Some true
```

``` ocaml
Bool.fromInt 8 = None
```

``` ocaml
Bool.fromInt (-3) = None
```

### `val from_int`

``` ocaml
val from_int : int -> bool option
```

### `val fromString`

``` ocaml
val fromString : string -> bool option
```

Convert a `String` into a `Bool`.

Examples

``` ocaml
Bool.fromString "true" = Some true
```

``` ocaml
Bool.fromString "false" = Some false
```

``` ocaml
Bool.fromString "True" = None
```

``` ocaml
Bool.fromString "False" = None
```

``` ocaml
Bool.fromString "0" = None
```

``` ocaml
Bool.fromString "1" = None
```

``` ocaml
Bool.fromString "Not even close" = None
```

### `val from_string`

``` ocaml
val from_string : string -> bool option
```

## [](#basic-operations)Basic operations

### `val (&&)`

``` ocaml
val (&&) : bool -> bool -> bool
```

The lazy logical AND operator.

Returns `true` if both of its operands evaluate to `true`.

If the 'left' operand evaluates to `false`, the 'right' operand is not
evaluated.

Examples

``` ocaml
Bool.(true && true) = true
```

``` ocaml
Bool.(true && false) = false
```

``` ocaml
Bool.(false && true) = false
```

``` ocaml
Bool.(false && false) = false
```

### `val (||)`

``` ocaml
val (||) : bool -> bool -> bool
```

The lazy logical OR operator.

Returns `true` if one of its operands evaluates to `true`.

If the 'left' operand evaluates to `true`, the 'right' operand is not
evaluated.

Examples

``` ocaml
Bool.(true || true) = true
```

``` ocaml
Bool.(true || false) = true
```

``` ocaml
Bool.(false || true) = true
```

``` ocaml
Bool.(false || false) = false
```

### `val xor`

``` ocaml
val xor : bool -> bool -> bool
```

The exclusive or operator.

Returns `true` if **exactly one** of its operands is `true`.

Examples

``` ocaml
Bool.xor true true  = false
```

``` ocaml
Bool.xor true false = true
```

``` ocaml
Bool.xor false true  = true
```

``` ocaml
Bool.xor false false = false
```

### `val not`

``` ocaml
val not : t -> bool
```

Negate a `bool`.

Examples

``` ocaml
Bool.not false = true
```

``` ocaml
Bool.not true = false
```

## [](#convert)Convert

### `val toString`

``` ocaml
val toString : bool -> string
```

Convert a `bool` to a `String`

Examples

``` ocaml
Bool.toString true = "true"
```

``` ocaml
Bool.toString false = "false"
```

### `val to_string`

``` ocaml
val to_string : bool -> string
```

### `val toInt`

``` ocaml
val toInt : bool -> int
```

Convert a `bool` to an `Int`.

Examples

``` ocaml
Bool.toInt true = 1
```

``` ocaml
Bool.toInt false = 0
```

### `val to_int`

``` ocaml
val to_int : bool -> int
```

## [](#compare)Compare

### `val equal`

``` ocaml
val equal : bool -> bool -> bool
```

Test for the equality of two `bool` values.

Examples

``` ocaml
Bool.equal true true = true
```

``` ocaml
Bool.equal false false = true
```

``` ocaml
Bool.equal false true = false
```

### `val compare`

``` ocaml
val compare : bool -> bool -> int
```

Compare two boolean values

Examples

``` ocaml
Bool.compare true false = 1
```

``` ocaml
Bool.compare false true = -1
```

``` ocaml
Bool.compare true true = 0
```

``` ocaml
Bool.compare false false = 0
```
