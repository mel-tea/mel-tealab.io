# Module `TableclothSet.Poly`

Construct sets which can hold any data type using the polymorphic
`compare` function.

### `type identity`

``` ocaml
type identity
```

### `type t`

``` ocaml
type nonrec 'a t = ( 'a, identity ) t
```

### `val empty`

``` ocaml
val empty : unit -> 'a t
```

The empty set.

A great starting point.

### `val singleton`

``` ocaml
val singleton : 'a -> 'a t
```

Create a set of a single value

Examples

``` ocaml
Set.Int.singleton (5, "Emu") |> Set.toList = [(5, "Emu")]
```

### `val fromArray`

``` ocaml
val fromArray : 'a array -> 'a t
```

Create a set from an `Array`

Examples

``` ocaml
Set.Poly.fromArray [(1, "Ant");(2, "Bat");(2, "Bat")] |> Set.toList = [(1, "Ant"); (2, "Bat")]
```

### `val from_array`

``` ocaml
val from_array : 'a array -> 'a t
```

### `val fromList`

``` ocaml
val fromList : 'a list -> 'a t
```

Create a set from a `List`

Examples

``` ocaml
Set.Poly.fromList [(1, "Ant");(2, "Bat");(2, "Bat")] |> Set.toList = [(1, "Ant"); (2, "Bat")]
```

### `val from_list`

``` ocaml
val from_list : 'a list -> 'a t
```
