# Module `Internal`

### `val toBeltComparator`

``` ocaml
val toBeltComparator :    (module TableclothComparator.S with type identity = 'id and type t = 'a) ->   ( 'a0, 'id0 ) Belt.Id.comparable
```
