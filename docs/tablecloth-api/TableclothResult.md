# Module `TableclothResult`

-   [Create](#create)
-   [Convert](#convert)
-   [Compare](#compare)
-   [Operators](#operators)

A `Result` is used to represent a computation which may fail.

A `Result` is a variant, which has a constructor for successful results
`(Ok 'ok)`, and one for unsuccessful results (`(Error 'error)`).

``` ocaml
type ('ok, 'error) t =
  | Ok of 'ok
  | Error of 'error
```

Here is how you would annotate a `Result` variable whose `Ok` variant is
an integer and whose `Error` variant is a string:

``` ocaml
let ok: (int, string) Result.t = Ok 3
```

``` ocaml
let error: (int, string) Result.t = Error "This computation failed!"
```

**Note** The `'error` case can be of **any** type and while `string` is
very common you could also use:

-   `string List.t` to allow errors to be accumulated
-   `exn`, in which case the result type just makes exceptions explicit
    in the return type
-   A variant or polymorphic variant, with one case per possible error.
    This is means each error can be dealt with explicitly. See [this
    excellent
    article](https://keleshev.com/composable-error-handling-in-ocaml)
    for mnore information on this approach.

If the function you are writing can only fail in a single obvious way,
maybe you want an `Option` instead.

### `type t`

``` ocaml
type ('ok, 'error) t = ( 'ok, 'error ) Stdlib.result
```

## [](#create)Create

### `val ok`

``` ocaml
val ok : 'ok -> ( 'ok, 'error ) t
```

A function alternative to the `Ok` constructor which can be used in
places where the constructor isn't permitted such as at the of a
`Fun`.(|\>) or functions like `List`.map.

Examples

``` ocaml
String.reverse "desserts" |> Result.ok = Ok "stressed"
```

``` ocaml
List.map [1; 2; 3] ~f:Result.ok = [Ok 1; Ok 2; Ok 3]
```

### `val error`

``` ocaml
val error : 'error -> ( 'ok, 'error ) t
```

A function alternative to the `Error` constructor which can be used in
places where the constructor isn't permitted such as at the of a
`Fun`.pipe or functions like `List`.map.

**Note**

When targetting the Bucklescript compiler you **can** use constructors
with the fast pipe.

``` ocaml
5 |. Ok = (Ok 5)
```

See the [Reason
docs](https://reasonml.github.io/docs/en/pipe-first#pipe-into-variants)
for more.

Examples

``` ocaml
Int.negate 3 |> Result.error 3 = Error (-3)
```

``` ocaml
List.map [1; 2; 3] ~f:Result.error = [Error 1; Error 2; Error 3]
```

### `val attempt`

``` ocaml
val attempt : ( unit -> 'ok ) -> ( 'ok, exn ) t
```

Run the provided function and wrap the returned value in a `Result`,
catching any exceptions raised.

Examples

``` ocaml
Result.attempt (fun () -> 5 / 0) = Error Division_by_zero
```

``` ocaml
let numbers = [|1,2,3|] in
Result.attempt (fun () -> numbers.(3)) =
  Error (Invalid_argument "index out of bounds")
```

### `val fromOption`

``` ocaml
val fromOption : 'ok option -> error:'error -> ( 'ok, 'error ) t
```

Convert an `Option` to a `Result` where a `(Some value)` becomes
`(Ok value)` and a `None` becomes `(Error error)`.

Examples

``` ocaml
Result.fromOption (Some 84) ~error:"Greater than 100" = Ok 8
```

``` ocaml
Result.fromOption None ~error:"Greater than 100" =
  Error "Greater than 100"
```

### `val from_option`

``` ocaml
val from_option : 'ok option -> error:'error -> ( 'ok, 'error ) t
```

### `val isOk`

``` ocaml
val isOk : ( _, _ ) t -> bool
```

Check if a `Result` is an `Ok`.

Useful when you want to perform some side affect based on the presence
of an `Ok` like logging.

**Note** if you need access to the contained value rather than doing
`Result.isOk` followed by `Result`.unwrapUnsafe its safer and just as
convenient to use pattern matching directly or use one of
`Result`.andThen or `Result`.map

Examples

``` ocaml
Result.isOk (Ok 3) = true
```

``` ocaml
Result.isOk (Error 3) = false
```

### `val is_ok`

``` ocaml
val is_ok : ( _, _ ) t -> bool
```

### `val isError`

``` ocaml
val isError : ( _, _ ) t -> bool
```

Check if a `Result` is an `Error`.

Useful when you want to perform some side affect based on the presence
of an `Error` like logging.

**Note** if you need access to the contained value rather than doing
`Result`.isOk followed by `Result`.unwrapUnsafe its safer and just as
convenient to use pattern matching directly or use one of
`Result`.andThen or `Result`.map

Examples

``` ocaml
Result.isError (Ok 3) = false
```

``` ocaml
Result.isError (Error 3) = true
```

### `val is_error`

``` ocaml
val is_error : ( _, _ ) t -> bool
```

### `val and_`

``` ocaml
val and_ : ( 'ok, 'error ) t -> ( 'ok, 'error ) t -> ( 'ok, 'error ) t
```

Returns the first argument if it [`isError`](#val-isError), otherwise
return the second argument.

Unlike the `Bool`.(&&) operator, the `and_` function does not
short-circuit. When you call `and_`, both arguments are evaluated before
being passed to the function.

Examples

``` ocaml
Result.and_ (Ok "Antelope") (Ok "Salmon") = Ok "Salmon"
```

``` ocaml
Result.and_
  (Error (`UnexpectedBird "Finch"))
  (Ok "Salmon")
  = Error (`UnexpectedBird "Finch")
```

``` ocaml
Result.and_
  (Ok "Antelope")
  (Error (`UnexpectedBird "Finch"))
    = Error (`UnexpectedBird "Finch")
```

``` ocaml
Result.and_
  (Error (`UnexpectedInvertabrate "Honey bee"))
  (Error (`UnexpectedBird "Finch"))
    = Error (`UnexpectedBird "Honey Bee")
```

### `val or_`

``` ocaml
val or_ : ( 'ok, 'error ) t -> ( 'ok, 'error ) t -> ( 'ok, 'error ) t
```

Return the first argument if it [`isOk`](#val-isOk), otherwise return
the second.

Unlike the built in `||` operator, the `or_` function does not
short-circuit. When you call `or_`, both arguments are evaluated before
being passed to the function.

Examples

``` ocaml
Result.or_ (Ok "Boar") (Ok "Gecko") = (Ok "Boar")
```

``` ocaml
Result.or_ (Error (`UnexpectedInvertabrate "Periwinkle")) (Ok "Gecko") = (Ok "Gecko")
```

``` ocaml
Result.or_ (Ok "Boar") (Error (`UnexpectedInvertabrate "Periwinkle")) = (Ok "Boar") 
```

``` ocaml
Result.or_ (Error (`UnexpectedInvertabrate "Periwinkle")) (Error (`UnexpectedBird "Robin")) = (Error (`UnexpectedBird "Robin"))
```

### `val orElse`

``` ocaml
val orElse : ( 'ok, 'error ) t -> ( 'ok, 'error ) t -> ( 'ok, 'error ) t
```

Return the second argument if it [`isOk`](#val-isOk), otherwise return
the first.

Like [`or_`](#val-or_) but in reverse. Useful when using the `|>`
operator

Examples

``` ocaml
Result.orElse (Ok "Boar") (Ok "Gecko") = (Ok "Gecko")
```

``` ocaml
Result.orElse (Error (`UnexpectedInvertabrate "Periwinkle")) (Ok "Gecko") = (Ok "Gecko")
```

``` ocaml
Result.orElse (Ok "Boar") (Error (`UnexpectedInvertabrate "Periwinkle")) = (Ok "Boar") 
```

``` ocaml
Result.orElse (Error (`UnexpectedInvertabrate "Periwinkle")) (Error (`UnexpectedBird "Robin")) = (Error (`UnexpectedInvertabrate "Periwinkle"))
```

### `val or_else`

``` ocaml
val or_else : ( 'ok, 'error ) t -> ( 'ok, 'error ) t -> ( 'ok, 'error ) t
```

### `val both`

``` ocaml
val both : ( 'a, 'error ) t -> ( 'b, 'error ) t -> ( 'a * 'b, 'error ) t
```

Combine two results, if both are `Ok` returns an `Ok` containing a
`Tuple` of the values.

If either is an `Error`, returns the `Error`.

The same as writing `Result.map2 ~f:Tuple.make`

Examples

``` ocaml
Result.both (Ok "Badger") (Ok "Rhino") = Ok ("Dog", "Rhino")
```

``` ocaml
Result.both (Error (`UnexpectedBird "Flamingo")) (Ok "Rhino") =
  (Error (`UnexpectedBird "Flamingo"))
```

``` ocaml
Result.both
  (Ok "Badger")
  (Error (`UnexpectedInvertabrate "Blue ringed octopus")) =
    (Error (`UnexpectedInvertabrate "Blue ringed octopus"))
```

``` ocaml
Result.both
  (Error (`UnexpectedBird "Flamingo"))
  (Error (`UnexpectedInvertabrate "Blue ringed octopus")) =
    (Error (`UnexpectedBird "Flamingo"))
```

### `val flatten`

``` ocaml
val flatten : ( ( 'ok, 'error ) t, 'error ) t -> ( 'ok, 'error ) t
```

Collapse a nested result, removing one layer of nesting.

Examples

``` ocaml
Result.flatten (Ok (Ok 2)) = Ok 2
```

``` ocaml
Result.flatten (Ok (Error (`UnexpectedBird "Peregrin falcon"))) =
  (Error (`UnexpectedBird "Peregrin falcon"))
```

``` ocaml
Result.flatten (Error (`UnexpectedInvertabrate "Woodlouse")) =
  (Error (`UnexpectedInvertabrate "Woodlouse"))
```

### `val unwrap`

``` ocaml
val unwrap : ( 'ok, 'error ) t -> default:'ok -> 'ok
```

Unwrap a Result using the `~default` value in case of an `Error`

Examples

``` ocaml
Result.unwrap ~default:0 (Ok 12) = 12
```

``` ocaml
Result.unwrap ~default:0 ((Error (`UnexpectedBird "Ostrich"))) = 0
```

### `val unwrapLazy`

``` ocaml
val unwrapLazy : ( 'ok, 'error ) t -> default:'ok Stdlib.Lazy.t -> 'ok
```

Unwrap a Result using the `Lazy.force default` value in case of an
`Error`

Examples

``` ocaml
Result.unwrap ~default:(lazy 0) (Ok 12) = 12
```

``` ocaml
Result.unwrap ~default:(lazy 0) ((Error (`UnexpectedBird "Ostrich"))) = 0
```

### `val unwrapUnsafe`

``` ocaml
val unwrapUnsafe : ( 'ok, _ ) t -> 'ok
```

Unwrap a Result, raising an exception in case of an `Error`

*Exceptions*

Raises an `Invalid_argument "Result.unwrapUnsafe called with an Error"`
exception.

Examples

``` ocaml
Result.unwrapUnsafe (Ok 12) = 12
```

``` ocaml
Result.unwrapUnsafe (Error "bad") 
```

### `val unwrap_unsafe`

``` ocaml
val unwrap_unsafe : ( 'ok, _ ) t -> 'ok
```

### `val unwrapError`

``` ocaml
val unwrapError : ( 'ok, 'error ) t -> default:'error -> 'error
```

Like `Result`.unwrap but unwraps an `Error` value instead

Examples

``` ocaml
Result.unwrapError
  (Error (`UnexpectedBird "Swallow"))
  ~default:(`UnexpectedInvertabrate "Ladybird") =
    `UnexpectedBird "Swallow"
```

``` ocaml
Result.unwrapError
  (Ok 5)
  ~default:(`UnexpectedInvertabrate "Ladybird") =
    `UnexpectedInvertabrate "Ladybird"
```

### `val unwrap_error`

``` ocaml
val unwrap_error : ( 'ok, 'error ) t -> default:'error -> 'error
```

### `val map2`

``` ocaml
val map2 :    ( 'a, 'error ) t ->   ( 'b, 'error ) t ->   f:( 'a -> 'b -> 'c ) ->   ( 'c, 'error ) t
```

Combine two results

If one of the results is an `Error`, that becomes the return result.

If both are `Error` values, returns its first.

Examples

``` ocaml
Result.map2 (Ok 7) (Ok 3) ~f:Int.add = Ok 10
```

``` ocaml
Result.map2 (Error "A") (Ok 3) ~f:Int.add = Error "A"
```

``` ocaml
Result.map2 (Ok 7) (Error "B") ~f:Int.add = Error "B"
```

``` ocaml
Result.map2 (Error "A") (Error "B") ~f:Int.add = Error "A"
```

### `val values`

``` ocaml
val values : ( 'ok, 'error ) t list -> ( 'ok list, 'error ) t
```

If all of the elements of a list are `Ok`, returns an `Ok` of the the
list of unwrapped values.

If **any** of the elements are an `Error`, the first one encountered is
returned.

Examples

``` ocaml
Result.values [Ok 1; Ok 2; Ok 3; Ok 4] = Ok [1; 2; 3; 4]
```

``` ocaml
Result.values [Ok 1; Error "two"; Ok 3; Error "four"] = Error "two"
```

### `val combine`

``` ocaml
val combine :    ( 'ok, 'error ) Stdlib.result list ->   ( 'ok list, 'error ) Stdlib.result
```

`Result.combine results` takes a list of `Result` values. If all the
elements in `results` are of the form `Ok x`, then `Result.combine`
creates a list `xs` of all the values extracted from their `Ok`s, and
returns `Ok xs` If any of the elements in `results` are of the form
`Error err`, the first of them is returned as the result of
`Result.combine`.

``` ocaml
Result.combine [Ok 1; Ok 2; Ok 3; Ok 4] = Ok [1; 2; 3; 4]
Result.combine [Ok 1; Error "two"; Ok 3; Error "four"] = Error "two"
```

### `val map`

``` ocaml
val map : ( 'a, 'error ) t -> f:( 'a -> 'b ) -> ( 'b, 'error ) t
```

Transforms the `'ok` in a result using `f`. Leaves the `'error`
untouched.

Examples

``` ocaml
Result.map (Ok 3) ~f:(Int.add 1) = Ok 9
```

``` ocaml
Result.map (Error "three") ~f:(Int.add 1) = Error "three"
```

### `val mapError`

``` ocaml
val mapError : ( 'ok, 'a ) t -> f:( 'a -> 'b ) -> ( 'ok, 'b ) t
```

Transforms the value in an `Error` using `f`. Leaves an `Ok` untouched.

Examples

``` ocaml
Result.mapError (Ok 3) ~f:String.reverse = Ok 3
```

``` ocaml
Result.mapError (Error "bad") ~f:(Int.add 1)  = Error "bad"
```

### `val map_error`

``` ocaml
val map_error : ( 'ok, 'a ) t -> f:( 'a -> 'b ) -> ( 'ok, 'b ) t
```

### `val andThen`

``` ocaml
val andThen :    ( 'a, 'error ) t ->   f:( 'a -> ( 'b, 'error ) t ) ->   ( 'b, 'error ) t
```

Run a function which may fail on a result.

Short-circuits of called with an `Error`.

Examples

``` ocaml
let reciprical (x:float) : (string, float) Standard.Result.t = (
  if (x = 0.0) then
    Error "Divide by zero"
  else
    Ok (1.0 /. x)
)

let root (x:float) : (string, float) Standard.Result.t = (
  if (x < 0.0) then
    Error "Cannot be negative"
  else
    Ok (Float.squareRoot x)
)
```

Examples

``` ocaml
Result.andThen ~f:reciprical (Ok 4.0) = Ok 0.25
```

``` ocaml
Result.andThen ~f:reciprical (Error "Missing number!") = Error "Missing number!"
```

``` ocaml
Result.andThen ~f:reciprical (Ok 0.0) = Error "Divide by zero"
```

``` ocaml
Result.andThen (Ok 4.0) ~f:root  |> Result.andThen ~f:reciprical = Ok 0.5
```

``` ocaml
Result.andThen (Ok -2.0) ~f:root |> Result.andThen ~f:reciprical = Error "Cannot be negative"
```

``` ocaml
Result.andThen (Ok 0.0) ~f:root |> Result.andThen ~f:reciprical = Error "Divide by zero"
```

### `val and_then`

``` ocaml
val and_then :    ( 'a, 'error ) t ->   f:( 'a -> ( 'b, 'error ) t ) ->   ( 'b, 'error ) t
```

### `val tap`

``` ocaml
val tap : ( 'ok, _ ) t -> f:( 'ok -> unit ) -> unit
```

Run a function against an `(Ok value)`, ignores `Error`s.

Examples

``` ocaml
Result.tap (Ok "Dog") ~f:print_endline
(* prints "Dog" *)
```

## [](#convert)Convert

### `val toOption`

``` ocaml
val toOption : ( 'ok, _ ) t -> 'ok option
```

Convert a `Result` to an `Option`.

An `Ok x` becomes `Some x`

An `Error _` becomes `None`

Examples

``` ocaml
Result.toOption (Ok 42) = Some 42
```

``` ocaml
Result.toOption (Error "Missing number!") = None
```

### `val to_option`

``` ocaml
val to_option : ( 'ok, _ ) t -> 'ok option
```

## [](#compare)Compare

### `val equal`

``` ocaml
val equal :    ( 'ok -> 'ok -> bool ) ->   ( 'error -> 'error -> bool ) ->   ( 'ok, 'error ) t ->   ( 'ok, 'error ) t ->   bool
```

Test two results for equality using the provided functions.

Examples

``` ocaml
Result.equal String.equal Int.equal (Ok 3) (Ok 3) = true
```

``` ocaml
Result.equal String.equal Int.equal (Ok 3) (Ok 4) = false
```

``` ocaml
Result.equal String.equal Int.equal (Error "Fail") (Error "Fail") = true
```

``` ocaml
Result.equal String.equal Int.equal (Error "Expected error") (Error "Unexpected error") = false
```

``` ocaml
Result.equal String.equal Int.equal (Error "Fail") (Ok 4) = false
```

### `val compare`

``` ocaml
val compare :    ( 'ok -> 'ok -> int ) ->   ( 'error -> 'error -> int ) ->   ( 'ok, 'error ) t ->   ( 'ok, 'error ) t ->   int
```

Compare results for using the provided functions.

In the case when one of the results is an `Error` and one is `Ok`,
`Error`s are considered 'less' then `Ok`s

Examples

``` ocaml
Result.compare String.compare Int.compare (Ok 3) (Ok 3) = 0
```

``` ocaml
Result.compare String.compare Int.compare (Ok 3) (Ok 4) = -1
```

``` ocaml
Result.compare String.compare Int.compare (Error "Fail") (Error "Fail") = 0
```

``` ocaml
Result.compare String.compare Int.compare (Error "Fail") (Ok 4) = -1
```

``` ocaml
Result.compare String.compare Int.compare (Ok 4) (Error "Fail") = 1
```

``` ocaml
Result.compare String.compare Int.compare (Error "Expected error") (Error "Unexpected error") = -1
```

## [](#operators)Operators

In functions that make heavy use of `Result`s operators can make code
significantly more concise at the expense of placing a greater cognitive
burden on future readers.

### `val (|?)`

``` ocaml
val (|?) : ( 'a, 'error ) t -> 'a -> 'a
```

An operator version of `Result`.unwrap where the `default` value goes to
the right of the operator.

Examples

The following eamples assume `open Result.Infix` is in scope.

``` ocaml
Ok 4 |? 8 = 4
```

``` ocaml
Error "Missing number!" |? 8 = 8
```

### `val (>>=)`

``` ocaml
val (>>=) :    ( 'ok, 'error ) t ->   ( 'ok -> ( 'b, 'error ) t ) ->   ( 'b, 'error ) t
```

An operator version of [`andThen`](#val-andThen)

Examples

The following examples assume

``` ocaml
open Result.Infix

let reciprical (x:float) : (string, float) Standard.Result.t =
  if (x = 0.0) then
    Error "Divide by zero"
  else
    Ok (1.0 /. x)
```

Is in scope.

``` ocaml
Ok 4. >>= reciprical = Ok 0.25
```

``` ocaml
Error "Missing number!" >>= reciprical = Error "Missing number!"
```

``` ocaml
Ok 0. >>= reciprical = Error "Divide by zero"
```

### `val (>>|)`

``` ocaml
val (>>|) : ( 'a, 'error ) t -> ( 'a -> 'b ) -> ( 'b, 'error ) t
```

An operator version of [`map`](#val-map)

Examples

The following examples assume `open Result.Infix` is in scope.

``` ocaml
Ok 4 >>| Int.add(1) = Ok 5
```

``` ocaml
Error "Its gone bad" >>| Int.add(1) = Error "Its gone bad"
```
