# Module `TableclothSet.String`

Construct sets of [`String`](#)s

### `type t`

``` ocaml
type nonrec t = Of(TableclothString).t
```

### `val empty`

``` ocaml
val empty : t
```

A set with nothing in it.

### `val singleton`

``` ocaml
val singleton : string -> t
```

Create a set of a single [`String`](#)

Examples

``` ocaml
Set.String.singleton "Bat" |> Set.toList = ["Bat"]
```

### `val fromArray`

``` ocaml
val fromArray : string array -> t
```

Create a set from an `Array`

Examples

``` ocaml
Set.String.fromArray [|"a";"b";"g";"b";"g";"a";"a"|] |> Set.toArray = [|"a";"b";"g"|]
```

### `val from_array`

``` ocaml
val from_array : string array -> t
```

### `val fromList`

``` ocaml
val fromList : string list -> t
```

Create a set from a `List`

Examples

``` ocaml
Set.String.fromList [|"a";"b";"g";"b";"g";"a";"a"|] |> Set.toList = ["a";"b";"g"]
```

### `val from_list`

``` ocaml
val from_list : string list -> t
```
