# Module `TableclothSet`

-   [Create](#create)
-   [Basic operations](#basic-operations)
-   [Query](#query)
-   [Combine](#combine)
-   [Transform](#transform)
-   [Convert](#convert)

A `Set` represents a collection of unique values.

`Set` is an immutable data structure which means operations like
`Set`.add and `Set`.remove do not modify the data structure, but return
a new set with the desired changes.

Since sets of `int`s and `string`s are so common the specialised
`Set`.Int and `Set`.String modules are available which offer a
convenient way to construct new sets.

Custom data types can be used with sets as long as the module satisfies
the `Comparator`.S interface.

``` ocaml
module Point = struct
  type t = int * int
  let compare = Tuple2.compare Int.compare Int.compare
  include Comparator.Make(struct
    type nonrec t = t
    let compare = compare
  end)
end

let points : Set.Of(Point).t = Set.fromList (module Points) [(0, 0); (3, 4); (6, 7)]
```

See the `Comparator` module for a more details.

### `type t`

``` ocaml
type ('a, 'id) t = ( 'a, 'id ) Belt.Set.t
```

### `module Of`

``` ocaml
module Of (M : TableclothComparator.S) : sig ... end
```

This functor lets you describe the type of Sets a little more concisely.

## [](#create)Create

You can create a Set by providing a module conform to the `Comparator`.S
signature by using [`empty`](#val-empty), [`singleton`](#val-singleton),
[`fromList`](#val-fromList) or [`fromArray`](#val-fromArray).

Specialised versions of the [`empty`](#val-empty),
[`singleton`](#val-singleton), [`fromList`](#val-fromList) and
[`fromArray`](#val-fromArray) functions available in the `Set`.Int and
`Set`.String sub-modules.

### `val empty`

``` ocaml
val empty : ( 'a, 'identity ) TableclothComparator.s -> ( 'a, 'identity ) t
```

A set with nothing in it.

Often used as an initial value for functions like `Array`.fold

Examples

``` ocaml
Array.fold 
  [|'m'; 'i'; 's'; 's'; 'i'; 's'; 's';'i';'p';'p';'i'|] 
  ~intial:(Set.empty (module Char))
  ~f:Set.add
|> Set.toArray
= [|'i'; 'm'; 'p'; 's'|] 
```

### `val singleton`

``` ocaml
val singleton :    ( 'a, 'identity ) TableclothComparator.s ->   'a ->   ( 'a, 'identity ) t
```

Create a set from a single [`Int`](TableclothSet-Int)

Examples

``` ocaml
Set.singleton (module Int) 7 |> Set.toList = [7]
```

### `val fromArray`

``` ocaml
val fromArray :    ( 'a, 'identity ) TableclothComparator.s ->   'a array ->   ( 'a, 'identity ) t
```

Create a set from an `Array`

Examples

``` ocaml
Set.fromArray (module String) [|"Ant"; "Bat"; "Bat"; "Goldfish"|] |> Set.toArray = [|"Ant";"Bat";"Goldfish"|]
```

### `val from_array`

``` ocaml
val from_array :    ( 'a, 'identity ) TableclothComparator.s ->   'a array ->   ( 'a, 'identity ) t
```

### `val fromList`

``` ocaml
val fromList :    ( 'a, 'identity ) TableclothComparator.s ->   'a list ->   ( 'a, 'identity ) t
```

Create a set from a `List`

Examples

``` ocaml
Set.fromList (module Char) ['A'; 'B'; 'B'; 'G'] |> Set.toList = ['A';'B';'G']
```

### `val from_list`

``` ocaml
val from_list :    ( 'a, 'identity ) TableclothComparator.s ->   'a list ->   ( 'a, 'identity ) t
```

## [](#basic-operations)Basic operations

### `val add`

``` ocaml
val add : ( 'a, 'id ) t -> 'a -> ( 'a, 'id ) t
```

Insert a value into a set.

Examples

``` ocaml
Set.add (Set.Int.fromList [1; 2]) 3 |> Set.toList = [1; 2; 3]
```

``` ocaml
Set.add (Set.Int.fromList [1; 2]) 2 |> Set.toList = [1; 2]
```

### `val remove`

``` ocaml
val remove : ( 'a, 'id ) t -> 'a -> ( 'a, 'id ) t
```

Remove a value from a set, if the set doesn't contain the value anyway,
returns the original set

Examples

``` ocaml
Set.remove (Set.Int.fromList [1; 2]) 2 |> Set.toList = [1]
```

``` ocaml
let originalSet = Set.Int.fromList [1; 2] in
let newSet = Set.remove orignalSet 3 in
originalSet = newSet
```

### `val includes`

``` ocaml
val includes : ( 'a, _ ) t -> 'a -> bool
```

Determine if a value is in a set

Examples

``` ocaml
Set.includes (Set.String.fromList ["Ant"; "Bat"; "Cat"]) "Bat" = true
```

### `val (.?{})`

``` ocaml
val (.?{}) : ( 'element, _ ) t -> 'element -> bool
```

The [index
operator](https://caml.inria.fr/pub/docs/manual-ocaml/indexops.html)
version of [`includes`](#val-includes)

**Note** Currently this is only supported by the OCaml syntax.

Examples

``` ocaml
let animals = Set.String.fromList ["Ant"; "Bat"; "Cat"] in

animals.Set.?{"Emu"} = false
```

### `val length`

``` ocaml
val length : ( _, _ ) t -> int
```

Determine the number of elements in a set.

Examples

``` ocaml
Set.length (Set.Int.fromList [1; 2; 3]) = 3
```

### `val find`

``` ocaml
val find : ( 'value, _ ) t -> f:( 'value -> bool ) -> 'value option
```

Returns, as an `Option`, the first element for which `f` evaluates to
`true`. If `f` doesn't return `true` for any of the elements `find` will
return `None`.

Examples

``` ocaml
Set.find ~f:Int.isEven (Set.Int.fromList [1; 3; 4; 8]) = Some 4
```

``` ocaml
Set.find ~f:Int.isOdd (Set.Int.fromList [0; 2; 4; 8]) = None
```

``` ocaml
Set.find ~f:Int.isEven Set.Int.empty = None
```

## [](#query)Query

### `val isEmpty`

``` ocaml
val isEmpty : ( _, _ ) t -> bool
```

Check if a set is empty.

Examples

``` ocaml
Set.isEmpty (Set.Int.empty) = true
```

``` ocaml
Set.isEmpty (Set.Int.singleton 4) = false
```

### `val is_empty`

``` ocaml
val is_empty : ( _, _ ) t -> bool
```

### `val any`

``` ocaml
val any : ( 'value, _ ) t -> f:( 'value -> bool ) -> bool
```

Determine if `f` returns true for `any` values in a set.

Examples

``` ocaml
Set.any (Set.Int.fromArray [|2;3|]) ~f:Int.isEven = true
```

``` ocaml
Set.any (Set.Int.fromList [1;3]) ~f:Int.isEven = false
```

``` ocaml
Set.any (Set.Int.fromList []) ~f:Int.isEven = false
```

### `val all`

``` ocaml
val all : ( 'value, _ ) t -> f:( 'value -> bool ) -> bool
```

Determine if `f` returns true for `all` values in a set.

Examples

``` ocaml
Set.all ~f:Int.isEven (Set.Int.fromArray [|2;4|]) = true
```

``` ocaml
Set.all ~f:Int.isEven (Set.Int.fromLis [2;3]) = false
```

``` ocaml
Set.all ~f:Int.isEven Set.Int.empty = true
```

## [](#combine)Combine

### `val difference`

``` ocaml
val difference : ( 'a, 'id ) t -> ( 'a, 'id ) t -> ( 'a, 'id ) t
```

Returns a new set with the values from the first set which are not in
the second set.

Examples

``` ocaml
Set.difference (Set.Int.fromList [1;2;5]) (Set.Int.fromList [2;3;4]) |> Set.toList = [1;5]
```

``` ocaml
Set.difference (Set.Int.fromList [2;3;4]) (Set.Int.fromList [1;2;5]) |> Set.toList = [3;4]
```

### `val intersection`

``` ocaml
val intersection : ( 'a, 'id ) t -> ( 'a, 'id ) t -> ( 'a, 'id ) t
```

Get the intersection of two sets. Keeps values that appear in both sets.

Examples

``` ocaml
Set.intersection (Set.Int.fromList [1;2;5]) (Set.Int.fromList [2;3;4]) |> Set.toList= [2]
```

### `val union`

``` ocaml
val union : ( 'a, 'id ) t -> ( 'a, 'id ) t -> ( 'a, 'id ) t
```

Get the union of two sets. Keep all values.

Examples

``` ocaml
Set.union (Set.Int.fromList [1;2;5]) (Set.Int.fromList [2;3;4]) |> Set.toList = [1;2;3;4;5]
```

## [](#transform)Transform

### `val filter`

``` ocaml
val filter : ( 'a, 'id ) t -> f:( 'a -> bool ) -> ( 'a, 'id ) t
```

Keep elements that `f` returns `true` for.

Examples

``` ocaml
Set.filter (Set.Int.fromList [1;2;3]) ~f:Int.isEven |> Set.toList = [2]
```

### `val partition`

``` ocaml
val partition :    ( 'a, 'id ) t ->   f:( 'a -> bool ) ->   ( 'a, 'id ) t * ( 'a, 'id ) t
```

Divide a set into two according to `f`. The first set will contain the
values that `f` returns `true` for, values that `f` returns `false` for
will end up in the second.

Examples

``` ocaml
let numbers = Set.Int.fromList [1; 1; 5; 6; 5; 7; 9; 8] in
let (evens, odds) = Set.partition numbers ~f:Int.isEven in
Set.toList evens = [6; 8]
Set.toList odds = [1; 5; 7; 9]
```

### `val fold`

``` ocaml
val fold : ( 'a, _ ) t -> initial:'b -> f:( 'b -> 'a -> 'b ) -> 'b
```

Transform a set into a value which is result of running each element in
the set through `f`, where each successive invocation is supplied the
return value of the previous.

See `Array`.fold for a more in-depth explanation.

Examples

``` ocaml
Set.fold ~f:( * ) ~initial:1 (Set.Int.fromList [1;2;3;4]) = 24
```

### `val forEach`

``` ocaml
val forEach : ( 'a, _ ) t -> f:( 'a -> unit ) -> unit
```

Runs a function `f` against each element of the set.

### `val for_each`

``` ocaml
val for_each : ( 'a, _ ) t -> f:( 'a -> unit ) -> unit
```

## [](#convert)Convert

### `val toArray`

``` ocaml
val toArray : ( 'a, _ ) t -> 'a array
```

Converts a set into an `Array`

### `val to_array`

``` ocaml
val to_array : ( 'a, _ ) t -> 'a array
```

### `val toList`

``` ocaml
val toList : ( 'a, _ ) t -> 'a list
```

Converts a set into a `List`.

### `val to_list`

``` ocaml
val to_list : ( 'a, _ ) t -> 'a list
```

### `module Poly`

``` ocaml
module Poly : sig ... end
```

Construct sets which can hold any data type using the polymorphic
`compare` function.

### `module Int`

``` ocaml
module Int : sig ... end
```

Construct sets of [`Int`](TableclothSet-Int)s

### `module String`

``` ocaml
module String : sig ... end
```

Construct sets of [`String`](TableclothSet-String)s
