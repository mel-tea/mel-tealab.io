# Module `TableclothArray`

-   [Create](#create)
-   [Basic operations](#basic-operations)
-   [Query](#query)
-   [Transform](#transform)
-   [Combine](#combine)
-   [Deconstruct](#deconstruct)
-   [Iterate](#iterate)
-   [Convert](#convert)
-   [Compare](#compare)

A mutable vector of elements which must have the same type.

Has constant time (O(1)) [`get`](#val-get), [`set`](#val-set) and
[`length`](#val-length) operations.

Arrays have a fixed length, if you want to be able to add an arbitrary
number of elements maybe you want a `List`.

### `type t`

``` ocaml
type 'a t = 'a array
```

## [](#create)Create

You can create an `array` in OCaml with the `[|1; 2; 3|]` syntax.

### `val singleton`

``` ocaml
val singleton : 'a -> 'a t
```

Create an array with only one element.

Examples

``` ocaml
Array.singleton 1234 = [|1234|]
```

``` ocaml
Array.singleton "hi" = [|"hi"|]
```

### `val repeat`

``` ocaml
val repeat : 'a -> length:int -> 'a t
```

Creates an array of length `length` with the value `x` populated at each
index.

Examples

``` ocaml
Array.repeat ~length:5 'a' = [|'a'; 'a'; 'a'; 'a'; 'a'|]
```

``` ocaml
Array.repeat ~length:0 7 = [||]
```

``` ocaml
Array.repeat ~length:(-1) "Why?" = [||]
```

### `val range`

``` ocaml
val range : ?from:int -> int -> int t
```

Creates an array containing all of the integers from `from` if it is
provided or `0` if not, up to but not including `to`

Examples

``` ocaml
Array.range 5 = [|0; 1; 2; 3; 4|] 
```

``` ocaml
Array.range ~from:2 5 = [|2; 3; 4|] 
```

``` ocaml
Array.range ~from:(-2) 3 = [|-2; -1; 0; 1; 2|] 
```

### `val initialize`

``` ocaml
val initialize : int -> f:( int -> 'a ) -> 'a t
```

Initialize an array. `Array.initialize n ~f` creates an array of length
`n` with the element at index `i` initialized to the result of `(f i)`.

Examples

``` ocaml
Array.initialize 4 ~f:identity = [|0; 1; 2; 3|]
```

``` ocaml
Array.initialize 4 ~f:(fun n -> n * n) = [|0; 1; 4; 9|]
```

### `val fromList`

``` ocaml
val fromList : 'a list -> 'a t
```

Create an array from a `List`.

Examples

``` ocaml
Array.fromList [1;2;3] = [|1;2;3|]
```

### `val from_list`

``` ocaml
val from_list : 'a list -> 'a t
```

### `val clone`

``` ocaml
val clone : 'a t -> 'a t
```

Create a shallow copy of an array.

Examples

``` ocaml
let numbers = [|1;2;3|] in
let otherNumbers = Array.copy numbers in
numbers.(1) <- 9;
numbers = [|1;9;3|];
otherNumbers = [|1;2;3|];
```

``` ocaml
let numberGrid = [|
  [|1;2;3|];
  [|4;5;6|];
  [|7;8;9|];
|] in

let numberGridCopy = Array.copy numberGrid in

numberGrid.(1).(1) <- 0;

numberGridCopy.(1).(1) = 9;
```

## [](#basic-operations)Basic operations

### `val get`

``` ocaml
val get : 'a t -> int -> 'a
```

Get the element at the specified index.

The first element has index number 0.

The last element has index number `Array.length a - 1`.

You should prefer using the dedicated literal syntax;

``` ocaml
array.(n)
```

Or using the safer `Array`.getAt function.

Exceptions

Raises `Invalid_argument "index out of bounds"` for indexes outside of
the range `0` to `(Array.length a - 1)`.

Examples

``` ocaml
[|1; 2; 3; 2; 1|].(3) = 2
```

``` ocaml
let animals = [|"cat"; "dog"; "eel"|] in
animals.(2) = "eel"
```

### `val getAt`

``` ocaml
val getAt : 'a t -> index:int -> 'a option
```

Returns, as an `Option`, the element at index number `n` of array `a`.

Returns `None` if `n` is outside the range `0` to
`(Array.length a - 1)`.

Examples

``` ocaml
Array.getAt [|0; 1; 2|] ~index:5 = None
```

``` ocaml
Array.getAt [||] ~index:0 = None
```

### `val get_at`

``` ocaml
val get_at : 'a t -> index:int -> 'a option
```

### `val (.?())`

``` ocaml
val (.?()) : 'element array -> int -> 'element option
```

The [index
operator](https://caml.inria.fr/pub/docs/manual-ocaml/indexops.html)
version of [`getAt`](#val-getAt)

**Note** Currently this is only supported by the OCaml syntax.

Examples

``` ocaml
Array.([||].?(3)) = Some 'g'
```

``` ocaml
Array.([||].?(9)) = None
```

### `val set`

``` ocaml
val set : 'a t -> int -> 'a -> unit
```

Modifies an array in place, replacing the element at `index` with
`value`.

You should prefer either to write

``` ocaml
array.(index) <- value
```

Or use the [`setAt`](#val-setAt) function instead.

Exceptions

Raises `Invalid_argument "index out of bounds"` if `n` is outside the
range `0` to `Array.length a - 1`.

Examples

``` ocaml
let numbers = [|1;2;3|] in
Array.set numbers 1 1;
numbers.(2) <- 0;

numbers = [|1;0;0|]
```

### `val setAt`

``` ocaml
val setAt : 'a t -> index:int -> value:'a -> unit
```

Like [`set`](#val-set) but with labelled arguments

### `val set_at`

``` ocaml
val set_at : 'a t -> index:int -> value:'a -> unit
```

### `val first`

``` ocaml
val first : 'a t -> 'a option
```

Get the first element of an array.

Returns `None` if the array is empty.

Examples

``` ocaml
Array.first [1;2;3] = Some 1
```

``` ocaml
Array.first [1] = Some 1
```

``` ocaml
Array.first [] = None
```

### `val last`

``` ocaml
val last : 'a t -> 'a option
```

Get the last element of an array.

Returns `None` if the array is empty.

Examples

``` ocaml
Array.last [1;2;3] = Some 3
```

``` ocaml
Array.last [1] = Some 1
```

``` ocaml
Array.last [] = None
```

### `val slice`

``` ocaml
val slice : ?to_:int -> 'a t -> from:int -> 'a t
```

Get a sub-section of a list. `from` is a zero-based index where we will
start our slice.

The `to_` is a zero-based index that indicates the end of the slice.

The slice extracts up to but not including `to_`.

Both the `from` and `to_` indexes can be negative, indicating an offset
from the end of the list.

Examples

``` ocaml
Array.slice ~from:0 ~to_:3 [0; 1; 2; 3; 4] = [0; 1; 2]
```

``` ocaml
Array.slice ~from:1 ~to_:4 [0; 1; 2; 3; 4] = [1; 2; 3]
```

``` ocaml
Array.slice ~from:5 ~to_:3 [0; 1; 2; 3; 4] = []
```

``` ocaml
Array.slice ~from:1 ~to_:(-1) [0; 1; 2; 3; 4] = [1; 2; 3]
```

``` ocaml
Array.slice ~from:(-2) ~to_:5 [0; 1; 2; 3; 4] = [3; 4]
```

``` ocaml
Array.slice ~from:(-2) ~to_:(-1) [0; 1; 2; 3; 4] = [3]
```

### `val swap`

``` ocaml
val swap : 'a t -> int -> int -> unit
```

Swaps the values at the provided indicies.

Exceptions

Raises an `Invalid_argument` exception of either index is out of bounds
for the array.

Examples

``` ocaml
Array.swap [|1; 2; 3|] 1 2 = [|1; 3; 2|]
```

### `val reverse`

``` ocaml
val reverse : 'a t -> unit
```

Reverses an array **in place**, mutating the existing array.

Examples

``` ocaml
let numbers = [|1; 2; 3|] in
Array.reverse numbers
numbers = [|3; 2; 1|];
```

### `val sort`

``` ocaml
val sort : 'a t -> compare:( 'a -> 'a -> int ) -> unit
```

Sort in place, modifying the existing array, using the provided
`compare` function to determine order.

On native it uses [merge sort](https://en.wikipedia.org/wiki/Merge_sort)
which means the sort is stable, runs in constant heap space, logarithmic
stack space and `n * log (n)` time.

When targeting javascript the time and space complexity of the sort
cannot be guaranteed as it depends on the implementation.

Examples

``` ocaml
Array.sortInPlace [|5;6;8;3;6|] ~compare:compare = [|3;5;6;6;8|]
```

## [](#query)Query

### `val isEmpty`

``` ocaml
val isEmpty : 'a t -> bool
```

Check if an array is empty

Examples

``` ocaml
Array.isEmpty [|1; 2, 3|] = false
```

``` ocaml
Array.isEmpty [||] = true
```

### `val is_empty`

``` ocaml
val is_empty : 'a t -> bool
```

### `val length`

``` ocaml
val length : 'a t -> int
```

Return the length of an array.

Examples

``` ocaml
Array.length [|1; 2, 3|] = 3
```

``` ocaml
Array.length [||] = 0
```

### `val any`

``` ocaml
val any : 'a t -> f:( 'a -> bool ) -> bool
```

Determine if `f` returns true for `any` values in an array.

Iteration is stopped as soon as `f` returns `true`

Examples

``` ocaml
Array.any ~f:Int.isEven [|1;2;3;5|] = true
```

``` ocaml
Array.any ~f:Int.isEven [|1;3;5;7|] = false
```

``` ocaml
Array.any ~f:Int.isEven [||] = false
```

### `val all`

``` ocaml
val all : 'a t -> f:( 'a -> bool ) -> bool
```

Determine if `f` returns true for `all` values in an array.

Iteration is stopped as soon as `f` returns `false`

Examples

``` ocaml
Array.all ~f:Int.isEven [|2;4|] = true
```

``` ocaml
Array.all ~f:Int.isEven [|2;3|] = false
```

``` ocaml
Array.all ~f:Int.isEven [||] = true
```

### `val count`

``` ocaml
val count : 'a t -> f:( 'a -> bool ) -> int
```

Count the number of elements which `f` returns `true` for

Examples

``` ocaml
Array.count [|7; 5; 8; 6|] ~f:Int.isEven = 2
```

### `val find`

``` ocaml
val find : 'a t -> f:( 'a -> bool ) -> 'a option
```

Returns, as an `Option`, the first element for which `f` evaluates to
`true`.

If `f` doesn't return `true` for any of the elements `find` will return
`None`

Examples

``` ocaml
Array.find ~f:Int.isEven [|1; 3; 4; 8;|] = Some 4
```

``` ocaml
Array.find ~f:Int.isOdd [|0; 2; 4; 8;|] = None
```

``` ocaml
Array.find ~f:Int.isEven [||] = None
```

### `val findIndex`

``` ocaml
val findIndex : 'a t -> f:( int -> 'a -> bool ) -> (int * 'a) option
```

Similar to `Array`.find but `f` is also called with the current index,
and the return value will be a tuple of the index the passing value was
found at and the passing value.

Examples

``` ocaml
Array.findIndex [|1; 3; 4; 8;|] ~f:(fun index number -> index > 2 && Int.isEven number) = Some (3, 8)
```

### `val find_index`

``` ocaml
val find_index : 'a t -> f:( int -> 'a -> bool ) -> (int * 'a) option
```

### `val includes`

``` ocaml
val includes : 'a t -> 'a -> equal:( 'a -> 'a -> bool ) -> bool
```

Test if an array contains the specified element using the provided
`equal` to test for equality.

Examples

``` ocaml
Array.contains [1; 2; 3]  2 ~equal:(=) = true
```

### `val minimum`

``` ocaml
val minimum : 'a t -> compare:( 'a -> 'a -> int ) -> 'a option
```

Find the smallest element using the provided `compare` function.

Returns `None` if called on an empty array.

Examples

``` ocaml
Array.minimum [|7;5;8;6|] ~compare:Int.compare = Some 5
```

``` ocaml
Array.minimum [||] ~compare:Int.compare = None
```

### `val maximum`

``` ocaml
val maximum : 'a t -> compare:( 'a -> 'a -> int ) -> 'a option
```

Find the largest element using the provided `compare` function.

Returns `None` if called on an empty array.

Examples

``` ocaml
Array.maximum [|7;5;8;6|] ~compare:Int.compare = Some 8
```

``` ocaml
Array.maximum [||] ~compare:Int.compare = None
```

### `val extent`

``` ocaml
val extent : 'a t -> compare:( 'a -> 'a -> int ) -> ('a * 'a) option
```

Find a `Tuple` of the [`minimum`](#val-minimum) and
[`maximum`](#val-maximum) in a single pass

Returns `None` if called on an empty array.

Examples

``` ocaml
Array.extent [|7;5;8;6|] ~compare:Int.compare = Some (5, 8)
```

``` ocaml
Array.extent [|7|] ~compare:Int.compare = Some (7, 7)
```

``` ocaml
Array.extent [||] ~compare:Int.compare = None
```

### `val sum`

``` ocaml
val sum : 'a t -> (module TableclothContainer.Sum with type t = 'a) -> 'a
```

Calculate the sum of a list using the provided modules `zero` value and
`add` function.

Examples

``` ocaml
Array.sum [|1; 2; 3|] (module Int) = 6
```

``` ocaml
Array.sum [|4.0; 4.5; 5.0|] (module Float) = 13.5
```

``` ocaml
Array.sum
  [|"a"; "b"; "c"|]
  (
    module struct
      type t = string
      let zero = ""
      let add = (^)
    end
  )
  = "abc"
```

## [](#transform)Transform

### `val map`

``` ocaml
val map : 'a t -> f:( 'a -> 'b ) -> 'b t
```

Create a new array which is the result of applying a function `f` to
every element.

Examples

``` ocaml
Array.map ~f:Float.squareRoot [|1.0; 4.0; 9.0|] = [|1.0; 2.0; 3.0|]
```

### `val mapWithIndex`

``` ocaml
val mapWithIndex : 'a t -> f:( int -> 'a -> 'b ) -> 'b t
```

Apply a function `f` to every element with its index as the first
argument.

Examples

``` ocaml
Array.mapWithIndex ~f:( * ) [|5; 5; 5|] = [|0; 5; 10|]
```

### `val map_with_index`

``` ocaml
val map_with_index : 'a t -> f:( int -> 'a -> 'b ) -> 'b t
```

### `val filter`

``` ocaml
val filter : 'a t -> f:( 'a -> bool ) -> 'a t
```

Keep elements that `f` returns `true` for.

Examples

``` ocaml
Array.filter ~f:Int.isEven [|1; 2; 3; 4; 5; 6|] = [|2; 4; 6|]
```

### `val filterMap`

``` ocaml
val filterMap : 'a t -> f:( 'a -> 'b option ) -> 'b t
```

Allows you to combine [`map`](#val-map) and [`filter`](#val-filter) into
a single pass.

The output array only contains elements for which `f` returns `Some`.

Why `filterMap` and not just [`filter`](#val-filter) then
[`map`](#val-map)?

[`filterMap`](#val-filterMap) removes the `Option` layer automatically.

If your mapping is already returning an `Option` and you want to skip
over `None`s, then `filterMap` is much nicer to use.

Examples

``` ocaml
let characters = [|'a'; '9'; '6'; ' '; '2'; 'z' |] in
Array.filterMap characters ~f:Char.toDigit = [|9; 6; 2|]
```

``` ocaml
Array.filterMap [|3; 4; 5; 6|] ~f:(fun number ->
  if Int.isEven number then
    Some (number * number)
  else
    None
) = [16; 36]
```

### `val filter_map`

``` ocaml
val filter_map : 'a t -> f:( 'a -> 'b option ) -> 'b t
```

### `val flatMap`

``` ocaml
val flatMap : 'a t -> f:( 'a -> 'b t ) -> 'b t
```

[`map`](#val-map) `f` onto an array and [`flatten`](#val-flatten) the
resulting arrays

Examples

``` ocaml
Array.flatMap ~f:(fun n -> [|n; n|]) [|1; 2; 3|] = [|1; 1; 2; 2; 3; 3|]
```

### `val flat_map`

``` ocaml
val flat_map : 'a t -> f:( 'a -> 'b t ) -> 'b t
```

### `val fold`

``` ocaml
val fold : 'a t -> initial:'b -> f:( 'b -> 'a -> 'b ) -> 'b
```

Produce a new value from an array.

`fold` takes two arguments, an `initial` 'accumulator' value and a
function `f`.

For each element of the array `f` will be called with two arguments; the
current accumulator and an element.

`f` returns the value that the accumulator should have for the next
iteration.

The `initial` value is the value the accumulator will have on the first
call to `f`.

After applying `f` to every element of the array, `fold` returns the
accumulator.

`fold` iterates over the elements of the array from first to last.

Folding is useful whenever you have a collection of something, and want
to produce a single value from it.

For examples if we have:

``` ocaml
let numbers = [|1, 2, 3|] in
let sum =
  Array.fold numbers ~initial:0 ~f:(fun accumulator element -> accumulator +element)
in
sum = 6
```

Walking though each iteration step by step:

1.  `accumulator: 0, element: 1, result: 1`
2.  `accumulator: 1, element: 2, result: 3`
3.  `accumulator: 3, element: 3, result: 6`

And so the final result is `6`. (Note that in reality you probably want
to use `Array`.sum)

Examples

``` ocaml
Array.fold [|1; 2; 3|] ~initial:[] ~f:(List.cons) = [3; 2; 1]
```

``` ocaml
Array.fold [|1; 1; 2; 2; 3|] ~initial:Set.Int.empty ~f:Set.add |> Set.toArray = [|1; 2; 3|]
```

``` ocaml
let lastEven integers =
  Array.fold integers ~initial:None ~f:(fun last int ->
    if Int.isEven then
      Some int
    else
      last
  )
in
lastEven [|1;2;3;4;5|] = Some 4
```

### `val foldRight`

``` ocaml
val foldRight : 'a t -> initial:'b -> f:( 'b -> 'a -> 'b ) -> 'b
```

This method is like [`fold`](#val-fold) except that it iterates over the
elements of the array from last to first.

Examples

``` ocaml
Array.foldRight ~f:(+) ~initial:0 (Array.repeat ~length:3 5) = 15
```

``` ocaml
Array.foldRight ~f:List.cons ~initial:[] [|1; 2; 3|] = [1; 2; 3]
```

### `val fold_right`

``` ocaml
val fold_right : 'a t -> initial:'b -> f:( 'b -> 'a -> 'b ) -> 'b
```

## [](#combine)Combine

### `val append`

``` ocaml
val append : 'a t -> 'a t -> 'a t
```

Creates a new array which is the result of appending the second array
onto the end of the first.

Examples

``` ocaml
let fortyTwos = Array.repeat ~length:2 42 in
let eightyOnes = Array.repeat ~length:3 81 in
Array.append fourtyTwos eightyOnes = [|42; 42; 81; 81; 81|];
```

### `val flatten`

``` ocaml
val flatten : 'a t t -> 'a t
```

Flatten an array of arrays into a single array:

Examples

``` ocaml
Array.flatten [|[|1; 2|]; [|3|]; [|4; 5|]|] = [|1; 2; 3; 4; 5|]
```

### `val zip`

``` ocaml
val zip : 'a t -> 'b t -> ('a * 'b) t
```

Combine two arrays by merging each pair of elements into a `Tuple`

If one array is longer, the extra elements are dropped.

The same as `Array.map2 ~f:Tuple.make`

Examples

``` ocaml
Array.zip [|1;2;3;4;5|] [|"Dog"; "Eagle"; "Ferret"|] = [|(1, "Dog"); (2, "Eagle"); (3, "Ferret")|]
```

### `val map2`

``` ocaml
val map2 : 'a t -> 'b t -> f:( 'a -> 'b -> 'c ) -> 'c t
```

Combine two arrays, using `f` to combine each pair of elements.

If one array is longer, the extra elements are dropped.

Examples

``` ocaml
let totals (xs : int array) (ys : int array) : int array =
  Array.map2 ~f:(+) xs ys in

totals [|1;2;3|] [|4;5;6|] = [|5;7;9|]
```

``` ocaml
Array.map2
  ~f:Tuple.create
  [|"alice"; "bob"; "chuck"|]
  [|2; 5; 7; 8|] =
    [|("alice",2); ("bob",5); ("chuck",7)|]
```

### `val map3`

``` ocaml
val map3 : 'a t -> 'b t -> 'c t -> f:( 'a -> 'b -> 'c -> 'd ) -> 'd t
```

Combine three arrays, using `f` to combine each trio of elements.

If one array is longer, the extra elements are dropped.

Examples

``` ocaml
Array.map3
  ~f:Tuple3.create
  [|"alice"; "bob"; "chuck"|]
  [|2; 5; 7; 8;|]
  [|true; false; true; false|] =
    [|("alice", 2, true); ("bob", 5, false); ("chuck", 7, true)|]
```

## [](#deconstruct)Deconstruct

### `val partition`

``` ocaml
val partition : 'a t -> f:( 'a -> bool ) -> 'a t * 'a t
```

Split an array into a `Tuple` of arrays. Values which `f` returns true
for will end up in `Tuple`.first.

Examples

``` ocaml
Array.partition [|1;2;3;4;5;6|] ~f:Int.isOdd = ([|1;3;5|], [|2;4;6|])
```

### `val splitAt`

``` ocaml
val splitAt : 'a t -> index:int -> 'a t * 'a t
```

Divides an array into a `Tuple` of arrays.

Elements which have index upto (but not including) `index` will be in
the first component of the tuple.

Elements with an index greater than or equal to `index` will be in the
second.

Exceptions

Raises an `Invalid_argument` exception if `index` is less than zero

Examples

``` ocaml
Array.splitAt [|1;2;3;4;5|] ~index:2 = ([|1;2|], [|3;4;5|])
```

``` ocaml
Array.splitAt [|1;2;3;4;5|] ~index:10 = ([|1;2;3;4;5|], [||])
```

``` ocaml
Array.splitAt [|1;2;3;4;5|] ~index:0 = ([||], [|1;2;3;4;5|])
```

### `val split_at`

``` ocaml
val split_at : 'a t -> index:int -> 'a t * 'a t
```

### `val splitWhen`

``` ocaml
val splitWhen : 'a t -> f:( 'a -> bool ) -> 'a t * 'a t
```

Divides an array at the first element `f` returns `true` for.

Returns a `Tuple`, the first component contains the elements `f`
returned false for, the second component includes the element that `f`
retutned `true` for an all the remaining elements.

Examples

``` ocaml
Array.splitWhen
  [|5; 7; 8; 6; 4;|]
  ~f:Int.isEven =
  ([|5; 7|], [|8; 6; 4|])
```

``` ocaml
Array.splitWhen
  [|"Ant"; "Bat"; "Cat"|]
  ~f:(fun animal -> String.length animal > 3) =
    ([|"Ant"; "Bat"; "Cat"|], [||])
```

``` ocaml
Array.splitWhen [|2.; Float.pi; 1.111|] ~f:Float.isInteger =
  ([||], [|2.; Float.pi; 1.111|])
```

### `val split_when`

``` ocaml
val split_when : 'a t -> f:( 'a -> bool ) -> 'a t * 'a t
```

### `val unzip`

``` ocaml
val unzip : ('a * 'b) t -> 'a t * 'b t
```

Decompose an array of `Tuple`s into a `Tuple` of arrays.

Examples

``` ocaml
Array.unzip [(0, true); (17, false); (1337, true)] = ([0;17;1337], [true; false; true])
```

## [](#iterate)Iterate

### `val forEach`

``` ocaml
val forEach : 'a t -> f:( 'a -> unit ) -> unit
```

Iterates over the elements of invokes `f` for each element.

Examples

``` ocaml
Array.forEach [|1; 2; 3|] ~f:(fun int -> print (Int.toString int))
```

### `val for_each`

``` ocaml
val for_each : 'a t -> f:( 'a -> unit ) -> unit
```

### `val forEachWithIndex`

``` ocaml
val forEachWithIndex : 'a t -> f:( int -> 'a -> unit ) -> unit
```

Iterates over the elements of invokes `f` for each element.

Examples

``` ocaml
Array.forEachI [|1; 2; 3|] ~f:(fun index int -> printf "%d: %d" index int)
(*
  0: 1
  1: 2
  2: 3
*)
```

### `val for_each_with_index`

``` ocaml
val for_each_with_index : 'a t -> f:( int -> 'a -> unit ) -> unit
```

### `val values`

``` ocaml
val values : 'a option t -> 'a t
```

Return all of the `Some` values from an array of options

Examples

``` ocaml
Array.values [|(Some "Ant"); None; (Some "Cat")|] = [|"Ant"; "Cat"|]
```

``` ocaml
Array.values [|None; None; None|] = [||]
```

### `val intersperse`

``` ocaml
val intersperse : 'a t -> sep:'a -> 'a t
```

Places `sep` between all the elements of the given array.

Examples

``` ocaml
Array.intersperse ~sep:"on" [|"turtles"; "turtles"; "turtles"|] =
[|"turtles"; "on"; "turtles"; "on"; "turtles"|]
```

``` ocaml
Array.intersperse ~sep:0 [||] = [||]
```

### `val chunksOf`

``` ocaml
val chunksOf : 'a t -> size:int -> 'a t t
```

Split an array into equally sized chunks.

If there aren't enough elements to make the last 'chunk', those elements
are ignored.

Examples

``` ocaml
Array.chunksOf ~size:2 [|"#FFBA49"; "#9984D4"; "#20A39E"; "#EF5B5B"; "#23001E"|] =  [|
  [|"#FFBA49"; "#9984D4"|];
  [|"#20A39E"; "#EF5B5B"|];
|]
```

### `val chunks_of`

``` ocaml
val chunks_of : 'a t -> size:int -> 'a t t
```

### `val sliding`

``` ocaml
val sliding : ?step:int -> 'a t -> size:int -> 'a t t
```

Provides a sliding 'window' of sub-arrays over an array.

The first sub-array starts at index `0` of the array and takes the first
`size` elements.

The sub-array then advances the index `step` (which defaults to 1)
positions before taking the next `size` elements.

The sub-arrays are guaranteed to always be of length `size` and
iteration stops once a sub-array would extend beyond the end of the
array.

Examples

``` ocaml
Array.sliding [|1;2;3;4;5|] ~size:1 = [|[|1|]; [|2|]; [|3|]; [|4|]; [|5|]|] 
```

``` ocaml
Array.sliding [|1;2;3;4;5|] ~size:2 = [|[|1;2|]; [|2;3|]; [|3;4|]; [|4;5|]|] 
```

``` ocaml
Array.sliding [|1;2;3;4;5|] ~size:3 = [|[|1;2;3|]; [|2;3;4|]; [|3;4;5|]|] 
```

``` ocaml
Array.sliding [|1;2;3;4;5|] ~size:2 ~step:2 = [|[|1;2|]; [|3;4|]|] 
```

``` ocaml
Array.sliding [|1;2;3;4;5|] ~size:1 ~step:3 = [|[|1|]; [|4|]|] 
```

## [](#convert)Convert

### `val join`

``` ocaml
val join : string t -> sep:string -> string
```

Converts an array of strings into a `String`, placing `sep` between each
string in the result.

Examples

``` ocaml
Array.join [|"Ant"; "Bat"; "Cat"|] ~sep:", " = "Ant, Bat, Cat"
```

### `val groupBy`

``` ocaml
val groupBy :    'value t ->   ( 'key, 'id ) TableclothComparator.s ->   f:( 'value -> 'key ) ->   ( 'key, 'value list, 'id ) TableclothMap.t
```

Collect elements which `f` produces the same key for

Produces a map from `'key` to a `List` of all elements which produce the
same `'key`

Examples

``` ocaml
let animals = ["Ant"; "Bear"; "Cat"; "Dewgong"] in
Array.groupBy animals (module Int) ~f:String.length = Map.Int.fromList [
  (3, ["Cat"; "Ant"]);
  (4, ["Bear"]);
  (7, ["Dewgong"]);
]
```

### `val group_by`

``` ocaml
val group_by :    'value t ->   ( 'key, 'id ) TableclothComparator.s ->   f:( 'value -> 'key ) ->   ( 'key, 'value list, 'id ) TableclothMap.t
```

### `val toList`

``` ocaml
val toList : 'a t -> 'a list
```

Create a `List` of elements from an array.

Examples

``` ocaml
Array.toList [|1;2;3|] = [1;2;3]
```

``` ocaml
Array.toList (Array.fromList [3; 5; 8]) = [3; 5; 8]
```

### `val to_list`

``` ocaml
val to_list : 'a t -> 'a list
```

### `val toIndexedList`

``` ocaml
val toIndexedList : 'a t -> (int * 'a) list
```

Create an indexed `List` from an array. Each element of the array will
be paired with its index as a `Tuple`.

Examples

``` ocaml
Array.toIndexedList [|"cat"; "dog"|] = [(0, "cat"); (1, "dog")]
```

### `val to_indexed_list`

``` ocaml
val to_indexed_list : 'a t -> (int * 'a) list
```

## [](#compare)Compare

### `val equal`

``` ocaml
val equal : ( 'a -> 'a -> bool ) -> 'a t -> 'a t -> bool
```

Test two arrays for equality using the provided function to test pairs
of elements.

### `val compare`

``` ocaml
val compare : ( 'a -> 'a -> int ) -> 'a t -> 'a t -> int
```

Compare two arrays using the provided function to compare pairs of
elements.

A shorter array is 'less' than a longer one.

Examples

``` ocaml
Array.compare Int.compare [|1;2;3|] [|1;2;3;4|] = -1
```

``` ocaml
Array.compare Int.compare [|1;2;3|] [|1;2;3|] = 0
```

``` ocaml
Array.compare Int.compare [|1;2;5|] [|1;2;3|] = 1
```
