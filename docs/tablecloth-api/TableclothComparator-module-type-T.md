# Module type `TableclothComparator.T`

T represents the input for the [`Make`](TableclothComparator-Make)
functor

### `type t`

``` ocaml
type nonrec t
```

### `val compare`

``` ocaml
val compare : t -> t -> int
```
