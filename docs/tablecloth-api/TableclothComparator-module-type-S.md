# Module type `TableclothComparator.S`

The output type of [`Make`](TableclothComparator-Make).

### `type t`

``` ocaml
type t
```

### `type identity`

``` ocaml
type identity
```

### `val comparator`

``` ocaml
val comparator : ( t, identity ) comparator
```
