# Module `TableclothComparator.Make`

Create a new comparator by providing a module which satisifies
[`T`](TableclothComparator-module-type-T).

-   [Examples](#examples)
-   [Parameters](#parameters)
-   [Signature](#signature)

### [](#examples)Examples

``` ocaml
module Book = struct
  module T = struct
    type t = {
      isbn: string;
      title: string;
    }
    let compare bookA bookB =
      String.compare bookA.isbn bookB.isbn
  end

  include T
  include Comparator.Make(T)
end

let books = Set.empty (module Book)
```

The output type of [`Make`](#).

## [](#parameters)Parameters

### `argument 1 M`

``` ocaml
module M : T
```

## [](#signature)Signature

### `type identity`

``` ocaml
type identity
```

### `val comparator`

``` ocaml
val comparator : ( M.t, identity ) comparator
```
