# Module `TableclothTuple2`

-   [Create](#create)
-   [Transform](#transform)
-   [Convert](#convert)
-   [Compare](#compare)

Functions for manipulating pairs of values

### `type t`

``` ocaml
type ('a, 'b) t = 'a * 'b
```

## [](#create)Create

### `val make`

``` ocaml
val make : 'a -> 'b -> 'a * 'b
```

Create a two-tuple with the given values.

The values do not have to be of the same type.

Examples

``` ocaml
Tuple2.make 3 "Clementine" = (3, "Clementine")
```

### `val fromArray`

``` ocaml
val fromArray : 'a array -> ('a * 'a) option
```

Create a tuple from the first two elements of an `Array`.

If the array is longer than two elements, the extra elements are
ignored.

If the array is less than two elements, returns `None`

Examples

``` ocaml
Tuple2.fromArray [|1; 2|] = Some (1, 2)
```

``` ocaml
Tuple2.fromArray [|1|] = None
```

``` ocaml
Tuple2.fromArray [|4; 5; 6|] = Some (4, 5)
```

### `val from_array`

``` ocaml
val from_array : 'a array -> ('a * 'a) option
```

### `val fromList`

``` ocaml
val fromList : 'a list -> ('a * 'a) option
```

Create a tuple from the first two elements of a `List`.

If the list is longer than two elements, the extra elements are ignored.

If the list is less than two elements, returns `None`

Examples

``` ocaml
Tuple2.fromList [1; 2] = Some (1, 2)
```

``` ocaml
Tuple2.fromList [1] = None
```

``` ocaml
Tuple2.fromList [4; 5; 6] = Some (4, 5)
```

### `val from_list`

``` ocaml
val from_list : 'a list -> ('a * 'a) option
```

### `val first`

``` ocaml
val first : ('a * 'b) -> 'a
```

Extract the first value from a tuple.

Examples

``` ocaml
Tuple2.first (3, 4) = 3
```

``` ocaml
Tuple2.first ("john", "doe") = "john"
```

### `val second`

``` ocaml
val second : ('a * 'b) -> 'b
```

Extract the second value from a tuple.

Examples

``` ocaml
Tuple2.second (3, 4) = 4
```

``` ocaml
Tuple2.second ("john", "doe") = "doe"
```

## [](#transform)Transform

### `val mapFirst`

``` ocaml
val mapFirst : ('a * 'b) -> f:( 'a -> 'x ) -> 'x * 'b
```

Transform the [`first`](#val-first) value in a tuple.

Examples

``` ocaml
Tuple2.mapFirst ~f:String.reverse ("stressed", 16) = ("desserts", 16)
```

``` ocaml
Tuple2.mapFirst ~f:String.length ("stressed", 16) = (8, 16)
```

### `val map_first`

``` ocaml
val map_first : ('a * 'b) -> f:( 'a -> 'x ) -> 'x * 'b
```

### `val mapSecond`

``` ocaml
val mapSecond : ('a * 'b) -> f:( 'b -> 'c ) -> 'a * 'c
```

Transform the second value in a tuple.

Examples

``` ocaml
Tuple2.mapSecond ~f:Float.squareRoot ("stressed", 16.) = ("stressed", 4.)
```

``` ocaml
Tuple2.mapSecond ~f:(~-) ("stressed", 16) = ("stressed", -16)
```

### `val map_second`

``` ocaml
val map_second : ('a * 'b) -> f:( 'b -> 'c ) -> 'a * 'c
```

### `val mapEach`

``` ocaml
val mapEach : ('a * 'b) -> f:( 'a -> 'x ) -> g:( 'b -> 'y ) -> 'x * 'y
```

Transform both values of a tuple, using `f` for the first value and `g`
for the second.

Examples

``` ocaml
Tuple2.mapEach ~f:String.reverse ~g:Float.squareRoot ("stressed", 16.) = ("desserts", 4.)
```

``` ocaml
Tuple2.mapEach ~f:String.length ~g:(~-) ("stressed", 16) = (8, -16)
```

### `val map_each`

``` ocaml
val map_each : ('a * 'b) -> f:( 'a -> 'x ) -> g:( 'b -> 'y ) -> 'x * 'y
```

### `val mapAll`

``` ocaml
val mapAll : ('a * 'a) -> f:( 'a -> 'b ) -> 'b * 'b
```

Transform both of the values of a tuple using the same function.

`mapAll` can only be used on tuples which have the same type for each
value.

Examples

``` ocaml
Tuple2.mapAll ~f:(Int.add 1) (3, 4, 5) = (4, 5, 6)
```

``` ocaml
Tuple2.mapAll ~f:String.length ("was", "stressed") = (3, 8)
```

### `val map_all`

``` ocaml
val map_all : ('a * 'a) -> f:( 'a -> 'b ) -> 'b * 'b
```

### `val swap`

``` ocaml
val swap : ('a * 'b) -> 'b * 'a
```

Switches the first and second values of a tuple.

Examples

``` ocaml
Tuple2.swap (3, 4) = (4, 3)
```

``` ocaml
Tuple2.swap ("stressed", 16) = (16, "stressed")
```

## [](#convert)Convert

### `val toArray`

``` ocaml
val toArray : ('a * 'a) -> 'a array
```

Turns a tuple into an `Array` of length two.

This function can only be used on tuples which have the same type for
each value.

Examples

``` ocaml
Tuple2.toArray (3, 4) = [|3; 4|]
```

``` ocaml
Tuple2.toArray ("was", "stressed") = [|"was"; "stressed"|]
```

### `val to_array`

``` ocaml
val to_array : ('a * 'a) -> 'a array
```

### `val toList`

``` ocaml
val toList : ('a * 'a) -> 'a list
```

Turns a tuple into a list of length two. This function can only be used
on tuples which have the same type for each value.

Examples

``` ocaml
Tuple2.toList (3, 4) = [3; 4]
```

``` ocaml
Tuple2.toList ("was", "stressed") = ["was"; "stressed"]
```

### `val to_list`

``` ocaml
val to_list : ('a * 'a) -> 'a list
```

## [](#compare)Compare

### `val equal`

``` ocaml
val equal :    ( 'a -> 'a -> bool ) ->   ( 'b -> 'b -> bool ) ->   ( 'a, 'b ) t ->   ( 'a, 'b ) t ->   bool
```

Test two `Tuple2`s for equality, using the provided functions to test
the first and second components.

Examples

``` ocaml
Tuple2.equal Int.equal String.equal (1, "Fox") (1, "Fox") = true
```

``` ocaml
Tuple2.equal Int.equal String.equal (1, "Fox") (2, "Hen") = false
```

### `val compare`

``` ocaml
val compare :    ( 'a -> 'a -> int ) ->   ( 'b -> 'b -> int ) ->   ( 'a, 'b ) t ->   ( 'a, 'b ) t ->   int
```

Compare two `Tuple2`s, using the provided functions to compare the first
components then, if the first components are equal, the second
components.

Examples

``` ocaml
Tuple2.compare Int.compare String.compare (1, "Fox") (1, "Fox") = 0
```

``` ocaml
Tuple2.compare Int.compare String.compare (1, "Fox") (1, "Eel") = 1
```

``` ocaml
Tuple2.compare Int.compare String.compare (1, "Fox") (2, "Hen") = -1
```
