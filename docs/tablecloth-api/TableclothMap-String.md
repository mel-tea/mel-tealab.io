# Module `TableclothMap.String`

Construct a Map with [`String`](#)s for keys.

### `type t`

``` ocaml
type nonrec 'value t = 'value Of(TableclothString).t
```

### `val empty`

``` ocaml
val empty : 'value t
```

A map with nothing in it.

### `val singleton`

``` ocaml
val singleton : key:string -> value:'value -> 'value t
```

Create a map from a key and value

Examples

``` ocaml
Map.String.singleton ~key:"Ant" ~value:1 |> Map.toList = [("Ant", 1)]
```

### `val fromArray`

``` ocaml
val fromArray : (string * 'value) array -> 'value t
```

Create a map from an `Array` of key-value tuples

### `val from_array`

``` ocaml
val from_array : (string * 'value) array -> 'value t
```

### `val fromList`

``` ocaml
val fromList : (string * 'value) list -> 'value t
```

Create a map from a `List` of key-value tuples

### `val from_list`

``` ocaml
val from_list : (string * 'value) list -> 'value t
```
