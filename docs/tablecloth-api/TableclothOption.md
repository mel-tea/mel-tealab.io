# Module `TableclothOption`

-   [Compare](#compare)
-   [Operators](#operators)

`Option` represents a value which may not be present.

It is a variant containing the `(Some 'a)` and `None` constructors

``` ocaml
type 'a t =
  | Some of 'a
  | None
```

Many other languages use `null` or `nil` to represent something similar.

`Option` values are very common and they are used in a number of ways:

-   Initial values
-   Optional function arguments
-   Optional record fields
-   Return values for functions that are not defined over their entire
    input range (partial functions).
-   Return value for otherwise reporting simple errors, where None is
    returned on error.

Lots of functions in `Standard` return options, one you have one you can
work with the value it might contain by:

-   Pattern matching
-   Using [`map`](#val-map) or [`andThen`](#val-andThen) (or their
    operators in `Infix`)
-   Unwrapping it using [`unwrap`](#val-unwrap), or its operator
    [`(|?)`](#val-(%7C?))
-   Converting a `None` into an exception
    using[`unwrapUnsafe`](#val-unwrapUnsafe)

If the function you are writing can fail in a variety of ways, use a
`Result` instead to better communicate with the caller.

If a function only fails in unexpected, unrecoverable ways, maybe you
want raise exception.

### `type t`

``` ocaml
type 'a t = 'a option
```

### `val some`

``` ocaml
val some : 'a -> 'a option
```

A function version of the `Some` constructor.

In most situations you just want to use the `Some` constructor directly.

However OCaml doesn't support piping to variant constructors.

Note that when using the Reason syntax you **can** use fast pipe (`->`)
with variant constructors, so you don't need this function.

See the [Reason
docs](https://reasonml.github.io/docs/en/pipe-first#pipe-into-variants)
for more.

Examples

``` ocaml
String.reverse("desserts") |> Option.some = Some "desserts" 
```

### `val and_`

``` ocaml
val and_ : 'a t -> 'a t -> 'a t
```

Returns `None` if the first argument is `None`, otherwise return the
second argument.

Unlike the built in `&&` operator, the `and_` function does not
short-circuit.

When you call `and_`, both arguments are evaluated before being passed
to the function.

Examples

``` ocaml
Option.and_ (Some 11) (Some 22) = Some 22
```

``` ocaml
Option.and_ None (Some 22) = None
```

``` ocaml
Option.and_ (Some 11) None = None
```

``` ocaml
Option.and_ None None = None
```

### `val or_`

``` ocaml
val or_ : 'a t -> 'a t -> 'a t
```

Return the first argument if it [`isSome`](#val-isSome), otherwise
return the second.

Unlike the built in `||` operator, the `or_` function does not
short-circuit. When you call `or_`, both arguments are evaluated before
being passed to the function.

Examples

``` ocaml
Option.or_ (Some 11) (Some 22) = Some 11
```

``` ocaml
Option.or_ None (Some 22) = Some 22
```

``` ocaml
Option.or_ (Some 11) None = Some 11
```

``` ocaml
Option.or_ None None = None
```

### `val orElse`

``` ocaml
val orElse : 'a t -> 'a t -> 'a t
```

Return the second argument if it [`isSome`](#val-isSome), otherwise
return the first.

Like [`or_`](#val-or_) but in reverse. Useful when using the `|>`
operator

Examples

``` ocaml
Option.orElse (Some 11) (Some 22) = Some 22
```

``` ocaml
Option.orElse None (Some 22) = Some 22
```

``` ocaml
Option.orElse (Some 11) None = Some 11
```

``` ocaml
Option.orElse None None = None
```

### `val or_else`

``` ocaml
val or_else : 'a t -> 'a t -> 'a t
```

### `val both`

``` ocaml
val both : 'a t -> 'b t -> ('a * 'b) t
```

Transform two options into an option of a `Tuple`.

Returns None if either of the aguments is None.

Examples

``` ocaml
Option.both (Some 3004) (Some "Ant") = Some (3004, "Ant")
```

``` ocaml
Option.both (Some 3004) None = None
```

``` ocaml
Option.both None (Some "Ant") = None
```

``` ocaml
Option.both None None = None
```

### `val flatten`

``` ocaml
val flatten : 'a t t -> 'a t
```

Flatten two optional layers into a single optional layer.

Examples

``` ocaml
Option.flatten (Some (Some 4)) = Some 4
```

``` ocaml
Option.flatten (Some None) = None
```

``` ocaml
Option.flatten (None) = None
```

### `val map`

``` ocaml
val map : 'a t -> f:( 'a -> 'b ) -> 'b t
```

Transform the value inside an option.

Leaves `None` untouched.

See [`(>>|)`](#val-(%3E%3E%7C)) for an operator version of this
function.

Examples

``` ocaml
Option.map ~f:(fun x -> x * x) (Some 9) = Some 81
```

``` ocaml
Option.map ~f:Int.toString (Some 9) = Some "9"
```

``` ocaml
Option.map ~f:(fun x -> x * x) None = None
```

### `val map2`

``` ocaml
val map2 : 'a t -> 'b t -> f:( 'a -> 'b -> 'c ) -> 'c t
```

Combine two `Option`s

If both options are `Some` returns, as `Some` the result of running `f`
on both values.

If either value is `None`, returns `None`

Examples

``` ocaml
Option.map2 (Some 3) (Some 4) ~f:Int.add = Some 7
```

``` ocaml
Option.map2 (Some 3) (Some 4) ~f:Tuple.make = Some (3, 4)
```

``` ocaml
Option.map2 (Some 3) None ~f:Int.add = None
```

``` ocaml
Option.map2 None (Some 4) ~f:Int.add = None
```

### `val andThen`

``` ocaml
val andThen : 'a t -> f:( 'a -> 'b t ) -> 'b t
```

Chain together many computations that may not return a value.

It is helpful to see its definition:

``` ocaml
let andThen t ~f =
  match t with
  | Some x -> f x
  | None -> None
```

This means we only continue with the callback if we have a value.

For example, say you need to parse some user input as a month:

``` ocaml
let toValidMonth (month: int) : (int option) =
  if (1 <= month && month <= 12) then
    Some month
  else
    None
in

let userInput = "5" in

Int.fromString userInput
|> Option.andThen ~f:toValidMonth
```

If `String.toInt` produces `None` (because the `userInput` was not an
integer) this entire chain of operations will short-circuit and result
in `None`. If `toValidMonth` results in `None`, again the chain of
computations will result in `None`.

See [`(>>=)`](#val-(%3E%3E=)) for an operator version of this function.

Examples

``` ocaml
Option.andThen (Some [1, 2, 3]) ~f:List.head = Some 1
```

``` ocaml
Option.andThen (Some []) ~f:List.head = None
```

### `val and_then`

``` ocaml
val and_then : 'a t -> f:( 'a -> 'b t ) -> 'b t
```

### `val unwrap`

``` ocaml
val unwrap : 'a t -> default:'a -> 'a
```

Unwrap an `option('a)` returning `default` if called with `None`.

This comes in handy when paired with functions like `Map`.get or
`List`.head which return an `Option`.

See [`(|?)`](#val-(%7C?)) for an operator version of this function.

**Note** This can be overused! Many cases are better handled using
pattern matching, [`map`](#val-map) or [`andThen`](#val-andThen).

Examples

``` ocaml
Option.unwrap ~default:99 (Some 42) = 42
```

``` ocaml
Option.unwrap ~default:99 None = 99
```

``` ocaml
Option.unwrap ~default:"unknown" (Map.get Map.String.empty "Tom") = "unknown"
```

### `val unwrapUnsafe`

``` ocaml
val unwrapUnsafe : 'a t -> 'a
```

Unwrap an `option('a)` returning the enclosed `'a`.

**Note** in most situations it is better to use pattern matching,
[`unwrap`](#val-unwrap), [`map`](#val-map) or [`andThen`](#val-andThen).
Can you structure your code slightly differently to avoid potentially
raising an exception?

Exceptions

Raises an `Invalid_argument` exception if called with `None`

Examples

``` ocaml
List.head [1;2;3] |> Option.unwrapUnsafe = 1
```

``` ocaml
List.head [] |> Option.unwrapUnsafe
```

### `val unwrap_unsafe`

``` ocaml
val unwrap_unsafe : 'a t -> 'a
```

### `val isSome`

``` ocaml
val isSome : 'a t -> bool
```

Check if an `Option` is a `Some`.

In most situtations you should just use pattern matching instead.

Examples

``` ocaml
Option.isSome (Some 3004) = true
```

``` ocaml
Option.isSome None = false
```

### `val is_some`

``` ocaml
val is_some : 'a t -> bool
```

### `val isNone`

``` ocaml
val isNone : 'a t -> bool
```

Check if an `Option` is a `None`.

In most situtations you should just use pattern matching instead.

Examples

``` ocaml
Option.isNone (Some 3004) = false
```

``` ocaml
Option.isNone None = true
```

### `val is_none`

``` ocaml
val is_none : 'a t -> bool
```

### `val tap`

``` ocaml
val tap : 'a t -> f:( 'a -> unit ) -> unit
```

Run a function against a value, if it is present.

### `val toArray`

``` ocaml
val toArray : 'a t -> 'a array
```

Convert an option to a `Array`.

`None` is represented as an empty list and `Some` is represented as a
list of one element.

Examples

``` ocaml
Option.toArray (Some 3004) = [|3004|]
```

``` ocaml
Option.toArray (None) = [||]
```

### `val to_array`

``` ocaml
val to_array : 'a t -> 'a array
```

### `val toList`

``` ocaml
val toList : 'a t -> 'a list
```

Convert an option to a `List`.

`None` is represented as an empty list and `Some` is represented as a
list of one element.

Examples

``` ocaml
Option.toList (Some 3004) = [3004]
```

``` ocaml
Option.toList (None) = []
```

### `val to_list`

``` ocaml
val to_list : 'a t -> 'a list
```

## [](#compare)Compare

### `val equal`

``` ocaml
val equal : ( 'a -> 'a -> bool ) -> 'a t -> 'a t -> bool
```

Test two optional values for equality using the provided function

Examples

``` ocaml
Option.equal Int.equal (Some 1) (Some 1) = true
```

``` ocaml
Option.equal Int.equal (Some 1) (Some 3) = false
```

``` ocaml
Option.equal Int.equal (Some 1) None = false
```

``` ocaml
Option.equal Int.equal None None = true
```

### `val compare`

``` ocaml
val compare : ( 'a -> 'a -> int ) -> 'a t -> 'a t -> int
```

Compare two optional values using the provided function.

A `None` is "less" than a `Some`

Examples

``` ocaml
Option.compare Int.compare (Some 1) (Some 3) = -1
```

``` ocaml
Option.compare Int.compare (Some 1) None = 1
```

``` ocaml
Option.compare Int.compare None None = 0
```

## [](#operators)Operators

For code that works extensively with `Option`s these operators can make
things significantly more concise at the expense of placing a greater
cognitive burden on future readers.

``` ocaml
let nameToAge = Map.String.fromArray [|
  ("Ant", 1);
  ("Bat", 5);
  ("Cat", 19);
|] in

let catAge = Map.get nameToAge "Cat" |? 8 in
(* 19 *)

Option.(
  Map.get nameToAge "Ant" >>= (fun antAge ->
    Map.get nameToAge "Bat" >>| (fun batAge ->
      Int.absolute(batAge - antAge)
    )
  )
)
(* Some (4) *)
```

### `val (|?)`

``` ocaml
val (|?) : 'a t -> 'a -> 'a
```

The operator version of `get`

Examples

``` ocaml
Some 3004 |? 8 = 3004
```

``` ocaml
None |? 8 = 8
```

### `val (>>|)`

``` ocaml
val (>>|) : 'a t -> ( 'a -> 'b ) -> 'b t
```

The operator version of [`map`](#val-map)

Examples

``` ocaml
Some "desserts" >>| String.reverse = Some "stressed"
```

``` ocaml
None >>| String.reverse = None
```

### `val (>>=)`

``` ocaml
val (>>=) : 'a t -> ( 'a -> 'b t ) -> 'b t
```

The operator version of [`andThen`](#val-andThen)

Examples

``` ocaml
Some [1, 2, 3] >>= List.head = Some 1
```

``` ocaml
Some [] >>= List.head = None
```
