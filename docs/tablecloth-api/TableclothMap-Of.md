# Module `TableclothMap.Of`

This functor lets you describe the type of Maps a little more concisely.

``` ocaml
let stringToInt : int Map.Of(String).t =
  Map.fromList (module String) [("Apple", 2); ("Pear", 0)]
```

Is the same as

``` ocaml
let stringToInt : (string, int, String.identity) Map.t =
  Map.fromList (module String) [("Apple", 2); ("Pear", 0)]
```

-   [Parameters](#parameters)
-   [Signature](#signature)

## [](#parameters)Parameters

### `argument 1 M`

``` ocaml
module M : TableclothComparator.S
```

## [](#signature)Signature

### `type t`

``` ocaml
type nonrec 'value t = ( M.t, 'value, M.identity ) t
```
