# Module `TableclothSet.Of`

This functor lets you describe the type of Sets a little more concisely.

``` ocaml
let names : Set.Of(String).t =
  Set.fromList (module String) ["Andrew"; "Tina"]
```

Is the same as

``` ocaml
let names : (string, String.identity) Set.t =
  Set.fromList (module String) ["Andrew"; "Tina"]
```

-   [Parameters](#parameters)
-   [Signature](#signature)

## [](#parameters)Parameters

### `argument 1 M`

``` ocaml
module M : TableclothComparator.S
```

## [](#signature)Signature

### `type t`

``` ocaml
type nonrec t = ( M.t, M.identity ) t
```
