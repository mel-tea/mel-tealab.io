# Module `TableclothChar`

-   [Create](#create)
-   [Query](#query)
-   [Modify](#modify)
-   [Convert](#convert)
-   [Compare](#compare)

Functions for working with single characters.

Character literals are enclosed in `'a'` pair of single quotes.

``` ocaml
let digit = '7'
```

The functions in this module work on ASCII characters (range 0-255)
only, **not Unicode**.

Since character 128 through 255 have varying values depending on what
standard you are using (ISO 8859-1 or Windows 1252), you are advised to
stick to the 0-127 range.

### `type t`

``` ocaml
type t = char
```

## [](#create)Create

You can also create a `Char` using single quotes:

``` ocaml
let char = 'c'
```

### `val fromCode`

``` ocaml
val fromCode : int -> char option
```

Convert an ASCII [code point](https://en.wikipedia.org/wiki/Code_point)
to a character.

Returns `None` if the codepoint is outside the range of 0 to 255
inclusive.

Examples

``` ocaml
Char.fromCode 65 = Some 'A'
```

``` ocaml
Char.fromCode 66 = Some 'B'
```

``` ocaml
Char.fromCode 3000 = None
```

``` ocaml
Char.fromCode (-1) = None
```

The full range of extended ASCII is from `0` to `255`. For numbers
outside that range, you get `None`.

### `val from_code`

``` ocaml
val from_code : int -> char option
```

### `val fromString`

``` ocaml
val fromString : string -> char option
```

Converts a string to character. Returns None when the string isn't of
length one.

Examples

``` ocaml
Char.fromString "A" = Some 'A'
```

``` ocaml
Char.fromString " " = Some ' '
```

``` ocaml
Char.fromString "" = None
```

``` ocaml
Char.fromString "abc" = None
```

``` ocaml
Char.fromString " a" = None
```

### `val from_string`

``` ocaml
val from_string : string -> char option
```

## [](#query)Query

### `val isLowercase`

``` ocaml
val isLowercase : char -> bool
```

Detect lower case ASCII characters.

Examples

``` ocaml
Char.isLowercase 'a' = true
```

``` ocaml
Char.isLowercase 'b' = true
```

``` ocaml
Char.isLowercase 'z' = true
```

``` ocaml
Char.isLowercase '0' = false
```

``` ocaml
Char.isLowercase 'A' = false
```

``` ocaml
Char.isLowercase '-' = false
```

### `val is_lowercase`

``` ocaml
val is_lowercase : char -> bool
```

### `val isUppercase`

``` ocaml
val isUppercase : char -> bool
```

Detect upper case ASCII characters.

Examples

``` ocaml
Char.isUppercase 'A' = true
```

``` ocaml
Char.isUppercase 'B' = true
```

``` ocaml
Char.isUppercase 'Z' = true
```

``` ocaml
Char.isUppercase 'h' = false
```

``` ocaml
Char.isUppercase '0' = false
```

``` ocaml
Char.isUppercase '-' = false
```

### `val is_uppercase`

``` ocaml
val is_uppercase : char -> bool
```

### `val isLetter`

``` ocaml
val isLetter : char -> bool
```

Detect upper and lower case ASCII alphabetic characters.

Examples

``` ocaml
Char.isLetter 'a' = true
```

``` ocaml
Char.isLetter 'b' = true
```

``` ocaml
Char.isLetter 'E' = true
```

``` ocaml
Char.isLetter 'Y' = true
```

``` ocaml
Char.isLetter '0' = false
```

``` ocaml
Char.isLetter '-' = false
```

### `val is_letter`

``` ocaml
val is_letter : char -> bool
```

### `val isDigit`

``` ocaml
val isDigit : char -> bool
```

Detect when a character is a number

Examples

``` ocaml
Char.isDigit '0' = true
```

``` ocaml
Char.isDigit '1' = true
```

``` ocaml
Char.isDigit '9' = true
```

``` ocaml
Char.isDigit 'a' = false
```

``` ocaml
Char.isDigit 'b' = false
```

### `val is_digit`

``` ocaml
val is_digit : char -> bool
```

### `val isAlphanumeric`

``` ocaml
val isAlphanumeric : char -> bool
```

Detect upper case, lower case and digit ASCII characters.

Examples

``` ocaml
Char.isAlphanumeric 'a' = true
```

``` ocaml
Char.isAlphanumeric 'b' = true
```

``` ocaml
Char.isAlphanumeric 'E' = true
```

``` ocaml
Char.isAlphanumeric 'Y' = true
```

``` ocaml
Char.isAlphanumeric '0' = true
```

``` ocaml
Char.isAlphanumeric '7' = true
```

``` ocaml
Char.isAlphanumeric '-' = false
```

### `val is_alphanumeric`

``` ocaml
val is_alphanumeric : char -> bool
```

### `val isPrintable`

``` ocaml
val isPrintable : char -> bool
```

Detect if a character is a
[printable](https://en.wikipedia.org/wiki/ASCII#Printable_characters)
character

A Printable character has a `Char`.toCode in the range 32 to 127,
inclusive (`' '` to `'~'`).

Examples

``` ocaml
Char.isPrintable 'G' = true
```

``` ocaml
Char.isPrintable '%' = true
```

``` ocaml
Char.isPrintable ' ' = true
```

``` ocaml
Char.isPrintable '\t' = false
```

``` ocaml
Char.isPrintable '\007' = false
```

### `val is_printable`

``` ocaml
val is_printable : char -> bool
```

### `val isWhitespace`

``` ocaml
val isWhitespace : char -> bool
```

Detect one of the following characters:

-   `'\t'` (tab)
-   `'\n'` (newline)
-   `'\011'` (vertical tab)
-   `'\012'` (form feed)
-   `'\r'` (carriage return)
-   `' '` (space)

Examples

``` ocaml
Char.isWhitespace '\t' = true
```

``` ocaml
Char.isWhitespace ' ' = true
```

``` ocaml
Char.isWhitespace '?' = false
```

``` ocaml
Char.isWhitespace 'G' = false
```

### `val is_whitespace`

``` ocaml
val is_whitespace : char -> bool
```

## [](#modify)Modify

### `val toLowercase`

``` ocaml
val toLowercase : char -> char
```

Converts an ASCII character to lower case, preserving non alphabetic
ASCII characters.

Examples

``` ocaml
Char.toLowercase 'A' = 'a'
```

``` ocaml
Char.toLowercase 'B' = 'b'
```

``` ocaml
Char.toLowercase '7' = '7'
```

### `val to_lowercase`

``` ocaml
val to_lowercase : char -> char
```

### `val toUppercase`

``` ocaml
val toUppercase : char -> char
```

Convert an ASCII character to upper case, preserving non alphabetic
ASCII characters.

Examples

``` ocaml
toUppercase 'a' = 'A'
```

``` ocaml
toUppercase 'b' = 'B'
```

``` ocaml
toUppercase '7' = '7'
```

### `val to_uppercase`

``` ocaml
val to_uppercase : char -> char
```

## [](#convert)Convert

### `val toCode`

``` ocaml
val toCode : char -> int
```

Convert to the corresponding ASCII `code point``cp`.

`cp`: https://en.wikipedia.org/wiki/Code_point

Examples

``` ocaml
Char.toCode 'A' = 65
```

``` ocaml
Char.toCode 'B' = 66
```

### `val to_code`

``` ocaml
val to_code : char -> int
```

### `val toString`

``` ocaml
val toString : char -> string
```

Convert a character into a string.

Examples

``` ocaml
Char.toString 'A' = "A"
```

``` ocaml
Char.toString '{' = "{"
```

``` ocaml
Char.toString '7' = "7"
```

### `val to_string`

``` ocaml
val to_string : char -> string
```

### `val toDigit`

``` ocaml
val toDigit : char -> int option
```

Converts a digit character to its corresponding `Int`.

Returns `None` when the character isn't a digit.

Examples

``` ocaml
Char.toDigit "7" = Some 7
```

``` ocaml
Char.toDigit "0" = Some 0
```

``` ocaml
Char.toDigit "A" = None
```

``` ocaml
Char.toDigit "" = None
```

### `val to_digit`

``` ocaml
val to_digit : char -> int option
```

## [](#compare)Compare

### `val equal`

``` ocaml
val equal : t -> t -> bool
```

Test two `Char`s for equality

### `val compare`

``` ocaml
val compare : t -> t -> int
```

Compare two `Char`s

### `type identity`

``` ocaml
type identity
```

The unique identity for `Comparator`

### `val comparator`

``` ocaml
val comparator : ( t, identity ) TableclothComparator.t
```
