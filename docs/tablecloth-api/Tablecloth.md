# Module `Tablecloth`

### `module Bool`

``` ocaml
module Bool = TableclothBool
```

Functions for working with boolean (`true` or `false`) values.

### `module Char`

``` ocaml
module Char = TableclothChar
```

Functions for working with single characters.

### `module String`

``` ocaml
module String = TableclothString
```

Functions for working with `"strings"`

### `module Int`

``` ocaml
module Int = TableclothInt
```

Fixed precision integers

### `module Float`

``` ocaml
module Float = TableclothFloat
```

Functions for working with floating point numbers.

### `module Container`

``` ocaml
module Container = TableclothContainer
```

Interfaces for use with container types like [`Array`](TableclothArray)
or [`List`](TableclothList)

### `module Array`

``` ocaml
module Array = TableclothArray
```

A fixed lenfth collection of values

### `module List`

``` ocaml
module List = TableclothList
```

Arbitrary length, singly linked lists

### `module Option`

``` ocaml
module Option = TableclothOption
```

Functions for working with optional values.

### `module Result`

``` ocaml
module Result = TableclothResult
```

Functions for working with computations which may fail.

### `module Tuple2`

``` ocaml
module Tuple2 = TableclothTuple2
```

Functions for manipulating tuples of length two

### `module Tuple3`

``` ocaml
module Tuple3 = TableclothTuple3
```

Functions for manipulating tuples of length three

### `module Comparator`

``` ocaml
module Comparator = TableclothComparator
```

### `module Set`

``` ocaml
module Set = TableclothSet
```

A collection of unique values

### `module Map`

``` ocaml
module Map = TableclothMap
```

A collection of key-value pairs

### `module Fun`

``` ocaml
module Fun = TableclothFun
```

Functions for working with functions.
