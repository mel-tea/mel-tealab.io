# Module `TableclothTuple3`

-   [Create](#create)
-   [Basic operations](#basic-operations)
-   [Modify](#modify)
-   [Transform](#transform)
-   [Convert](#convert)
-   [Compare](#compare)

Functions for manipulating trios of values

### `type t`

``` ocaml
type ('a, 'b, 'c) t = 'a * 'b * 'c
```

## [](#create)Create

### `val make`

``` ocaml
val make : 'a -> 'b -> 'c -> 'a * 'b * 'c
```

Create a `Tuple3`.

Examples

``` ocaml
Tuple3.create 3 "cat" false = (3, "cat", false)
```

``` ocaml
List.map3 ~f:Tuple3.create [1;2;3] ['a'; 'b'; 'c'] [4.; 5.; 6.] =
  [(1, 'a', 4.), (2, 'b', 5.), (3, 'c', 6.)]
```

### `val fromArray`

``` ocaml
val fromArray : 'a array -> ('a * 'a * 'a) option
```

Create a tuple from the first two elements of an `Array`.

If the array is longer than two elements, the extra elements are
ignored.

If the array is less than two elements, returns `None`

Examples

``` ocaml
Tuple3.fromArray [|1; 2;3 |] = Some (1, 2, 3)
```

``` ocaml
Tuple3.fromArray [|1; 2|] = None
```

``` ocaml
Tuple3.fromArray [|4;5;6;7|] = Some (4, 5, 6)
```

### `val from_array`

``` ocaml
val from_array : 'a array -> ('a * 'a * 'a) option
```

### `val fromList`

``` ocaml
val fromList : 'a list -> ('a * 'a * 'a) option
```

Create a tuple from the first two elements of a `List`.

If the list is longer than two elements, the extra elements are ignored.

If the list is less than two elements, returns `None`

Examples

``` ocaml
Tuple3.fromList [1; 2; 3] = Some (1, 2, 3)
```

``` ocaml
Tuple3.fromList [1; 2] = None
```

``` ocaml
Tuple3.fromList [4; 5; 6; 7] = Some (4, 5, 6)
```

### `val from_list`

``` ocaml
val from_list : 'a list -> ('a * 'a * 'a) option
```

## [](#basic-operations)Basic operations

### `val first`

``` ocaml
val first : ('a * 'b * 'c) -> 'a
```

Extract the first value from a tuple.

Examples

``` ocaml
Tuple3.first (3, 4, 5) = 3
```

``` ocaml
Tuple3.first ("john", "danger", "doe") = "john"
```

### `val second`

``` ocaml
val second : ('a * 'b * 'c) -> 'b
```

Extract the second value from a tuple.

Examples

``` ocaml
Tuple3.second (3, 4, 5) = 4
```

``` ocaml
Tuple3.second ("john", "danger", "doe") = "danger"
```

### `val third`

``` ocaml
val third : ('a * 'b * 'c) -> 'c
```

Extract the third value from a tuple.

Examples

``` ocaml
Tuple3.third (3, 4, 5) = 5
```

``` ocaml
Tuple3.third ("john", "danger", "doe") = "doe"
```

### `val initial`

``` ocaml
val initial : ('a * 'b * 'c) -> 'a * 'b
```

Extract the first and second values of a `Tuple3` as a `Tuple2`.

Examples

``` ocaml
Tuple3.initial (3, "stressed", false) = (3, "stressed")
```

``` ocaml
Tuple3.initial ("john", 16, true) = ("john", 16)
```

### `val tail`

``` ocaml
val tail : ('a * 'b * 'c) -> 'b * 'c
```

Extract the second and third values of a `Tuple3` as a `Tuple2`.

Examples

``` ocaml
Tuple3.tail (3, "stressed", false) = ("stressed", false)
```

``` ocaml
Tuple3.tail ("john", 16, true) = (16, true)
```

## [](#modify)Modify

### `val rotateLeft`

``` ocaml
val rotateLeft : ('a * 'b * 'c) -> 'b * 'c * 'a
```

Move each value in the tuple one position to the left, moving the value
in the first position into the last position.

Examples

``` ocaml
Tuple3.rotateLeft (3, 4, 5) = (4, 5, 3)
```

``` ocaml
Tuple3.rotateLeft ("was", "stressed", "then") = ("stressed", "then", "was")
```

### `val rotate_left`

``` ocaml
val rotate_left : ('a * 'b * 'c) -> 'b * 'c * 'a
```

### `val rotateRight`

``` ocaml
val rotateRight : ('a * 'b * 'c) -> 'c * 'a * 'b
```

Move each value in the tuple one position to the right, moving the value
in the last position into the first position.

Examples

``` ocaml
Tuple3.rotateRight (3, 4, 5) = (5, 3, 4)
```

``` ocaml
Tuple3.rotateRight ("was", "stressed", "then") = ("then", "was", "stressed")
```

### `val rotate_right`

``` ocaml
val rotate_right : ('a * 'b * 'c) -> 'c * 'a * 'b
```

## [](#transform)Transform

### `val mapFirst`

``` ocaml
val mapFirst : ('a * 'b * 'c) -> f:( 'a -> 'x ) -> 'x * 'b * 'c
```

Transform the first value in a tuple.

Examples

``` ocaml
Tuple3.mapFirst ~f:String.reverse ("stressed", 16, false) = ("desserts", 16, false)
```

``` ocaml
Tuple3.mapFirst ~f:String.length ("stressed", 16, false) = (8, 16, false)
```

### `val map_first`

``` ocaml
val map_first : ('a * 'b * 'c) -> f:( 'a -> 'x ) -> 'x * 'b * 'c
```

### `val mapSecond`

``` ocaml
val mapSecond : ('a * 'b * 'c) -> f:( 'b -> 'y ) -> 'a * 'y * 'c
```

Transform the second value in a tuple.

Examples

``` ocaml
Tuple3.mapSecond ~f:Float.squareRoot ("stressed", 16., false) = ("stressed", 4., false)
```

``` ocaml
Tuple3.mapSecond ~f:(~-) ("stressed", 16, false) = ("stressed", -16, false)
```

### `val map_second`

``` ocaml
val map_second : ('a * 'b * 'c) -> f:( 'b -> 'y ) -> 'a * 'y * 'c
```

### `val mapThird`

``` ocaml
val mapThird : ('a * 'b * 'c) -> f:( 'c -> 'z ) -> 'a * 'b * 'z
```

Transform the third value in a tuple.

Examples

``` ocaml
Tuple3.mapThird ~f:not ("stressed", 16, false) ("stressed", 16, true)
```

### `val map_third`

``` ocaml
val map_third : ('a * 'b * 'c) -> f:( 'c -> 'z ) -> 'a * 'b * 'z
```

### `val mapEach`

``` ocaml
val mapEach :    ('a * 'b * 'c) ->   f:( 'a -> 'x ) ->   g:( 'b -> 'y ) ->   h:( 'c -> 'z ) ->   'x * 'y * 'z
```

Transform each value in a tuple by applying `f` to the
[`first`](#val-first) value, `g` to the [`second`](#val-second) value
and `h` to the [`third`](#val-third) value.

Examples

``` ocaml
Tuple3.mapEach
  ~f:String.reverse
  ~g:Float.squareRoot
  ~h:Bool.not
  ("stressed", 16., false) = ("desserts", 4., true)
```

### `val map_each`

``` ocaml
val map_each :    ('a * 'b * 'c) ->   f:( 'a -> 'x ) ->   g:( 'b -> 'y ) ->   h:( 'c -> 'z ) ->   'x * 'y * 'z
```

### `val mapAll`

``` ocaml
val mapAll : ('a * 'a * 'a) -> f:( 'a -> 'b ) -> 'b * 'b * 'b
```

Transform all the values of a tuple using the same function.

`mapAll` can only be used on tuples which have the same type for each
value.

Examples

``` ocaml
Tuple3.mapAll ~f:Float.squareRoot (9., 16., 25.) = (3., 4., 5.)
```

``` ocaml
Tuple3.mapAll ~f:String.length ("was", "stressed", "then") = (3, 8, 4)
```

### `val map_all`

``` ocaml
val map_all : ('a * 'a * 'a) -> f:( 'a -> 'b ) -> 'b * 'b * 'b
```

## [](#convert)Convert

### `val toArray`

``` ocaml
val toArray : ('a * 'a * 'a) -> 'a array
```

Turns a tuple into a `List` of length three.

This function can only be used on tuples which have the same type for
each value.

Examples

``` ocaml
Tuple3.toArray (3, 4, 5) = [3; 4; 5]
```

``` ocaml
Tuple3.toArray ("was", "stressed", "then") = ["was"; "stressed"; "then"]
```

### `val to_array`

``` ocaml
val to_array : ('a * 'a * 'a) -> 'a array
```

### `val toList`

``` ocaml
val toList : ('a * 'a * 'a) -> 'a list
```

Turns a tuple into a `List` of length three.

This function can only be used on tuples which have the same type for
each value.

Examples

``` ocaml
Tuple3.toList (3, 4, 5) = [3; 4; 5]
```

``` ocaml
Tuple3.toList ("was", "stressed", "then") = ["was"; "stressed"; "then"]
```

### `val to_list`

``` ocaml
val to_list : ('a * 'a * 'a) -> 'a list
```

## [](#compare)Compare

### `val equal`

``` ocaml
val equal :    ( 'a -> 'a -> bool ) ->   ( 'b -> 'b -> bool ) ->   ( 'c -> 'c -> bool ) ->   ( 'a, 'b, 'c ) t ->   ( 'a, 'b, 'c ) t ->   bool
```

Test two `Tuple3`s for equality, using the provided functions to test
the first, second and third components.

Examples

``` ocaml
Tuple3.equal Int.equal String.equal Char.equal (1, "Fox", 'j') (1, "Fox", 'k') = true
```

``` ocaml
Tuple3.equal Int.equal String.equal Char.equal (1, "Fox", 'j') (2, "Hen", 'j') = false
```

### `val compare`

``` ocaml
val compare :    ( 'a -> 'a -> int ) ->   ( 'b -> 'b -> int ) ->   ( 'c -> 'c -> int ) ->   ( 'a, 'b, 'c ) t ->   ( 'a, 'b, 'c ) t ->   int
```

Compare two `Tuple3`s, using the provided functions to compare the first
components then, if the first components are equal, the second
components, then the third components

Examples

``` ocaml
Tuple3.compare Int.compare String.compare Char.compare (1, "Fox", 'j') (1, "Fox", 'j') = 0
```

``` ocaml
Tuple3.compare Int.compare String.compare Char.compare (1, "Fox", 'j') (1, "Eel", 'j') = 1
```

``` ocaml
Tuple3.compare Int.compare String.compare Char.compare (1, "Fox", 'j') (2, "Fox", 'm') = -1
```
