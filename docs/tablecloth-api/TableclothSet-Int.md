# Module `TableclothSet.Int`

Construct sets of [`Int`](#)s

### `type t`

``` ocaml
type nonrec t = Of(TableclothInt).t
```

### `val empty`

``` ocaml
val empty : t
```

A set with nothing in it.

### `val singleton`

``` ocaml
val singleton : int -> t
```

Create a set from a single [`Int`](#)

Examples

``` ocaml
Set.Int.singleton 5 |> Set.toList = [5]
```

### `val fromArray`

``` ocaml
val fromArray : int array -> t
```

Create a set from an `Array`

Examples

``` ocaml
Set.Int.fromArray [|1;2;3;3;2;1;7|] |> Set.toArray = [|1;2;3;7|]
```

### `val from_array`

``` ocaml
val from_array : int array -> t
```

### `val fromList`

``` ocaml
val fromList : int list -> t
```

Create a set from a `List`

Examples

``` ocaml
Set.Int.fromList [1;2;3;3;2;1;7] |> Set.toList = [1;2;3;7]
```

### `val from_list`

``` ocaml
val from_list : int list -> t
```
