# Module `TableclothFloat`

-   [Constants](#constants)
-   [Create](#create)
-   [Basic arithmetic and operators](#basic-arithmetic-and-operators)
-   [Fancier math](#fancier-math)
-   [Query](#query)
-   [Angles](#angles)
-   [Polar coordinates](#polar-coordinates)
-   [Rounding](#rounding)
-   [Convert](#convert)
-   [Compare](#compare)

A module for working with [floating-point
numbers](https://en.wikipedia.org/wiki/Floating-point_arithmetic).

Valid syntax for `float`s includes:

``` ocaml
0.
42.
42.0
3.14
-0.1234
123_456.123_456
6.022e23   (* = (6.022 * 10^23) *)
6.022e+23  (* = (6.022 * 10^23) *)
1.602e-19  (* = (1.602 * 10^-19) *)
1e3        (* = (1 * 10 ** 3) = 1000. *)
```

Without opening this module you can use the `.` suffixed operators e.g

``` ocaml
1. +. 2. /. 0.25 *. 2. = 17. 
```

But by opening this module locally you can use the un-suffixed operators

``` ocaml
Float.((10.0 - 1.5 / 0.5) ** 3.0) = 2401.0
```

**Historical Note:** The particular details of floats (e.g. `NaN`) are
specified by [IEEE 754](https://en.wikipedia.org/wiki/IEEE_754) which is
literally hard-coded into almost all CPUs in the world.

### `type t`

``` ocaml
type t = float
```

## [](#constants)Constants

### `val zero`

``` ocaml
val zero : t
```

The literal `0.0` as a named value

### `val one`

``` ocaml
val one : t
```

The literal `1.0` as a named value

### `val nan`

``` ocaml
val nan : t
```

`NaN` as a named value. NaN stands for [not a
number](https://en.wikipedia.org/wiki/NaN).

**Note** comparing values with `Float`.nan will **always return**
`false` even if the value you are comparing against is also `NaN`.

e.g

``` ocaml
let isNotANumber x = Float.(x = nan) in

isNotANumber nan = false
```

For detecting `Nan` you should use `Float`.isNaN

### `val infinity`

``` ocaml
val infinity : t
```

Positive
[infinity](https://en.wikipedia.org/wiki/IEEE_754-1985#Positive_and_negative_infinity)

``` ocaml
Float.log ~base:10.0 0.0 = Float.infinity
```

### `val negativeInfinity`

``` ocaml
val negativeInfinity : t
```

Negative infinity, see `Float`.infinity

### `val negative_infinity`

``` ocaml
val negative_infinity : t
```

### `val e`

``` ocaml
val e : t
```

An approximation of [Euler's
number](https://en.wikipedia.org/wiki/E_(mathematical_constant)).

### `val pi`

``` ocaml
val pi : t
```

An approximation of [pi](https://en.wikipedia.org/wiki/Pi).

### `val epsilon`

``` ocaml
val epsilon : t
```

The smallest interval between two representable numbers.

### `val largestValue`

``` ocaml
val largestValue : t
```

The largest (furthest from zero) representable positive `float`

### `val largest_value`

``` ocaml
val largest_value : t
```

### `val smallestValue`

``` ocaml
val smallestValue : t
```

The smallest representable positive `float`. The closest to zero without
actually being zero.

### `val smallest_value`

``` ocaml
val smallest_value : t
```

### `val maximumSafeInteger`

``` ocaml
val maximumSafeInteger : t
```

For floats greater than `maximumSafeInteger`, it no longer holds that
`Float.(n + 1.) > n`

### `val maximum_safe_integer`

``` ocaml
val maximum_safe_integer : t
```

### `val minimumSafeInteger`

``` ocaml
val minimumSafeInteger : t
```

For floats less than `minimumSafeInteger`, it no longer holds that
`Float.(n - 1.) < n`

### `val minimum_safe_integer`

``` ocaml
val minimum_safe_integer : t
```

## [](#create)Create

### `val fromInt`

``` ocaml
val fromInt : int -> t
```

Convert an `Int` to a `float`

Examples

``` ocaml
Float.fromInt 5 = 5.0
Float.fromInt 0 = 0.0
Float.fromInt -7 = -7.0
```

### `val from_int`

``` ocaml
val from_int : int -> t
```

### `val fromString`

``` ocaml
val fromString : string -> t option
```

Convert a `String` to a `float`. The behaviour of this function is
platform specific. Parses `nan` and `infinity` case-insensitive.

Examples

``` ocaml
Float.fromString "4.667" = Some 4.667
```

``` ocaml
Float.fromString "-4.667" = Some (-4.667)
```

``` ocaml
Float.fromString "Hamster" = None
```

``` ocaml
Float.fromString "NaN" = Some Float.nan
```

``` ocaml
Float.fromString "nan" = Some Float.nan
```

``` ocaml
Float.fromString "Infinity" = Some Float.infinity
```

### `val from_string`

``` ocaml
val from_string : string -> t option
```

## [](#basic-arithmetic-and-operators)Basic arithmetic and operators

### `val add`

``` ocaml
val add : t -> t -> t
```

Addition for floating point numbers.

Although `int`s and `float`s support many of the same basic operations
such as addition and subtraction you **cannot** `add` an `int` and a
`float` directly which means you need to use functions like
`Int`.toFloat to convert both values to the same type.

So if you needed to add a `List`.length to a `float` for some reason,
you could:

``` ocaml
Float.add 3.14 (Int.toFloat (List.length [1,2,3])) = 6.14
```

or

``` ocaml
Float.roundToInt 3.14 + List.length [1,2,3] = 6
```

Languages like Java and JavaScript automatically convert `int` values to
`float` values when you mix and match. This can make it difficult to be
sure exactly what type of number you are dealing with and cause
unexpected behavior.

OCaml has opted for a design that makes all conversions explicit.

Examples

``` ocaml
Float.add 3.14 3.14 = 6.28
Float.(3.14 + 3.14 = 6.28)
```

### `val (+)`

``` ocaml
val (+) : t -> t -> t
```

See `Float`.add

### `val subtract`

``` ocaml
val subtract : t -> t -> t
```

Subtract numbers

Alternatively the `-` operator can be used

Examples

``` ocaml
Float.subtract 4.0 3.0 = 1.0
```

``` ocaml
Float.(4.0 - 3.0) = 1.0
```

### `val ( )`

``` ocaml
val (-) : t -> t -> t
```

See `Float`.subtract

### `val multiply`

``` ocaml
val multiply : t -> t -> t
```

Multiply numbers

Alternatively the `*` operator can be used

Examples

``` ocaml
Float.multiply 2.0 7.0 = 14.0
```

``` ocaml
Float.(2.0 * 7.0) = 14.0
```

### `val (*)`

``` ocaml
val (*) : t -> t -> t
```

See `Float`.multiply

### `val divide`

``` ocaml
val divide : t -> by:t -> t
```

Floating-point division:

Alternatively the `/` operator can be used

Examples

``` ocaml
Float.divide 3.14 ~by:2.0 = 1.57
```

``` ocaml
Float.(3.14 / 2.0) = 1.57
```

### `val (/)`

``` ocaml
val (/) : t -> t -> t
```

See `Float`.divide

### `val power`

``` ocaml
val power : base:t -> exponent:t -> t
```

Exponentiation, takes the base first, then the exponent.

Alternatively the `**` operator can be used

Examples

``` ocaml
Float.power ~base:7.0 ~exponent:3.0 = 343.0
```

``` ocaml
Float.(7.0 ** 3.0) = 343.0
```

### `val (**)`

``` ocaml
val (**) : t -> t -> t
```

See `Float`.power

### `val negate`

``` ocaml
val negate : t -> t
```

Flips the 'sign' of a `float` so that positive floats become negative
and negative integers become positive. Zero stays as it is.

Alternatively an operator is available

Examples

``` ocaml
Float.(~- 4.0) = (-4.0)
```

``` ocaml
Float.negate 8 = (-8)
Float.negate (-7) = 7
Float.negate 0 = 0
```

### `val (~ )`

``` ocaml
val (~-) : t -> t
```

See `Float`.negate

### `val absolute`

``` ocaml
val absolute : t -> t
```

Get the [absolute value](https://en.wikipedia.org/wiki/Absolute_value)
of a number.

Examples

``` ocaml
Float.absolute 8. = 8.
Float.absolute (-7) = 7
Float.absolute 0 = 0
```

### `val maximum`

``` ocaml
val maximum : t -> t -> t
```

Returns the larger of two `float`s, if both arguments are equal, returns
the first argument

If either (or both) of the arguments are `NaN`, returns `NaN`

Examples

``` ocaml
Float.maximum 7. 9. = 9.
```

``` ocaml
Float.maximum (-4.) (-1.) = (-1.)
```

``` ocaml
Float.(isNaN (maximum 7. nan)) = true
```

### `val minimum`

``` ocaml
val minimum : t -> t -> t
```

Returns the smaller of two `float`s, if both arguments are equal,
returns the first argument

If either (or both) of the arguments are `NaN`, returns `NaN`

Examples

``` ocaml
Float.minimum 7.0 9.0 = 7.0
```

``` ocaml
Float.minimum (-4.0) (-1.0) = (-4.0)
```

``` ocaml
Float.(isNaN (minimum 7. nan)) = true
```

### `val clamp`

``` ocaml
val clamp : t -> lower:t -> upper:t -> t
```

Clamps `n` within the inclusive `lower` and `upper` bounds.

Exceptions

Throws an `Invalid_argument` exception if `lower > upper`

Examples

``` ocaml
Float.clamp ~lower:0. ~upper:8. 5. = 5.
```

``` ocaml
Float.clamp ~lower:0. ~upper:8. 9. = 8.
```

``` ocaml
Float.clamp ~lower:(-10.) ~upper:(-5.) 5. = -5.
```

## [](#fancier-math)Fancier math

### `val squareRoot`

``` ocaml
val squareRoot : t -> t
```

Take the square root of a number.

`squareRoot` returns `NaN` when its argument is negative. See
`Float`.nan for more.

Examples

``` ocaml
Float.squareRoot 4.0 = 2.0
```

``` ocaml
Float.squareRoot 9.0 = 3.0
```

### `val square_root`

``` ocaml
val square_root : t -> t
```

### `val log`

``` ocaml
val log : t -> base:t -> t
```

Calculate the logarithm of a number with a given base.

Examples

``` ocaml
Float.log ~base:10. 100. = 2.
```

``` ocaml
Float.log ~base:2. 256. = 8.
```

## [](#query)Query

### `val isNaN`

``` ocaml
val isNaN : t -> bool
```

Determine whether a float is an undefined or unrepresentable number.

**Note** this function is more useful than it might seem since `NaN`
**does not** equal `Nan`:

``` ocaml
Float.(nan = nan) = false
```

Examples

``` ocaml
Float.isNaN (0.0 / 0.0) = true
```

``` ocaml
Float.(isNaN (squareRoot (-1.0))) = true
```

``` ocaml
Float.isNaN (1.0 / 0.0) = false  (* Float.infinity {b is} a number *)
```

``` ocaml
Float.isNaN 1. = false
```

### `val is_nan`

``` ocaml
val is_nan : t -> bool
```

### `val isFinite`

``` ocaml
val isFinite : t -> bool
```

Determine whether a float is finite number. True for any float except
`Infinity`, `-Infinity` or `NaN`

Notice that `NaN` is not finite!

Examples

``` ocaml
Float.isFinite (0. / 0.) = false
```

``` ocaml
Float.(isFinite (squareRoot (-1.))) = false
```

``` ocaml
Float.isFinite (1. / 0.) = false
```

``` ocaml
Float.isFinite 1. = true
```

``` ocaml
Float.(isFinite nan) = false
```

### `val is_finite`

``` ocaml
val is_finite : t -> bool
```

### `val isInfinite`

``` ocaml
val isInfinite : t -> bool
```

Determine whether a float is positive or negative infinity.

Examples

``` ocaml
Float.isInfinite (0. / 0.) = false
```

``` ocaml
Float.(isInfinite (squareRoot (-1.))) = false
```

``` ocaml
Float.isInfinite (1. / 0.) = true
```

``` ocaml
Float.isInfinite 1. = false
```

``` ocaml
Float.(isInfinite nan) = false
```

### `val is_infinite`

``` ocaml
val is_infinite : t -> bool
```

### `val isInteger`

``` ocaml
val isInteger : t -> bool
```

Determine whether the passed value is an integer.

Examples

``` ocaml
Float.isInteger 4.0 = true
```

``` ocaml
Float.isInteger Float.pi = false
```

### `val is_integer`

``` ocaml
val is_integer : t -> bool
```

### `val isSafeInteger`

``` ocaml
val isSafeInteger : t -> bool
```

Determine whether the passed value is a safe integer (number between
-(2\*\*53 - 1) and 2\*\*53 - 1).

Examples

``` ocaml
Float.isSafeInteger 4.0 = true
```

``` ocaml
Float.isSafeInteger Float.pi = false
```

``` ocaml
Float.(isSafeInteger (maximumSafeInteger + 1.)) = false
```

### `val is_safe_integer`

``` ocaml
val is_safe_integer : t -> bool
```

### `val inRange`

``` ocaml
val inRange : t -> lower:t -> upper:t -> bool
```

Checks if a float is between `lower` and up to, but not including,
`upper`.

If `lower` is not specified, it's set to to `0.0`.

Exceptions

Throws an `Invalid_argument` exception if `lower > upper`

Examples

``` ocaml
Float.inRange ~lower:2. ~upper:4. 3. = true
```

``` ocaml
Float.inRange ~lower:1. ~upper:2. 2. = false
```

``` ocaml
Float.inRange ~lower:5.2 ~upper:7.9 9.6 = false
```

### `val in_range`

``` ocaml
val in_range : t -> lower:t -> upper:t -> bool
```

## [](#angles)Angles

### `type radians`

``` ocaml
type radians = float
```

This type is just an alias for `float`.

Its purpose is to make understanding the signatures of the following
functions a little easier.

### `val hypotenuse`

``` ocaml
val hypotenuse : t -> t -> t
```

`hypotenuse x y` returns the length of the hypotenuse of a right-angled
triangle with sides of length `x` and `y`, or, equivalently, the
distance of the point `(x, y)` to `(0, 0)`.

Examples

``` ocaml
Float.hypotenuse 3. 4. = 5.
```

### `val degrees`

``` ocaml
val degrees : t -> radians
```

Converts an angle in
[degrees](https://en.wikipedia.org/wiki/Degree_(angle)) to
`Float`.radians.

Examples

``` ocaml
Float.degrees 180. = Float.pi
```

``` ocaml
Float.degrees 360. = Float.pi * 2.
```

``` ocaml
Float.degrees 90. = Float.pi /. 2.
```

### `val radians`

``` ocaml
val radians : t -> radians
```

Convert a `Float`.t to [radians](https://en.wikipedia.org/wiki/Radian).

**Note** This function doesn't actually do anything to its argument, but
can be useful to indicate intent when inter-mixing angles of different
units within the same function.

Examples

``` ocaml
Float.(radians pi) = 3.141592653589793
```

### `val turns`

``` ocaml
val turns : t -> radians
```

Convert an angle in
[turns](https://en.wikipedia.org/wiki/Turn_(geometry)) into
`Float`.radians.

One turn is equal to 360 degrees.

Examples

``` ocaml
Float.(turns (1. / 2.)) = pi
```

``` ocaml
Float.(turns 1. = degrees 360.)
```

## [](#polar-coordinates)Polar coordinates

### `val fromPolar`

``` ocaml
val fromPolar : (float * radians) -> float * float
```

Convert [polar
coordinates](https://en.wikipedia.org/wiki/Polar_coordinate_system)
(radius, radians) to [Cartesian
coordinates](https://en.wikipedia.org/wiki/Cartesian_coordinate_system)
(x,y).

Examples

``` ocaml
Float.(fromPolar (squareRoot 2., degrees 45.)) = (1., 1.)
```

### `val from_polar`

``` ocaml
val from_polar : (float * radians) -> float * float
```

### `val toPolar`

``` ocaml
val toPolar : (float * float) -> float * radians
```

Convert [Cartesian
coordinates](https://en.wikipedia.org/wiki/Cartesian_coordinate_system)
`(x, y)` to [polar
coordinates](https://en.wikipedia.org/wiki/Polar_coordinate_system)
`(radius, radians)`.

Examples

``` ocaml
Float.toPolar (-1.0, 0.0) = (1.0, Float.pi)
```

``` ocaml
Float.toPolar (3.0, 4.0) = (5.0, 0.9272952180016122)
```

``` ocaml
Float.toPolar (5.0, 12.0) = (13.0, 1.1760052070951352)
```

### `val to_polar`

``` ocaml
val to_polar : (float * float) -> float * radians
```

### `val cos`

``` ocaml
val cos : radians -> t
```

Figure out the cosine given an angle in
[radians](https://en.wikipedia.org/wiki/Radian).

Examples

``` ocaml
Float.(cos (degrees 60.)) = 0.5000000000000001
```

``` ocaml
Float.(cos (radians (pi / 3.))) = 0.5000000000000001
```

### `val acos`

``` ocaml
val acos : radians -> t
```

Figure out the arccosine for `adjacent / hypotenuse` in
[radians](https://en.wikipedia.org/wiki/Radian):

Examples

``` ocaml
Float.(acos (radians 1.0 / 2.0)) = Float.radians 1.0471975511965979 (* 60 degrees or pi/3 radians *)
```

### `val sin`

``` ocaml
val sin : radians -> t
```

Figure out the sine given an angle in
[radians](https://en.wikipedia.org/wiki/Radian).

Examples

``` ocaml
Float.(sin (degrees 30.)) = 0.49999999999999994
```

``` ocaml
Float.(sin (radians (pi / 6.))) = 0.49999999999999994
```

### `val asin`

``` ocaml
val asin : radians -> t
```

Figure out the arcsine for `opposite / hypotenuse` in
[radians](https://en.wikipedia.org/wiki/Radian):

Examples

``` ocaml
Float.(asin (1.0 / 2.0)) = 0.5235987755982989 (* 30 degrees or pi / 6 radians *)
```

### `val tan`

``` ocaml
val tan : radians -> t
```

Figure out the tangent given an angle in radians.

Examples

``` ocaml
Float.(tan (degrees 45.)) = 0.9999999999999999
```

``` ocaml
Float.(tan (radians (pi / 4.))) = 0.9999999999999999
```

``` ocaml
Float.(tan (pi / 4.)) = 0.9999999999999999
```

### `val atan`

``` ocaml
val atan : t -> radians
```

This helps you find the angle (in radians) to an `(x, y)` coordinate,
but in a way that is rarely useful in programming.

**You probably want** [`atan2`](#val-atan2) instead!

This version takes `y / x` as its argument, so there is no way to know
whether the negative signs comes from the `y` or `x` value. So as we go
counter-clockwise around the origin from point `(1, 1)` to `(1, -1)` to
`(-1,-1)` to `(-1,1)` we do not get angles that go in the full circle:

Notice that everything is between `pi / 2` and `-pi/2`. That is pretty
useless for figuring out angles in any sort of visualization, so again,
check out `Float`.atan2 instead!

Examples

``` ocaml
Float.atan (1. /. 1.) = 0.7853981633974483  (* 45 degrees or pi/4 radians *)
```

``` ocaml
Float.atan (1. /. -1.) = -0.7853981633974483  (* 315 degrees or 7 * pi / 4 radians *)
```

``` ocaml
Float.atan (-1. /. -1.) = 0.7853981633974483 (* 45 degrees or pi/4 radians *)
```

``` ocaml
Float.atan (-1. /.  1.) = -0.7853981633974483 (* 315 degrees or 7 * pi/4 radians *)
```

### `val atan2`

``` ocaml
val atan2 : y:t -> x:t -> radians
```

This helps you find the angle (in radians) to an `(x, y)` coordinate.

So rather than `Float.(atan (y / x))` you can `Float.atan2 ~y ~x` and
you can get a full range of angles:

Examples

``` ocaml
Float.atan2 ~y:1. ~x:1. = 0.7853981633974483  (* 45 degrees or pi/4 radians *)
```

``` ocaml
Float.atan2 ~y:1. ~x:(-1.) = 2.3561944901923449  (* 135 degrees or 3 * pi/4 radians *)
```

``` ocaml
Float.atan2 ~y:(-1.) ~x:(-1.) = -(2.3561944901923449) (* 225 degrees or 5 * pi/4 radians *)
```

``` ocaml
Float.atan2 ~y:(-1.) ~x:1. = -(0.7853981633974483) (* 315 degrees or 7 * pi/4 radians *)
```

## [](#rounding)Rounding

### `type direction`

``` ocaml
type direction = [ 
```

The possible `direction`s availible when doing `Float`.round.

See `Float`.round for what each variant represents.

### `val round`

``` ocaml
val round : ?direction:direction -> t -> t
```

Round a number, by default to the to the closest `int` with halves
rounded `` `Up `` (towards positive infinity)

Other rounding strategies are available by using the optional
`~direction` labelelled.

Examples

``` ocaml
Float.round 1.2 = 1.0
Float.round 1.5 = 2.0
Float.round 1.8 = 2.0
Float.round -1.2 = -1.0
Float.round -1.5 = -1.0
Float.round -1.8 = -2.0
```

Towards zero

``` ocaml
Float.round ~direction:`Zero 1.2 = 1.0
Float.round ~direction:`Zero 1.5 = 1.0
Float.round ~direction:`Zero 1.8 = 1.0
Float.round ~direction:`Zero (-1.2) = -1.0
Float.round ~direction:`Zero (-1.5) = -1.0
Float.round ~direction:`Zero (-1.8) = -1.0
```

Away from zero

``` ocaml
Float.round ~direction:`AwayFromZero 1.2 = 1.0
Float.round ~direction:`AwayFromZero 1.5 = 1.0
Float.round ~direction:`AwayFromZero 1.8 = 1.0
Float.round ~direction:`AwayFromZero (-1.2) = -1.0
Float.round ~direction:`AwayFromZero (-1.5) = -1.0
Float.round ~direction:`AwayFromZero (-1.8) = -1.0
```

Towards infinity

This is also known as `Float`.ceiling

``` ocaml
Float.round ~direction:`Up 1.2 = 1.0
Float.round ~direction:`Up 1.5 = 1.0
Float.round ~direction:`Up 1.8 = 1.0
Float.round ~direction:`Up (-1.2) = -1.0
Float.round ~direction:`Up (-1.5) = -1.0
Float.round ~direction:`Up (-1.8) = -1.0
```

Towards negative infinity

This is also known as `Float`.floor

``` ocaml
List.map  ~f:(Float.round ~direction:`Down) [-1.8; -1.5; -1.2; 1.2; 1.5; 1.8] = [-2.0; -2.0; -2.0; 1.0 1.0 1.0]
```

To the closest integer

Rounding a number `x` to the closest integer requires some tie-breaking
for when the `fraction` part of `x` is exactly `0.5`.

Halves rounded towards zero

``` ocaml
List.map  ~f:(Float.round ~direction:(`Closest `AwayFromZero)) [-1.8; -1.5; -1.2; 1.2; 1.5; 1.8] = [-2.0; -1.0; -1.0; 1.0 1.0 2.0]
```

Halves rounded away from zero

This method is often known as **commercial rounding**

``` ocaml
List.map  ~f:(Float.round ~direction:(`Closest `AwayFromZero)) [-1.8; -1.5; -1.2; 1.2; 1.5; 1.8] = [-2.0; -2.0; -1.0; 1.0 2.0 2.0]
```

Halves rounded down

``` ocaml
List.map  ~f:(Float.round ~direction:(`Closest `Down)) [-1.8; -1.5; -1.2; 1.2; 1.5; 1.8] = [-2.0; -2.0; -1.0; 1.0 1.0 2.0]
```

Halves rounded up

This is the default.

`Float.round 1.5` is the same as
`` Float.round ~direction:(`Closest `Up) 1.5 ``

Halves rounded towards the closest even number

``` ocaml
Float.round ~direction:(`Closest `ToEven) -1.5 = -2.0
Float.round ~direction:(`Closest `ToEven) -2.5 = -2.0
```

### `val floor`

``` ocaml
val floor : t -> t
```

Floor function, equivalent to `` Float.round ~direction:`Down ``.

Examples

``` ocaml
Float.floor 1.2 = 1.0
Float.floor 1.5 = 1.0
Float.floor 1.8 = 1.0
Float.floor -1.2 = -2.0
Float.floor -1.5 = -2.0
Float.floor -1.8 = -2.0
```

### `val ceiling`

``` ocaml
val ceiling : t -> t
```

Ceiling function, equivalent to `` Float.round ~direction:`Up ``.

Examples

``` ocaml
Float.ceiling 1.2 = 2.0
Float.ceiling 1.5 = 2.0
Float.ceiling 1.8 = 2.0
Float.ceiling -1.2 = (-1.0)
Float.ceiling -1.5 = (-1.0)
Float.ceiling -1.8 = (-1.0)
```

### `val truncate`

``` ocaml
val truncate : t -> t
```

Ceiling function, equivalent to `` Float.round ~direction:`Zero ``.

Examples

``` ocaml
Float.truncate 1.0 = 1.
Float.truncate 1.2 = 1.
Float.truncate 1.5 = 1.
Float.truncate 1.8 = 1.
Float.truncate (-1.2) = -1.
Float.truncate (-1.5) = -1.
Float.truncate (-1.8) = -1.
```

## [](#convert)Convert

### `val toInt`

``` ocaml
val toInt : t -> int option
```

Converts a `float` to an `Int` by **ignoring the decimal portion**. See
`Float`.truncate for examples.

Returns `None` when trying to round a `float` which can't be represented
as an `int` such as `Float`.nan or `Float`.infinity or numbers which are
too large or small.

You probably want to use some form of `Float`.round prior to using this
function.

Examples

``` ocaml
Float.(toInt 1.6) = (Some 1)
```

``` ocaml
Float.(toInt 2.0) = (Some 2)
```

``` ocaml
Float.(toInt 5.683) = (Some 5)
```

``` ocaml
Float.(toInt nan) = None
```

``` ocaml
Float.(toInt infinity) = None
```

``` ocaml
Float.(round 1.6 |> toInt) = Some 2
```

### `val to_int`

``` ocaml
val to_int : t -> int option
```

### `val toString`

``` ocaml
val toString : t -> string
```

Convert a `float` to a `String` The behaviour of this function is
platform specific

### `val to_string`

``` ocaml
val to_string : t -> string
```

## [](#compare)Compare

### `val equal`

``` ocaml
val equal : t -> t -> bool
```

Test two floats for equality

### `val compare`

``` ocaml
val compare : t -> t -> int
```

Compare two floats
