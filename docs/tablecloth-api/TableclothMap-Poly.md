# Module `TableclothMap.Poly`

Construct a Map which can be keyed by any data type using the
polymorphic `compare` function.

### `type identity`

``` ocaml
type identity
```

### `type t`

``` ocaml
type nonrec ('key, 'value) t = ( 'key, 'value, identity ) t
```

### `val empty`

``` ocaml
val empty : unit -> ( 'key, 'value ) t
```

A map with nothing in it.

### `val singleton`

``` ocaml
val singleton : key:'key -> value:'value -> ( 'key, 'value ) t
```

Create a map from a key and value

Examples

``` ocaml
Map.Poly.singleton ~key:false ~value:1 |> Map.toList = [(false, 1)]
```

### `val fromArray`

``` ocaml
val fromArray : ('key * 'value) array -> ( 'key, 'value ) t
```

Create a map from an `Array` of key-value tuples

### `val from_array`

``` ocaml
val from_array : ('key * 'value) array -> ( 'key, 'value ) t
```

### `val fromList`

``` ocaml
val fromList : ('key * 'value) list -> ( 'key, 'value ) t
```

Create a map from a `List` of key-value tuples

### `val from_list`

``` ocaml
val from_list : ('key * 'value) list -> ( 'key, 'value ) t
```
