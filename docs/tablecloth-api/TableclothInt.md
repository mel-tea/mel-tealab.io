# Module `TableclothInt`

-   [Constants](#constants)
-   [Create](#create)
-   [Operators](#operators)
-   [Query](#query)
-   [Convert](#convert)
-   [Compare](#compare)

The platform-dependant
[signed](https://en.wikipedia.org/wiki/Signed_number_representations)
[integer](https://en.wikipedia.org/wiki/Integer) type.

An `int` is a whole number.

`int`s are subject to
[overflow](https://en.wikipedia.org/wiki/Integer_overflow), meaning that
`Int.maximumValue + 1 = Int.minimumValue`.

If you need to work with integers larger than
[`maximumValue`](#val-maximumValue) (or smaller than
[`minimumValue`](#val-minimumValue) you can use the `Integer` module.

Valid syntax for `int`s includes:

``` ocaml
0
42
9000
1_000_000
1_000_000
0xFF (* 255 in hexadecimal *)
0x000A (* 10 in hexadecimal *)
```

**Note:** The number of bits used for an `int` is platform dependent.

When targeting Bucklescript [Ints are 32 bits](/bucklescript-docs/interop/common-data-types).

When targeting native OCaml uses 31-bits on 32-bit platforms and 63-bits
on 64-bit platforms which means that `int` math is well-defined in the
range `-2 ** 30` to `2 ** 30 - 1` for 32bit platforms `-2 ** 62` to
`2 ** 62 - 1` for 64bit platforms.

Outside of that range, the behavior is determined by the compilation
target.

You can read about the reasons for OCaml's unusual integer sizes
[here](https://v1.realworldocaml.org/v1/en/html/memory-representation-of-values.html).

*Historical Note:* The name `int` comes from the term
[integer](https://en.wikipedia.org/wiki/Integer)). It appears that the
`int` abbreviation was introduced in the programming language ALGOL 68.

Today, almost all programming languages use this abbreviation.

### `type t`

``` ocaml
type t = int
```

## [](#constants)Constants

### `val zero`

``` ocaml
val zero : t
```

The literal `0` as a named value

### `val one`

``` ocaml
val one : t
```

The literal `1` as a named value

### `val maximumValue`

``` ocaml
val maximumValue : t
```

The maximum representable `int` on the current platform

### `val maximum_value`

``` ocaml
val maximum_value : t
```

### `val minimumValue`

``` ocaml
val minimumValue : t
```

The minimum representable `int` on the current platform

### `val minimum_value`

``` ocaml
val minimum_value : t
```

## [](#create)Create

### `val fromString`

``` ocaml
val fromString : string -> t option
```

Attempt to parse a `string` into a `int`.

Examples

``` ocaml
Int.fromString "0" = Some 0.
```

``` ocaml
Int.fromString "42" = Some 42.
```

``` ocaml
Int.fromString "-3" = Some (-3)
```

``` ocaml
Int.fromString "123_456" = Some 123_456
```

``` ocaml
Int.fromString "0xFF" = Some 255
```

``` ocaml
Int.fromString "0x00A" = Some 10
```

``` ocaml
Int.fromString "Infinity" = None
```

``` ocaml
Int.fromString "NaN" = None
```

### `val from_string`

``` ocaml
val from_string : string -> t option
```

## [](#operators)Operators

**Note** You do not need to open the `Int` module to use the
[`(+)`](#val-(+)), [`(-)`](#val-(-)), [`(*)`](#val-(*)),
[`(**)`](#val-(**)), [`(mod)`](#val-(mod)) or [`(/)`](#val-(/))
operators, these are available as soon as you `open Tablecloth`

### `val add`

``` ocaml
val add : t -> t -> t
```

Add two `Int` numbers.

``` ocaml
Int.add 3002 4004 = 7006
```

Or using the globally available operator:

``` ocaml
3002 + 4004 = 7006
```

You *cannot* add an `int` and a `float` directly though.

See `Float`.add for why, and how to overcome this limitation.

### `val (+)`

``` ocaml
val (+) : t -> t -> t
```

See `Int`.add

### `val subtract`

``` ocaml
val subtract : t -> t -> t
```

Subtract numbers

``` ocaml
Int.subtract 4 3 = 1
```

Alternatively the operator can be used:

``` ocaml
4 - 3 = 1
```

### `val ( )`

``` ocaml
val (-) : t -> t -> t
```

See `Int`.subtract

### `val multiply`

``` ocaml
val multiply : t -> t -> t
```

Multiply `int`s like

``` ocaml
Int.multiply 2 7 = 14
```

Alternatively the operator can be used:

``` ocaml
(2 * 7) = 14
```

### `val (*)`

``` ocaml
val (*) : t -> t -> t
```

See `Int`.multiply

### `val divide`

``` ocaml
val divide : t -> by:t -> t
```

Integer division

Notice that the remainder is discarded.

Exceptions

Throws `Division_by_zero` when the divisor is `0`.

Examples

``` ocaml
Int.divide 3 ~by:2 = 1
```

``` ocaml
27 / 5 = 5
```

### `val (/)`

``` ocaml
val (/) : t -> t -> t
```

See `Int`.divide

### `val (/.)`

``` ocaml
val (/.) : t -> t -> float
```

Floating point division

Examples

``` ocaml
Int.(3 /. 2) = 1.5
```

``` ocaml
Int.(27 /. 5) = 5.25
```

``` ocaml
Int.(8 /. 4) = 2.0
```

### `val power`

``` ocaml
val power : base:t -> exponent:t -> t
```

Exponentiation, takes the base first, then the exponent.

Examples

``` ocaml
Int.power ~base:7 ~exponent:3 = 343
```

Alternatively the `**` operator can be used:

``` ocaml
7 ** 3 = 343
```

### `val (**)`

``` ocaml
val (**) : t -> t -> t
```

See `Int`.power

### `val negate`

``` ocaml
val negate : t -> t
```

Flips the 'sign' of an integer so that positive integers become negative
and negative integers become positive. Zero stays as it is.

Examples

``` ocaml
Int.negate 8 = (-8)
```

``` ocaml
Int.negate (-7) = 7
```

``` ocaml
Int.negate 0 = 0
```

Alternatively the `~-` operator can be used:

``` ocaml
~-(7) = (-7)
```

### `val (~ )`

``` ocaml
val (~-) : t -> t
```

See `Int`.negate

### `val absolute`

``` ocaml
val absolute : t -> t
```

Get the [absolute value](https://en.wikipedia.org/wiki/Absolute_value)
of a number.

Examples

``` ocaml
Int.absolute 8 = 8
```

``` ocaml
Int.absolute (-7) = 7
```

``` ocaml
Int.absolute 0 = 0
```

### `val modulo`

``` ocaml
val modulo : t -> by:t -> t
```

Perform [modular
arithmetic](https://en.wikipedia.org/wiki/Modular_arithmetic).

If you intend to use `modulo` to detect even and odd numbers consider
using `Int`.isEven or `Int`.isOdd.

The `modulo` function works in the typical mathematical way when you run
into negative numbers

Use `Int`.remainder for a different treatment of negative numbers.

Examples

``` ocaml
Int.modulo ~by:3 (-4) = 1
```

``` ocaml
Int.modulo ~by:3 (-3 )= 0
```

``` ocaml
Int.modulo ~by:3 (-2) = 2
```

``` ocaml
Int.modulo ~by:3 (-1) = 1
```

``` ocaml
Int.modulo ~by:3 0 = 0
```

``` ocaml
Int.modulo ~by:3 1 = 1
```

``` ocaml
Int.modulo ~by:3 2 = 2
```

``` ocaml
Int.modulo ~by:3 3 = 0
```

``` ocaml
Int.modulo ~by:3 4 = 1
```

### `val (mod)`

``` ocaml
val (mod) : t -> t -> t
```

See `Int`.modulo

### `val remainder`

``` ocaml
val remainder : t -> by:t -> t
```

Get the remainder after division. Here are bunch of examples of dividing
by four:

Use `Int`.modulo for a different treatment of negative numbers.

Examples

``` ocaml
List.map
  ~f:(Int.remainder ~by:4)
  [(-5); (-4); (-3); (-2); (-1); 0; 1; 2; 3; 4; 5] =
    [(-1); 0; (-3); (-2); (-1); 0; 1; 2; 3; 0; 1]
```

### `val maximum`

``` ocaml
val maximum : t -> t -> t
```

Returns the larger of two `int`s

Examples

``` ocaml
Int.maximum 7 9 = 9
```

``` ocaml
Int.maximum (-4) (-1) = (-1)
```

### `val minimum`

``` ocaml
val minimum : t -> t -> t
```

Returns the smaller of two `int`s

Examples

``` ocaml
Int.minimum 7 9 = 7
```

``` ocaml
Int.minimum (-4) (-1) = (-4)
```

## [](#query)Query

### `val isEven`

``` ocaml
val isEven : t -> bool
```

Check if an `int` is even

Examples

``` ocaml
Int.isEven 8 = true
```

``` ocaml
Int.isEven 7 = false
```

``` ocaml
Int.isEven 0 = true
```

### `val is_even`

``` ocaml
val is_even : t -> bool
```

### `val isOdd`

``` ocaml
val isOdd : t -> bool
```

Check if an `int` is odd

Examples

``` ocaml
Int.isOdd 7 = true
```

``` ocaml
Int.isOdd 8 = false
```

``` ocaml
Int.isOdd 0 = false
```

### `val is_odd`

``` ocaml
val is_odd : t -> bool
```

### `val clamp`

``` ocaml
val clamp : t -> lower:t -> upper:t -> t
```

Clamps `n` within the inclusive `lower` and `upper` bounds.

Exceptions

Throws an `Invalid_argument` exception if `lower > upper`

Examples

``` ocaml
Int.clamp ~lower:0 ~upper:8 5 = 5
```

``` ocaml
Int.clamp ~lower:0 ~upper:8 9 = 8
```

``` ocaml
Int.clamp ~lower:(-10) ~upper:(-5) 5 = (-5)
```

### `val inRange`

``` ocaml
val inRange : t -> lower:t -> upper:t -> bool
```

Checks if `n` is between `lower` and up to, but not including, `upper`.

Exceptions

Throws an `Invalid_argument` exception if `lower > upper`

Examples

``` ocaml
Int.inRange ~lower:2 ~upper:4 3 = true
```

``` ocaml
Int.inRange ~lower:5 ~upper:8 4 = false
```

``` ocaml
Int.inRange ~lower:(-6) ~upper:(-2) (-3) = true
```

### `val in_range`

``` ocaml
val in_range : t -> lower:t -> upper:t -> bool
```

## [](#convert)Convert

### `val toFloat`

``` ocaml
val toFloat : t -> float
```

Convert an integer into a float. Useful when mixing `Int` and `Float`
values like this:

Examples

``` ocaml
let halfOf (number : int) : float =
  Float.((Int.toFloat number) / 2)
  (* Note that locally opening the {!Float} module here allows us to use the floating point division operator *)
in
halfOf 7 = 3.5
```

### `val to_float`

``` ocaml
val to_float : t -> float
```

### `val toString`

``` ocaml
val toString : t -> string
```

Convert an `int` into a `string` representation.

Guarantees that

``` ocaml
Int.(fromString (toString n)) = Some n 
```

Examples

``` ocaml
Int.toString 3 = "3"
```

``` ocaml
Int.toString (-3) = "-3"
```

``` ocaml
Int.to_sString 0 = "0"
```

### `val to_string`

``` ocaml
val to_string : t -> string
```

## [](#compare)Compare

### `val equal`

``` ocaml
val equal : t -> t -> bool
```

Test two `int`s for equality

### `val compare`

``` ocaml
val compare : t -> t -> int
```

Compare two `int`s

### `type identity`

``` ocaml
type identity
```

The unique identity for `Comparator`

### `val comparator`

``` ocaml
val comparator : ( t, identity ) TableclothComparator.t
```
